RSpec.describe Companies::Tabs do
  describe "#call" do
    subject { described_class.new(document) }

    context 'without any tabs' do
      let (:document) do
        Document.new(boxes: [], categories: {})
      end

      it 'returns one required tab' do
        expect(subject.call.keys).to eq(['about'])
        expect(subject.call.about.boxes).to eq([])
      end
    end

    context 'without necessary tabs' do
      let (:document) do
        Document.new(
          boxes: [
            {type: 'paragraph', kind: 'text', body: 'text'},
            {type: 'paragraph', kind: 'h1',   body: 'header'},
            {type: 'paragraph', kind: 'text', body: 'text_last'}
          ],
          categories: {}
        )
      end

      it 'returns only one tab' do
        expect(subject.call.keys).to eq(['about'])
      end

      it 'returns all boxes in tab "about"' do
        expect(subject.call.about.boxes.map(&:body)).to eq(%w(text header text_last))
      end
    end

    context 'with all tabs' do
      let (:document) do
        Document.new(
          boxes: [
            {type: 'paragraph', kind: 'h1',   body: 'О компании'},
            {type: 'paragraph', kind: 'text', body: 'text1'},
            {type: 'paragraph', kind: 'h1',   body: 'Политика КСО'},
            {type: 'paragraph', kind: 'text', body: 'text2'},
            {type: 'paragraph', kind: 'h1',   body: 'Кадровая политика'},
            {type: 'paragraph', kind: 'text', body: 'text3'}
          ],
          categories: {}
        )
      end

      it 'returns 3 tabs' do
        expect(subject.call.keys.sort).to eq(%w(about kso policy))
      end

      it 'returns right box in tab "about"' do
        expect(subject.call.about.boxes.map(&:body)).to eq(%w(text1))
      end
      it 'returns right box in tab "kso"' do
        expect(subject.call.kso.boxes.map(&:body)).to eq(%w(text2))
      end
      it 'returns right box in tab "policy"' do
        expect(subject.call.policy.boxes.map(&:body)).to eq(%w(text3))
      end
    end

    context 'more difficult case' do
      let (:document) do
        Document.new(
          boxes: [
            {type: 'paragraph', kind: 'h1',   body: 'Политика корпоративной социальной ответственности *'},
            {type: 'paragraph', kind: 'text', body: 'text1'},               #kso
            {type: 'paragraph', kind: 'h1',   body: 'О компании'},
            {type: 'paragraph', kind: 'text', body: 'text2'},               #about
            {type: 'paragraph', kind: 'h1',   body: 'Кадровая политика'},
            {type: 'paragraph', kind: 'text', body: 'text3'},               #policy
            {type: 'paragraph', kind: 'h1',   body: 'Политика КСО *'},
            {type: 'paragraph', kind: 'text', body: 'text4'},               #kso
            {type: 'paragraph', kind: 'h1',   body: 'Кадровая'},            #kso
            {type: 'paragraph', kind: 'text', body: 'text5'},               #kso
            {type: 'paragraph', kind: 'h1',   body: 'Политика КСО'},
            {type: 'paragraph', kind: 'text', body: 'text6'},               #kso
          ],
          categories: {}
        )
      end

      it 'returns 3 tabs' do
        expect(subject.call.keys.sort).to eq(%w(about kso policy))
      end

      it 'returns right box in tab "about"' do
        expect(subject.call.about.boxes.map(&:body)).to eq(%w(text2))
      end
      it 'returns right box in tab "kso"' do
        expect(subject.call.kso.boxes.map(&:body)).to eq(%w(text1 text4 Кадровая text5 text6))
      end
      it 'returns right box in tab "policy"' do
        expect(subject.call.policy.boxes.map(&:body)).to eq(%w(text3))
      end
    end

    context 'with asterisk in tab titles' do
      let (:document) do
        Document.new(
          boxes: [
            {type: 'paragraph', kind: 'h1',   body: 'Политика КСО *'},
            {type: 'paragraph', kind: 'text', body: 'text1'},
            {type: 'paragraph', kind: 'h1',   body: 'Кадровая политика *'},
            {type: 'paragraph', kind: 'text', body: 'text2'},
            {type: 'paragraph', kind: 'h1',   body: 'О компании *'},
            {type: 'paragraph', kind: 'text', body: 'text3'}
          ],
          categories: {}
        )
      end

      it 'returns 3 tabs' do
        expect(subject.call.keys.sort).to eq(%w(about kso policy))
      end

      it 'returns 2 titles with asterisk' do
        expect(subject.call.values.map(&:title).sort).to eq([
          'Кадровая политика *',
          'О компании',
          'Политика корпоративной социальной ответственности *'
        ])
      end
    end
  end

  describe '#tab_names' do
    subject { described_class.new(nil) }

    it 'returns right names' do
      expect(subject.tab_names).to eq([
        {"slug" => :about, "tab_name" => 'О компании'},
        {"slug" => :kso, "tab_name" => 'Политика КСО'},
        {"slug"  => :policy, "tab_name" => 'Кадровая политика'}
      ])
    end
  end
end
