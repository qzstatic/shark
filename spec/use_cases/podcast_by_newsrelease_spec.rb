RSpec.describe PodcastByNewsrelease do
  describe "#call" do
    subject { described_class.new(newsrelease) }

    context 'with correct podcast box' do
      let (:newsrelease) do
        Roompen::Document.new(
          id: 1,
          title: 'newsrelease',
          url: 'url',
          published_at: '2015-02-27T02:48:46.654+03:00',
          boxes: [
            {
              type: 'newsrelease_podcast',
              podcast: {
                original: {
                  url: 'test'
                }
              }
            }
          ]
        )
      end

      it 'returns podcast object' do
        expect(subject.call.title).to eq('newsrelease')
        expect(subject.call.url).to eq('test')
      end
    end

    context 'with nil newsrelease' do
      let (:newsrelease) { nil }

      it 'returns nil' do
        expect(subject.call).to be_nil
      end
    end

    context "newsrelease hasn't box" do
      let (:newsrelease) do
        Roompen::Document.new(
          id: 1,
          title: 'newsrelease',
          url: 'url',
          published_at: '2015-02-27T02:48:46.654+03:00',
          boxes: []
        )
      end

      it 'returns nil' do
        expect(subject.call).to be_nil
      end
    end

    context "has box, but hasn't file" do
      let (:newsrelease) do
        Roompen::Document.new(
          id: 1,
          title: 'newsrelease',
          url: 'url',
          published_at: '2015-02-27T02:48:46.654+03:00',
          boxes: [
            { type: 'newsrelease_podcast' }
          ]
        )
      end

      it 'returns nil' do
        expect(subject.call).to be_nil
      end
    end

    context "has box, has file, but hasn't version" do
      let (:newsrelease) do
        Roompen::Document.new(
          id: 1,
          title: 'newsrelease',
          url: 'url',
          published_at: '2015-02-27T02:48:46.654+03:00',
          boxes: [
            {
              type: 'newsrelease_podcast',
              podcast: {}
            }
          ]
        )
      end

      it 'returns nil' do
        expect(subject.call).to be_nil
      end
    end

  end
end
