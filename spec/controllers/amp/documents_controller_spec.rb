RSpec.describe Amp::DocumentsController,:type => :controller do
  describe 'documents#show' do
    context 'with valid document_url and secret_key' do
      let(:secret_key) { 'e3474430a9' }
      let(:document_url) { 'management/articles/2016/06/03/643461-predprinimateli-eksperimentiruyut-vzbitim-medom' }
      it 'returns success' do
        get :show, :secret_key => secret_key, :document_url => document_url
        expect(response).to be_success
      end
    end
    context 'with invalid secret_key' do
      let(:secret_key) { '12345678' }
      let(:document_url) { 'management/articles/2016/06/03/643461-predprinimateli-eksperimentiruyut-vzbitim-medom' }
      it 'returns forbidden'do
        get :show, :secret_key => secret_key, :document_url => document_url
        expect(response).to be_forbidden
      end
    end
    context 'with valid secret_key and not exists document_url' do
      let(:secret_key) { 'a44767ebda' }
      let(:document_url) { 'management/foo' }
      it 'returns forbidden' do
        get :show, :secret_key => secret_key, :document_url => document_url
        expect(response).to have_http_status(404)
      end
    end
  end
end
