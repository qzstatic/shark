RSpec.describe MainController, type: :controller do
  describe 'main#index' do
    context 'desktop format' do
      it 'returns success' do
        get :index
        expect(response.status).to be_success
        expect(response).to render_template('main/index')
      end
    end

    context 'mobile format' do
      it 'returns success' do
        get :index, mobile: true
        expect(response).to be_success
        expect(response).to render_template('main/index')
      end
    end
  end
end
