RSpec.describe Amp::UrlHelper, type: :helper do
  describe "#access_amp_url?" do
    context 'with valid secret_key' do
      let(:secret_key) { 'e3474430a9' }
      let(:document_url) { 'management/articles/2016/06/03/643461-predprinimateli-eksperimentiruyut-vzbitim-medom' }
      it 'returns success' do
        expect(helper.access_amp_url?(document_url,secret_key)).to be true
      end
    end
    context 'with invalid secret_key' do
      let(:secret_key) { 'e3474430a9123' }
      let(:document_url) { 'management/articles/2016/06/03/643461-predprinimateli-eksperimentiruyut-vzbitim-medom' }
      it 'returns false' do
        expect(helper.access_amp_url?(document_url,secret_key)).to be false
      end
    end
    context 'with empty secret_key' do
      let(:secret_key) { '' }
      let(:document_url) { 'management/articles/2016/06/03/643461-predprinimateli-eksperimentiruyut-vzbitim-medom' }
      it 'returns false' do
        expect(helper.access_amp_url?(document_url,secret_key)).to be false
      end
    end
  end
  describe '#secret_amp_key' do
      context 'with valid url' do
        let(:url) { 'management/articles/2016/06/03/643461-predprinimateli-eksperimentiruyut-vzbitim-medom' }
        it 'returns secret_key' do
          expect(helper.secret_amp_key(url)).not_to be_empty
        end
      end
      context 'with invalid url' do
        let(:url) { 123 }
        it 'returns empty string' do
          expect(helper.secret_amp_key(url)).not_to be_empty
        end
      end
  end
end
