RSpec.describe ApplicationHelper, type: :helper do
  describe "#relative_link" do
    context 'with base domain' do
      let(:url) { 'vedomosti.ru/news/aaa/bbb' }
      
      it 'returns relative link' do
        expect(helper.relative_link(url)).to eq('/news/aaa/bbb')
      end
    end

    context 'with subdomain' do
      let(:url) { 'super.vedomosti.ru/news/aaa/bbb' }
      
      it 'returns absolute link' do
        expect(helper.relative_link(url)).to eq('http://super.vedomosti.ru/news/aaa/bbb')
      end
    end
    
    context 'with empty url' do
      let(:url) { '' }
      
      it 'returns #' do
        expect(helper.relative_link(url)).to eq('#')
      end
    end
    
    context 'with nil url' do
      let(:url) { nil }
      
      it 'returns #' do
        expect(helper.relative_link(url)).to eq('#')
      end
    end
  end


  
  describe "#label_data" do
    let(:document) { Document.new(document_attributes) }

    context 'page main' do
      let(:document_attributes) do
        {
          categories: {
            rubrics: [
              {
                slug: 'economics',
                title: 'Экономика',
                subrubrics: {
                  slug: 'projects',
                  title: 'Госинвестиции и проекты'
                },
              }
            ]
          }
        }
      end

      it 'list.top' do
        expect(helper.label_data(document, 'main', :index)).to eq(title: document.rubric.title,  url: rubric_path(rubric_slug: document.rubric.slug))
      end

      it 'list.rubric' do
        expect(helper.label_data(document, 'not_main', :index)).to eq(
          title: document.subrubric.title,
          url: subrubric_path(rubric_slug: document.rubric.slug, subrubric_slug: document.subrubric.slug)
        )
      end
    end

    context 'page not main' do
      let(:document_attributes) do
        {
          categories: {
            rubrics: [
              {
                slug: 'economics',
                title: 'Экономика',
                subrubrics: {
                  slug: 'projects',
                  title: 'Госинвестиции и проекты'
                },
              }
            ]
          }
        }
      end

      it 'list.top' do
        expect(helper.label_data(document, 'main', :not_index)).to eq(
          title: document.subrubric.title,
          url: subrubric_path(rubric_slug: document.rubric.slug, subrubric_slug: document.subrubric.slug)
        )
      end

      it 'list.subribric' do
        expect(helper.label_data(document, 'not_main', :not_index)).to be_nil
      end
    end

    context 'wihout data' do
      let(:document_attributes) do
        {
          categories: {
            rubrics: [
              {
                slug: 'economics',
                title: 'Экономика',
                subrubrics: {
                  slug: 'projects',
                  title: 'Госинвестиции и проекты'
                },
              }
            ]
          }
        }
      end
      
      it 'doc on page.main list.top without rubric' do
        document_attributes[:categories].delete(:rubrics)
        expect(helper.label_data(document, 'main', :index)).to be_nil
      end

      it 'doc on page.main list.rubric without subrubric' do
        document_attributes[:categories][:rubrics].first.delete(:subrubrics)
        expect(helper.label_data(document, 'not_main', :index)).to be_nil
      end
    end

    context 'library' do
      let(:document_attributes) do
        {
          categories: {
            library: {
              slug: 'characters',
              title: 'Действующее лицо'
            },
            rubrics: [{slug: 'ooops'}]
          }
        }
      end
      
      it 'doc from library' do
        expect(helper.label_data(document, 'not_main', :index)).to eq(title: document.categories.library.title,  url: library_path(slug: document.categories.library.slug))
      end
    end
    
    context 'opinion editorial' do
      let(:document_attributes) do
        {
          categories: {
            rubrics: [
              {
                slug: 'opinion',
                title: 'Мнения',
                subrubrics: {
                  slug: 'editorial',
                  title: 'От редакции'
                }
              },{
                slug: 'economics',
                title: 'Экономика',
                subrubrics: {
                  slug: 'projects',
                  title: 'Госинвестиции и проекты'
                }
              }
            ]
          }
        }
      end

      it 'page.main list.top' do
        expect(helper.label_data(document, 'main', :index)).to eq(
          title: document.subrubric.title,
          url: subrubric_path(rubric_slug: document.rubric.slug, subrubric_slug: document.subrubric.slug)
        )
      end
  
      it 'page.main list.rubric' do
        expect(helper.label_data(document, 'not_main', :index)).to eq(
          title: document.subrubric.title,
          url: subrubric_path(rubric_slug: document.rubric.slug, subrubric_slug: document.subrubric.slug)
        )
      end

      it 'page.not_main list.top' do
        expect(helper.label_data(document, 'main', :not_index)).to eq(
          title: document.subrubric.title,
          url: subrubric_path(rubric_slug: document.rubric.slug, subrubric_slug: document.subrubric.slug)
        )
      end
    end

    context 'opinion details' do
      let(:document_attributes) do
        {
          categories: {
            rubrics: [
              {
                slug: 'opinion',
                title: 'Мнения',
                subrubrics: {
                  slug: 'details',
                  title: 'Детали',
                  sections: {
                    slug: 'person_of_week',
                    title: 'Человек недели'
                  }
                }
              }
            ]
          }
        }
      end

      it 'page.main list.top' do
        expect(helper.label_data(document, 'main', :index)).to eq(
          title: document.section.title,
          url: section_path(rubric_slug: document.rubric.slug, subrubric_slug: document.subrubric.slug, section_slug: document.section.slug)
        )
      end

      it 'page.main list.rubric' do
        expect(helper.label_data(document, 'not_main', :index)).to eq(
          title: document.section.title,
          url: section_path(rubric_slug: document.rubric.slug, subrubric_slug: document.subrubric.slug, section_slug: document.section.slug)
        )
      end

      it 'page.not_main list.top' do
        expect(helper.label_data(document, 'main', :not_index)).to eq(
          title: document.section.title,
          url: section_path(rubric_slug: document.rubric.slug, subrubric_slug: document.subrubric.slug, section_slug: document.section.slug)
        )
      end
    end

    context 'theme cycles' do
      let(:document_attributes) do
        {
          categories: {
            rubrics: [
              {
                slug: 'opinion',
                title: 'Мнения',
                subrubrics: {
                  slug: 'editorial',
                  title: 'От редакции'
                }
              }
            ],
            themes: {
              slug: 'cycle_columns',
              title: 'Цикл колонок',
              cycle_list: {
                slug: 'conditions',
                title: 'Конъюктура'
              }
            }
          }
        }
      end
      
      it 'page.main list.top' do
        expect(helper.label_data(document, 'main', :index)).to eq(
          title: document.theme_cycle.title,
          url: theme_path(slug: document.theme_cycle.slug)
        )
      end

      it 'page.main list.subrubric' do
        expect(helper.label_data(document, 'not_main', :index)).to eq(
          title: document.theme_cycle.title,
          url: theme_path(slug: document.theme_cycle.slug)
        )
      end

      it 'page.not_main list.top' do
        expect(helper.label_data(document, 'main', :not_index)).to eq(
          title: document.theme_cycle.title,
          url: theme_path(slug: document.theme_cycle.slug)
        )
      end
    end
  end

  describe '#label_link' do
    let(:document_attributes) do
      {
        categories: {
          rubrics: [
            {
              slug: 'economics',
              title: 'Экономика',
              subrubrics: {
                slug: 'projects',
                title: 'Госинвестиции и проекты'
              },
            }
          ]
        }
      }
    end
    let(:document) { Document.new(document_attributes) }

    it 'return link tag' do
      expect(label_link(document, 'main', :index)).to eq('<a href="/rubrics/economics">Экономика</a>')
    end
  end

  describe '#get_value_of_category' do
    let(:categories) do
      {
        kinds: {
          title: "Тип материала",
          slug: "materials",
          position: 0,
        },
        rubrics: [
          {
            slug: 'economics',
            title: 'Экономика',
            subrubrics: {
              slug: 'projects',
              title: 'Госинвестиции и проекты'
            },
          },
          {
            slug: 'politics',
            title: 'Политика',
            subrubrics: {
              slug: 'world',
              title: 'World',
              sections: {
                slug: 'persons',
                title: 'Persons'
              }
            },
            region: {
              slug: 'spb',
              title: 'Spb'
            }
          },
          {
            slug: 'business',
            title: 'Business'
          }
        ]
      }
    end

    it 'return string for simple category' do
      expect(categories_for_swift(categories[:kinds])).to eq('materials')
    end

    it 'return hash of hash for complex categories' do
      expect(categories_for_swift(categories[:rubrics].first)).to eq({ economics: {subrubrics: 'projects'} })
    end

    it 'return complex hash for complex categories' do
      expect(categories_for_swift(categories)).to eq(
        {
          kinds: 'materials',
          rubrics: [
            {
              economics: {
                subrubrics: 'projects'
              }
            },
            {
              politics: {
                subrubrics: {
                  world: {
                    sections: 'persons'
                  },
                },
                region: 'spb'
              }
            },
            'business'
          ]
        }
      )
    end
  end

  describe '#categories_to_tags' do
    let(:categories) do
      {
        kinds: {
          title: "Тип материала",
          slug: "materials",
          position: 0,
        },
        rubrics: [
          {
            slug: 'economics',
            title: 'Экономика',
            subrubrics: {
              slug: 'projects',
              title: 'Госинвестиции и проекты'
            },
          },
          {
            slug: 'politics',
            title: 'Политика',
            subrubrics: {
              slug: 'world',
              title: 'World',
              sections: {
                slug: 'persons',
                title: 'Persons'
              }
            },
            region: {
              slug: 'spb',
              title: 'Spb'
            }
          },
          {
            slug: 'business',
            title: 'Business'
          }
        ]
      }
    end

    it 'convert' do
      expect(categories_to_tags(categories)).to eq([
        "kinds:materials",
        "rubrics:economics",
        "subrubrics:projects",
        "rubrics:politics",
        "subrubrics:world",
        "sections:persons",
        "region:spb",
        "rubrics:business"
      ])
    end
  end
  describe '#image_srcset' do
    context 'with valid document' do
      let(:document_url)  { 'vedomosti.ru/management/articles/2016/06/03/643461-predprinimateli-eksperimentiruyut-vzbitim-medom' }
      it 'returns not empty string' do
        @document = Roompen.document document_url
        expect(helper.image_srcset(@document.image)).not_to be_empty
      end
    end
  end
end
