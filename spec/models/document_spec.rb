RSpec.describe Document do
  describe "#newsrelease" do
    let(:document_attributes) do
      {
        links: {
          newsrelease: [
            {
              position: 0,
              bound_document: {
                id: 140737488361032,
                title: "Газетный выпуск №3746",
                published_at: "2014-12-26T00:00:00.000+03:00",
                url: "http://vedomosti.ru/newspaper/2014/12/26",
                categories: {
                  kinds: {
                    title: "Газетный выпуск",
                    slug: "newsrelease",
                    position: 50
                  },
                  domains: {
                    title: "vedomosti.ru",
                    slug: "vedomosti.ru",
                    position: 0
                  }
                }
              }
            }
          ]
        },
        categories: {}
      }
    end

    subject { Document.new(document_attributes) }

    it 'return newsrelease' do
      expect(subject.has_newsrelease?).to eq(true)
      expect(subject.newsrelease.url).to eq('http://vedomosti.ru/newspaper/2014/12/26')
    end
  end
end
