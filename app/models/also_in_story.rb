class AlsoInStory
  attr_reader :list, :excluded_document_id
  
  extend Forwardable
  
  def_delegators :list, :relative_url
  
  def initialize(list_slug)
    @list = Roompen.list(list_slug)
  end
  
  def slug
    list.slug
  end

  def documents
    list.documents.first(5)
  end

  def header
    'Другие материалы в сюжете'
  end

  def template
    'also_in_story'
  end

  def as_json(*)
    {
      slug: slug,
      header: header,
      template: template,
      relative_url: relative_url,
      documents: documents
    }
  end
end
