class Option
  attr_reader :id, :value
  
  TYPES = %w(company_speciality appointment income auto)

  def initialize(id, value)
    @id    = id
    @value = value
  end
  
  class << self
    TYPES.each do |type|
      define_method(type) do
        options[type] ||= begin
          json = Shark::Redis.connection.get('shark:user_options:' + type.to_s)
          data = json.nil? ? [] : Oj.load(json)
          data.map { |option_attrs| Option.new(*option_attrs) }
        end
      end
    end
    
  private
    def options
      @options ||= {}
    end
  end
end
