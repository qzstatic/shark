class AccessRight
  attr_accessor :order, :start_date, :end_date
  
  def initialize(access_right = {})
    @order, @start_date, @end_date = access_right.values_at(:order, :start_date, :end_date)
  end
  
  def order
    if @order
      Order.new(@order)
    end
  end
end
