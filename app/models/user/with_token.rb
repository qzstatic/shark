class User
  class WithToken < self
    attr_accessor :external_id, :oauth_token, :provider
    
    PROVIDERS = %w(twitter facebook vkontakte)
    
    validates_presence_of :oauth_token, :provider
    validates :provider, inclusion: { in: PROVIDERS }
    
    def attributes
      super.merge(
        provider:    provider,
        oauth_token: oauth_token
      )
    end
    
    class << self
      def with_token(ip, access_token, provider, token, token_secret = nil)
        user_info = Shark::SocialNetworkClient.user_info_for(
          provider:     provider,
          token:        token,
          token_secret: token_secret
        )
        
        response = client.post(
          ip,
          User::WithOauth.sign_in_url,
          provider:     provider,
          external_id:  user_info[:external_id],
          access_token: access_token
        )
        
        if response.success?
          User::WithToken.new(response.body)
        else
          user_info
        end
      end
    end
  end
end
