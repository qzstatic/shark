class User
  class WithOauth < self
    attr_accessor :external_id, :provider
    
    PROVIDERS = %w(twitter facebook vkontakte)
    
    validates_presence_of :external_id, :provider
    validates :provider, inclusion: { in: PROVIDERS }
    
    def attributes
      super.merge(
        provider:    provider,
        external_id: external_id
      )
    end
    
    class << self
      def sign_up_url
        '/shark/sign_up_with_oauth'
      end
      
      def sign_in_url
        '/shark/sign_in_with_oauth'
      end
      
      def from_auth_hash(auth_hash)
        if auth_hash[:provider] == 'twitter'
          names = auth_hash[:info][:name].split(' ')
        else
          names = auth_hash[:info].values_at(:first_name, :last_name)
        end
        
        new(
          external_id: auth_hash[:uid],
          provider:    auth_hash[:provider],
          first_name:  names.first,
          last_name:   names.last,
          nickname:    auth_hash[:info][:nickname],
          email:       auth_hash[:info][:email]
        )
      end
      
      def with(provider, external_id, ip, access_token)
        response = client.post(ip, sign_in_url, provider: provider, external_id: external_id, access_token: access_token)
        User::WithOauth.new(response.body) if response.success?
      end
    end
  end
end
