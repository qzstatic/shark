class User
  class WithPassword < self
    attr_accessor :password
    
    validates_presence_of :password
    validates_confirmation_of :password
    validates_length_of :password, in: 6..30
    
    def attributes
      super.merge(
        auth_type: 'password',
        password: password
      )
    end
    
    class << self
      def sign_up_url
        '/shark/sign_up_with_password'
      end
    end
  end
end
