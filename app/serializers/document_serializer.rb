class DocumentSerializer
  def initialize(document, bubo_reason)
    @document   = document
    @bubo_reason = bubo_reason
  end

  def as_json(*params)
    process_boxes(@document).except(:last_published_at)
  end

  def process_boxes(document)
    document.as_json.tap do |document_data|
      document_data[:boxes].each_with_index do |box_data, i|
        if box_data[:type] == 'inset_media'
          inset_media_parse!(box_data)
        elsif box_data[:type] == 'paywall_line'
          document_data[:boxes].delete_at(i)
          if document.pay_required? && !subscriber?
            document_data[:boxes].slice!(i..-1)
            break
          end
        else
          if box_data.has_key?(:body)
            box_data[:body] = strip_tags(box_data[:body])
          end
          if box_data.has_key?(:bound_document) && box_data[:bound_document] && box_data[:bound_document].has_key?(:subtitle)
            box_data[:bound_document][:subtitle] = strip_tags(box_data[:bound_document][:subtitle])
          end
        end
      end
      document_data[:subtitle] = strip_tags(document_data[:subtitle])
    end
  end

  def inset_media_parse!(box_data)
    if matched = /src="(?<url>.*?)"/.match(box_data[:body])
      uri = URI.parse(matched[:url])
      if /youtube.com/.match(uri.host)
        box_data[:provider] = 'youtube'
        box_data[:video_id] = uri.path.split('/').last
      end
    end
  end

  def strip_tags(text)
    Nokogiri::HTML(text).text
  end

  def subscriber?
    %w(subscriber mobile_subscriber ip).include?(@bubo_reason)
  end
end
