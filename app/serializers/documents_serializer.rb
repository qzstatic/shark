class DocumentsSerializer
  def initialize(documents)
    @documents = documents
  end
  
  def as_json(*params)
    @documents.map { |document| process_document(document).except(:last_published_at).as_json }
  end
  
  def process_document(document)
    document.as_json.tap do |document_data|
      document_data[:subtitle] = Nokogiri::HTML(document_data[:subtitle]).text
    end
  end
end
