class PasswordForm
  include ActiveModel::Model
  
  attr_accessor :password, :password_confirmation
  validates                 :password, presence: true
  validates_length_of       :password, within: 6..20
  validates_confirmation_of :password
end
