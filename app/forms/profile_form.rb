class ProfileForm
  include Virtus.model
  extend  ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations
  
  attribute :first_name, String
  attribute :last_name, String
  attribute :email, String
  attribute :phone, String
  attribute :password, String
  attribute :password_confirmation, String
  
  attribute :access_rights, Array
  
  attribute :access_token, String
  attribute :ip, IPAddr
  
  validates_presence_of :first_name, :last_name, :email
  validates_confirmation_of :password
  
  def access_rights
    super.map { |attributes| AccessRight.new(attributes) }
  end
  
  def save
    persist! if valid?
  end
  
private
  def persist!
    profile_response = client.patch(ip, profile_path, user: profile_attributes)
    
    if profile_response.success?
      unless password.empty?
        client.patch(ip, password_path, password: password)
      end
      
      true
    else
      errors.add(:base, response.body)
      
      false
    end
  end
  
  def profile_attributes
    %i(first_name last_name email phone).each_with_object(Hash.new) do |attr_name, hash|
      hash[attr_name] = public_send(attr_name)
    end
  end
  
  def profile_path
    "/shark/users/#{access_token}"
  end
  
  def password_path
    "/shark/users/#{access_token}/password"
  end
  
  def client
    AquariumClient.new
  end
end
