class PasswordRegistrationForm
  include Virtus.model
  extend  ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations
  
  attribute :first_name,            String
  attribute :last_name,             String
  attribute :nickname,              String
  attribute :email,                 String
  attribute :phone,                 String
  attribute :password,              String
  attribute :password_confirmation, String
  
  attribute :rules, Boolean
  
  attr_accessor :access_token, :ip
  
  attr_reader :status, :user_data
  private :status
  
  validates :first_name, :last_name, :email, :password, presence: true
  validates :password, length: { within: 6..30 }, confirmation: true
  
  def save
    persist! if valid?
  end
  
  def success?
    status == 200
  end
  
private
  def persist!
    response = client.post(
      ip,
      '/shark/sign_up_with_password',
      user: attributes.merge(access_token: access_token)
    )
    
    @status = response.status
    handle_aquarium_response(response.body)
  end
  
  def handle_aquarium_response(data)
    if success?
      @user_data = data
    else
      data.each do |attr_name, attr_errors|
        attr_errors.each do |error_message|
          errors.add(attr_name, error_message)
        end
      end
    end
  end
  
  def attributes
    {
      first_name: first_name,
      last_name:  last_name,
      email:      email,
      phone:      phone,
      nickname:   nickname,
      auth_type:  'password',
      password:   password
    }
  end
  
  def client
    @client ||= AquariumClient.new
  end
end
