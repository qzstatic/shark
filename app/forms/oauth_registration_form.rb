class OauthRegistrationForm
  include Virtus.model
  extend  ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations
  
  attribute :first_name, String
  attribute :last_name,  String
  attribute :nickname,   String
  attribute :email,      String
  attribute :phone,      String
  
  attribute :provider,    String
  attribute :external_id, String
  
  attribute :rules, Boolean
  
  attr_accessor :access_token, :ip
  
  attr_reader :status
  private :status
  
  PROVIDERS = %w(twitter facebook vkontakte).freeze
  
  validates :first_name, :last_name, :email, :external_id, :provider, presence: true
  validates :provider, inclusion: { in: PROVIDERS }
  validates :rules, acceptance: { accept: true }
  
  def save
    persist! if valid?
  end
  
  def success?
    status == 200
  end
  
private
  def persist!
    response = client.post(
      ip,
      '/shark/sign_up_with_oauth',
      user: attributes.merge(access_token: access_token)
    )
    
    @status = response.status
    handle_aquarium_response(response.body)
  end
  
  def handle_aquarium_response(data)
    unless success?
      data.each do |attr_name, attr_errors|
        attr_errors.each do |error_message|
          errors.add(attr_name, error_message)
        end
      end
    end
  end
  
  def client
    @client ||= AquariumClient.new
  end
  
  class << self
    def from_auth_hash(auth_hash, access_token:, ip:)
      if auth_hash[:provider] == 'twitter'
        names = auth_hash[:info][:name].split(' ')
      else
        names = auth_hash[:info].values_at(:first_name, :last_name)
      end
      
      new(
        first_name:   names.first,
        last_name:    names.last,
        nickname:     auth_hash[:info][:nickname],
        email:        auth_hash[:info][:email],
        provider:     auth_hash[:provider],
        external_id:  auth_hash[:uid],
        access_token: access_token,
        ip:           ip
      )
    end
  end
end
