class SignInForm
  include ActiveModel::Model
  
  attr_accessor :email, :password
  attr_accessor :access_token, :ip
  
  attr_reader :status, :user_data
  private :status
  
  validates :password, length: { within: 4..30 }
  validates :email, :password, presence: true
  
  def save
    if valid?
      persist!
      notify_newrelic
      handle_aquarium_errors
    end
  end
  
  def success?
    status == 200
  end
  
  def blocked?
    status == 403
  end
  
  def too_many_sessions?
    status == 429
  end
  
private
  def persist!
    response = client.post(
      ip,
      '/shark/sign_in_with_password',
      email:        email,
      password:     password,
      access_token: access_token
    )
    
    @status    = response.status
    @user_data = response.body if success?
  end
  
  def handle_aquarium_errors
    errors.add(:email, 'неверный логин или пароль') if status == 422
  end
  
  def notify_newrelic
    if success?
      ::NewRelic::Agent.increment_metric('Custom/Sessions/successful_sign_in')
    elsif blocked?
      ::NewRelic::Agent.increment_metric('Custom/Sessions/blocked_sign_in_attempt')
    else
      ::NewRelic::Agent.increment_metric('Custom/Sessions/failed_sign_in')
    end
  end
  
  def client
    @client ||= AquariumClient.new
  end
end
