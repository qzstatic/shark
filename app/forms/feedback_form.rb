class FeedbackForm
  include Virtus.model
  extend  ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations
  
  attribute :type,      String
  attribute :user_data, String
  attribute :username,  String
  attribute :email,     String
  attribute :phone,     String
  attribute :message,   String
            
  attr_accessor :access_token, :ip, :user_agent
  
  attr_reader :status
  private :status
  
  validates :type, :username, :email, :phone, :message, presence: true
  validates :email, email: true
  
  def save
    persist! if valid?
  end
  
  def success?
    status == 200
  end
  
private
  def persist!
    response = client.post(ip, '/shark/feedback', attributes: attributes.merge(access_token: access_token))
    @status = response.status
    handle_aquarium_response(response.body)
  end
  
  def handle_aquarium_response(data)
    unless success?
      data.each do |attr_name, attr_errors|
        attr_errors.each do |error_message|
          errors.add(attr_name, error_message)
        end
      end
    end
  end
  
  def user_data
    user_data = {
      user_agent: user_agent,
      ip:         ip
    }
    if user
      user_data.merge!(user: user.attributes.merge(Oj.load(user.for_user_options)))
    end
    
    user_data
  end
  
  def client
    AquariumClient.new
  end
  
  def user
    @user ||= User.with_token(access_token, ip)
  end
end
