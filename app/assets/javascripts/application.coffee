#= require angular
#= require angular-animate
#= require angular-cookies
#= require angular-route
#= require angular-ui-bootstrap-tpls
#= require angular-rails-templates
#= require angular-local-storage
#= require vendors/angular-locale_ru-ru
#= require vendors/encode-uri
#= require vendors/unique
#= require vendors/offClick
#= require vendors/angular-smooth-scroll
#= require vendors/angular-pickadate
#= require vendors/angular-placeholder-shim.js
#= require vendors/angular-md5

#= require vendors/jquery3
#= require vendors/jquery.html5-placeholder-shim.js
#= require vendors/jquery.cookie.js
#= require vendors/detect-private-browsing

#= require vendors/swfobject-2.2.min
#= require vendors/evercookie
#= require vendors/uri.min.js
#= require vendors/cookie
#= require vendors/boom
#= require vendors/tablesorter.min.js
#= require moment

#= require_tree ./modules
#= require_tree ./modules_templates

#= require settings
#= require versions
#= require buy_cookie
