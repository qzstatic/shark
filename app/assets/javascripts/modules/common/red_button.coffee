# Большая красная кнопка - оключает рекламу

# ##Директива отключения рекламы##
angular.module 'shared'
.directive 'adriverButton', ($window, $rootScope, $cookies, $timeout) ->
  scope: {}
  controller: ($scope) ->

  link: (scope, element) ->
    element.addClass('adriver-button_on') if $cookies.adriver_stop == 'true'
    element.on 'click', () ->
      $cookies.adriver_stop = if ($cookies.adriver_stop == 'false' || $cookies.adriver_stop == undefined) then true else false

      $timeout ()->
        window.location.replace("/")
      ,
      200


