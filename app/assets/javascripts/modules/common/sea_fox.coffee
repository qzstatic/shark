#Alopias vulpinus (Sea fox) - Лисья акула. Крупная акула, достигающая 6 метров в длину и веса более 510 кг.
# Почти половину длины рыба составляет ее длинный хвост.
#
#![Sea fox](http://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Alopias_vulpinus.jpg/350px-Alopias_vulpinus.jpg)

# ##Директива 'плавающего' правого меню##
# Временно используется
angular.module 'shared'
.directive 'seaFoxShark',($window, Scroller, $rootScope, $compile) ->

  controller: ($scope) ->
    $scope.sea_fox =
      current_banner_id: 1
      is_visible: true
      is_fixed: false
      breakpoints: []
      breakpoints2: []

      # Определяет области, в которой sea_fox фиксируется, те <code>style='position: fixed'</code> <br>
      # Для этого документа должны существовать 2 точки с id <code>#breakpoint1</code> и <code>#breakpoint2</code>
      set_breakpoints: ->
        $scope.sea_fox.breakpoints2[0] = $('#breakpoint1').offset().top - 7
        $scope.sea_fox.breakpoints2[1] = $('#breakpoint2').offset().top

        height = $('#breakpoint2').offset().top - $('#breakpoint1').offset().top
        for i in [1,2,3]
          $scope.sea_fox.breakpoints.push (i * height/3)


      # Меняет баннер в зависимости от параметра offset
      # 1. offset - отступ сверху
      change_banner: (offset) ->
        if offset <= $scope.sea_fox.breakpoints[0]
          $scope.sea_fox.current_banner_id = 1
        else if (offset <= $scope.sea_fox.breakpoints[1] && offset > $scope.sea_fox.breakpoints[0])
          $scope.sea_fox.current_banner_id = 2
        else if offset > $scope.sea_fox.breakpoints[1]
          $scope.sea_fox.current_banner_id = 3

      # Устанавливает position:fixed в зависимости от параметра offset
      # 1. offset - отступ сверху
      change_position: (offset) ->
        if offset < $scope.sea_fox.breakpoints2[0]
          $scope.sea_fox.is_fixed = false
          $scope.sea_fox.current_top_position = $scope.sea_fox.breakpoints2[0] + 'px'
        else if offset > $scope.sea_fox.breakpoints2[0] && offset <= $scope.sea_fox.breakpoints2[1] - 600
          $scope.sea_fox.is_fixed = true
          $scope.sea_fox.change_banner(offset)
        else if offset > $scope.sea_fox.breakpoints2[1] - 600
          $scope.sea_fox.is_fixed = false
          #position: fixed; top: 60px
          $scope.sea_fox.current_top_position = $scope.sea_fox.breakpoints2[1] - 540 + 'px'


      # перерасчитывает breakpoint'ы и геренирует новые события для Scroller'а
      update_breakpoints: ->
        # пересчет верхней и нижней границы тела документа
        $scope.sea_fox.set_breakpoints()
        $scope.sea_fox.change_position angular.element($window)[0].pageYOffset

        # удаление старых breakpoint'ов
        Scroller.remove_breakpoint 'sea_fox.fix'
        Scroller.remove_breakpoint 'sea_fox.bottom'
        Scroller.remove_breakpoint 'sea_fox.top'

        # назначение новых breakpoint'ов
        Scroller.add_breakpoint 'sea_fox.fix', $scope.sea_fox.breakpoints2[0],  $scope.sea_fox.breakpoints2[1] - 600
        Scroller.add_breakpoint 'sea_fox.top', 0, $scope.sea_fox.breakpoints2[0]
        Scroller.add_breakpoint 'sea_fox.bottom', $scope.sea_fox.breakpoints2[1] - 600, $scope.sea_fox.breakpoints2[1] + 100500

  link: (scope, element) ->
    #инициализируем скоуп
    $compile(element.contents())(scope.$new())

    #начинать работать можно только после определения размеров
    $rootScope.$on 'blueShark.load', ->
      sea_fox = scope.sea_fox
      scope.$apply ->
        sea_fox.set_breakpoints()
        sea_fox.change_position @pageYOffset
        element.removeClass 'hidden'

      #установка breakpoint'ов
      Scroller.add_breakpoints [
        #фиксирование меню
        { event_name: 'sea_fox.fix', offset_top: sea_fox.breakpoints2[0], offset_bottom: sea_fox.breakpoints2[1] - 600 }
        { event_name: 'sea_fox.unfix_top', offset_top: 0, offset_bottom: sea_fox.breakpoints2[0] }
        { event_name: 'sea_fox.unfix_bottom', offset_top: sea_fox.breakpoints2[1] - 600, offset_bottom: sea_fox.breakpoints2[1] + 100500 }
        #показ баннеров
        { event_name: 'sea_fox.show_banner1', offset_top: 0, offset_bottom: sea_fox.breakpoints[0] }
        { event_name: 'sea_fox.show_banner2', offset_top: sea_fox.breakpoints[0], offset_bottom: sea_fox.breakpoints[1] }
        { event_name: 'sea_fox.show_banner3', offset_top: sea_fox.breakpoints[1], offset_bottom: sea_fox.breakpoints[2] }
      ]

    #реагирование на события
    $rootScope.$on 'sea_fox.unfix_top.start', -> scope.$apply -> scope.sea_fox.current_top_position = scope.sea_fox.breakpoints2[0] + 'px'
    $rootScope.$on 'sea_fox.unfix_bottom.start', -> scope.$apply -> scope.sea_fox.current_top_position = scope.sea_fox.breakpoints2[1] - 540 + 'px'
    #смена position fix/relative
    $rootScope.$on 'sea_fox.fix.start', -> scope.$apply -> scope.sea_fox.is_fixed = true
    $rootScope.$on 'sea_fox.fix.stop', -> scope.$apply -> scope.sea_fox.is_fixed = false
    #смена баннеров
    $rootScope.$on 'sea_fox.show_banner1.start', -> scope.$apply -> scope.sea_fox.current_banner_id = 1
    $rootScope.$on 'sea_fox.show_banner2.start', -> scope.$apply -> scope.sea_fox.current_banner_id = 2
    $rootScope.$on 'sea_fox.show_banner3.start', -> scope.$apply -> scope.sea_fox.current_banner_id = 3

    #изменение окна браузера
    $rootScope.$on 'blueShark.1280', ->
      scope.$apply ->
        scope.sea_fox.update_breakpoints()
    $rootScope.$on 'blueShark.1024', ->
      scope.$apply ->
        scope.sea_fox.update_breakpoints()
