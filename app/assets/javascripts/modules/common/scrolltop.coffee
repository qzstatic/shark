angular.module 'shared'
.directive 'scrollTop', (Scroller, $rootScope)->

  controller: ($scope)->
    $scope.scroll_top =
      visible: false

  link: (scope, element, attrs) ->
    #реагирует на загрузку документа
    $rootScope.$on 'blueShark.load', ->
      scope.$apply ->
        Scroller.add_breakpoints [
          { event_name: 'scroll_top.hide', offset_top: 0, offset_bottom: $(window).height() }
          { event_name: 'scroll_top.show', offset_top: $(window).height(), offset_bottom: scope.scroll_top.course_offset + 100500 }
        ]

    $rootScope.$on 'scroll_top.hide.stop', -> scope.$apply -> scope.scroll_top.visibile = true
    $rootScope.$on 'scroll_top.show.start', -> scope.$apply -> scope.scroll_top.visibile = true
