# Директива правого меню
angular.module 'shared'
.directive 'subscribePopup', ($timeout, $rootScope, $compile, $http)->

  controller: ($scope, $element) ->


    userHasSubscription = (obj) ->
      for i in obj.mail_types
        if i.slug == 'newsrelease' then return true else return false
      return false
    referers = ['news.yandex', 'news.google', 'news.rambler', 'news.mail', 'r.mail.', 'news-clck.yandex', ]

    url = document.referrer

    isRequiredReferer = (refs, curRef) ->
      for i in refs
        if curRef.indexOf(i) != -1 then return true else return false

    isNeedASubscription = ->
      if user_options.status == 'guest'
        return true
      if userHasSubscription(user_mailing)
        return false
      else
        return true
      return false

    $scope.is_visible = false

    $scope.show = ->

      if typeof dataLayer != 'undefined'
        dataLayer.push {
          event: 'vedBanner'
          categoryV: 'yellowBanner'
          bannername: 'yellowBanner'
          actionV: 'show'
        }
      $scope.is_visible = true
      $.cookie('subscribe_popup', 'false', { expires: 7, path: '/' })

    $scope.close = ->
      if typeof dataLayer != 'undefined'
        dataLayer.push {
          event: 'vedBanner'
          categoryV: 'yellowBanner'
          bannername: 'yellowBanner'
          actionV: 'close'
        }

      $scope.is_visible = false


    if !$.cookie('subscribe_popup') && isRequiredReferer(referers, url) && isNeedASubscription()
      $scope.show()

  link: (scope, element, attr)->


