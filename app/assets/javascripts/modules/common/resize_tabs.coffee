angular.module 'shared'
.directive 'resizeTabs', ($window, $rootScope, seaSettings, $timeout) ->
  link: ($scope, $element) ->

    getParentwidth = () ->
      return $($element).outerWidth()

    elems = $($element).children()
    elements_collection = elems.filter((index, el) ->
      return $(el).hasClass('disabled')
    )

    getElementsWidth  = () ->
      totalWidth = 0
      elems.each((index, el) ->
        if $(el).is(":visible")
          totalWidth += $(el).outerWidth()
      )
      return totalWidth + elems.length - 1 # elem.length - 1 = суммарный размер отступов

    hideElemes  = () ->
      i  = elements_collection.length - 1
      elements_collection.each((index, el) ->
        if getParentwidth() < getElementsWidth()
          $(elements_collection[i]).hide()
          i--
      )

    showElemes  = () ->
      elements_collection.each((index, el) ->
        if getParentwidth() >  getElementsWidth()
          $(el).show()

      )

    angular.element($window).bind 'load resize', =>
      if ($(window).width() < 1279 )
        hideElemes()
      else
        showElemes()

