# Зебровая акула (лат. Stegostoma fasciatum) — единственный вид семейства зебровых акул (Stegostomatidae).
# Легко узнаваема благодаря пятнистой окраске, длинному хвосту и продольным гребням на коже по бокам.

# Распространена в тропических и субтропических водах Тихого и Индийского океанов, иногда встречается в южной части Японского моря.
#
# ![Carcharodon carcharias](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/Leopard_shark.jpg/350px-Leopard_shark.jpg)

# ##Директива поиска на странице /search
# Включает себя форму extended_search

angular.module 'shared'
.directive 'stegostomaShark',($http, $location, seaSettings, $rootScope, $timeout, $animate) ->
  templateUrl: 'modules_templates/stegostoma.html'
  scope: {}
  controller: ($scope) ->
    #параметры поиска
    $scope.search = {
      query: ''
    }


    $scope.toggle_advanced_options = ->
      $scope.advanced_options_visible = if true == $scope.advanced_options_visible then false else true
    $scope.dateFocus = () ->
      $scope.$emit('extendedSearch.pick_period')
    #формирует поисковую строку на основе формы поиска
    $scope.set_query = (form_obj) ->

      $scope.search.query = form_obj.query
      $scope.search.date_from = form_obj.date_from
      $scope.search.date_to = form_obj.date_to
      #обрабатываем период
      switch form_obj.period
        when 'all'
          delete $scope.search.date_from
          delete $scope.search.date_to
        when 'month'
          $scope.search.date_from = moment().subtract('months', 1).format('YYYY-MM-DD')
          $scope.search.date_to = moment().format('YYYY-MM-DD')
        when 'year'
          $scope.search.date_from = moment().subtract('year', 1).format('YYYY-MM-DD')
          $scope.search.date_to = moment().format('YYYY-MM-DD')
#        when 'custom'
#          $scope.search.date_from = "#{form_obj.from.year}-#{form_obj.from.month}-#{form_obj.from.day}"
#          $scope.search.date_to = "#{form_obj.to.year}-#{form_obj.to.month}-#{form_obj.to.day}"

      #всегда исключаем эти категории
      exclude = []

      #всегда ищем среди материалов
      $scope.search.categories = 'materials'

      #ищем только в газете
      if false == form_obj.source.site && true == form_obj.source.paper
        $scope.search.categories = 'newspaper'

      #исключаем газету
      if true == form_obj.source.site && false == form_obj.source.paper
        exclude.push 'newspaper'

      #если включен флажок по сайту
      if true == form_obj.source.site
        for item, value of form_obj.documents
          if false == value
            if 'articles' == item
              exclude.push 'blogs', 'articles', 'characters', 'quotes', 'columns'
            else
              exclude.push item

      $scope.search.exclude = exclude.join(',')

    #сортировка поиска
    $scope.set_sort = (sort)->
      $scope.search.sort = sort
      $scope.search_start()

    #страница вывода результатов
    $scope.set_from = (from)->
      $scope.search.from = from

    #запускает поиск
    $scope.search_start = ->
      #запускаем показ прелоадера
      $scope.$emit('somniosus.show_preloader', {results: [], stat: 0})
      #подменяем урл
      $location.search($scope.search)
      #получаем результаты поиска
      $timeout ->
        $http.get "#{seaSettings.regulus_url}?q=#{window.location.search.slice(7)}"
        .success (data, status, headers, config) ->

          #генерирует событие отрисовки результатов поиска
          $scope.$emit('somniosus.parse_results', {results: data.found, stat: data.stat})
          $scope.results = data.found
          $scope.total = data.stat.total

        .error (data, status, headers, config) ->
          #console.log 'что-то пошло не так'
      , 0

  link: (scope, element) ->
    #событие на загрузку страницы
    $rootScope.$on 'blueShark.ready', ->
      scope.search = $location.search()
      scope.$emit 'extendedSearch.set_data', $location.search()
      scope.search_start()

    $animate.enabled(false, element);


    $rootScope.$on 'stegostoma.set_from', (event, page)->
      scope.set_from(page * 20 - 20)
      scope.search_start()

