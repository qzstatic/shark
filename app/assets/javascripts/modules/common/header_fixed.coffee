# ##Директива 'плавающего' header'a##
# 44px - поправка на высоту header'а
angular.module 'shared'
.directive 'vedHeaderFixed', ($compile, $window, Scroller, $rootScope, ProperOffsetTop) ->

  controller: ($scope) ->
    $scope.header_fixed =
      visible: false
      toggle: ->

  link: (scope, element) ->
    element.removeClass 'hidden'

    #реагирует на загрузку документа
    $rootScope.$on 'blueShark.load', ->
      scope.$apply ->
        if $('.b-course').length > 0
          scope.header_fixed.course_offset = $('.b-course').offset().top
          scope.header_fixed.visible = angular.element($window)[0].pageYOffset > scope.header_fixed.course_offset


        eventArr = ['header_fix.unfix','header_fix.fix']

        properOffsetTop = new ProperOffsetTop('.b-course', eventArr, (_offset)-> Scroller.add_breakpoints([
          { event_name: 'header_fix.unfix', offset_top: 0, offset_bottom: _offset - 44 }
          { event_name: 'header_fix.fix', offset_top: _offset - 44, offset_bottom: _offset + 100500 }
        ]))


        Scroller.add_breakpoints [
          { event_name: 'header_fix.unfix', offset_top: 0, offset_bottom: scope.header_fixed.course_offset - 44 }
          { event_name: 'header_fix.fix', offset_top: scope.header_fixed.course_offset - 44, offset_bottom: scope.header_fixed.course_offset + 100500 }
        ]
    $rootScope.$on 'header_fix.fix.start', -> scope.$apply -> scope.header_fixed.visible = true
    $rootScope.$on 'header_fix.unfix.start', -> scope.$apply -> scope.header_fixed.visible = false

