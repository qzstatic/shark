# ##Директива новостей##
angular.module 'shared'
.directive 'vedNews', (Scroller, $rootScope, $compile, $timeout) ->
  scope: {}
  controller: ($scope) ->
    $scope.correct_news_array = (element, height, height2) ->
      while height > height2 && $(element).find('li').length >= 2
        $(element).find('li').eq(-1).remove()
        height = $(element).height()


  link: (scope, element, attr) ->
    $compile(element.contents())(scope.$new())

    $rootScope.$on 'blueShark.load', ->
      scope.$apply ->
        elements = $(attr.elements)
        height = 0
        elements.each ()->
          height += $(@).height()
        scope.correct_news_array element, $(element).height(), height


