angular.module 'shared'
.directive 'vedVkPost', ($http, $sce)->
  scope: true
  templateUrl: 'modules_templates/vk_post.html'
  link: (scope, element, attr) ->
    scope.link = attr.vedVkPost
    postIds = scope.link.match(/wall(.*_.*)/)
    if (postIds && postIds.length > 1)
      id = postIds[1]
      if (id.indexOf('?') != -1)
        id = id.substring(0, id.indexOf('?'));

      $http.jsonp("https://api.vk.com/method/wall.getById?posts=#{id}&extended=1&callback=JSON_CALLBACK")
        .then((data) ->
          console.log(data)
          response = data.data.response
          if response.wall?.length > 0
            wall = response.wall[0];
            result = {};
            result['text'] = wall.text;
            attach = wall.attachment;
            if attach?.type == 'photo' && attach.photo
              if attach.photo.src_big
                result['image'] = attach.photo.src_big
              else if (attach.photo.src_small)
                result['image'] = attach.photo.src_small
              else result['image'] = attach.photo.src

            if response.profiles?.length > 0
              result['author'] = "#{response.profiles[0].first_name} #{response.profiles[0].last_name}"
            else if response.groups?.length > 0
              result['author'] = response.groups[0].name
              result['authorPic'] = response.groups[0].photo_medium || response.groups[0].photo
            else
              result['author'] = "John Doe"
            scope.result = result
            scope.result.text = $sce.trustAsHtml scope.result.text
            console.log(scope.result)

      )
