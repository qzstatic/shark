# Директива рисования выпадающего меню
# использутется в 2-х шаблонах
# В одном из них - она вложена в другую директиву, которая своим скоупом может повредить, поэтому скоуп vedHeaderMenu сделан в объекте .header_menu и не изолирован
angular.module 'shared'
.directive 'vedHeaderMenu', ($rootScope, $compile) ->
  controller: ($scope, $timeout) ->
    timeout = 0
    delay = 600

    $scope.header_menu =
      is_visible: false

      check_menu: ->
        $timeout.cancel timeout if  @is_visible?

      show_menu: ->
        @is_visible = true
        $timeout.cancel timeout

      hide_menu: ->
        timeout = $timeout ->
          $scope.header_menu.is_visible = false
        ,
          delay

      fast_hide_menu: ->
        @is_visible = false

  link: (scope, element)->
    $compile(element.contents())(scope.$new())
    element.removeClass 'hidden'

    element.on 'mouseleave', ->
      scope.header_menu.hide_menu()
    element.on 'mouseenter', ->
      scope.header_menu.check_menu()

    $rootScope.$on 'menu_button.click', ->
      scope.header_menu.show_menu()
      scope.$apply()

