# директива позволяет "на лету" вставлять  конструкции типа "lorem ipsum <span ng-click="hide_message()">dolor</span>"
angular.module('shared').directive('bindHtmlCompile', [
  '$compile'
  ($compile) ->
    (scope, element, attrs) ->
      scope.$watch ((scope) ->
        scope.$eval attrs['bindHtmlCompile']
      ), (value) ->
        element.html value
        $compile(element.contents()) scope
        return
      return
])