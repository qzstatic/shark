# ##Директива fullscreen##
# Связь директив происходит при помощи событий.
#
angular.module 'shared'
.directive 'vedFullscreen',($rootScope, Keyboard, $timeout, seaSettings, seaCache, $location) ->
  scope: {}
  templateUrl: 'modules_templates/fullscreen.html'
  controller: ($scope, $element) ->
    $scope.firstTimeOpened = true
    current_url = null
    #Change banner 300x600
    $scope.change_fullscreen_banner = ->
      asd2=[];
      asd2.push(googletag.slot_manager_instance.find("#{seaSettings.fullscreen_banner}"))
      googletag.pubads().refresh(asd2,{changeCorrelator: true});
    #функция показа картинки (работает совместно с директивой imageonload)
    $scope.load_image = ->
      $scope.preloader_visibility = true
      $timeout ->
        $scope.image.visibility = true
        $scope.preloader_visibility = false
      , 500

    #отправка статистики
    $scope.send_statistic = ()->
      url = "#{document.location.pathname}##{seaSettings.prefix}#{$location.path()}"

      if ( !current_url || url != current_url ) && $rootScope.fullscreen_on
        current_url = url
  
        if ga?
          ga('set', 'page', url)
          ga('send', 'pageview')

        if yaCounter3796804?
          yaCounter3796804.hit(url)

        if _tmr?
          _tmr.push({id: "371008", type: "pageView", start: (new Date()).getTime()})

        (new Image()).src = '//counter.yadro.ru/hit?t44.1;r'+escape(document.referrer)+(if (typeof(screen)=='undefined') then '' else ';s'+screen.width+'*'+screen.height+'*'+(if screen.colorDepth then screen.colorDepth else screen.pixelDepth))+';u'+escape(url)+';'+Math.random()

    # закрывает fullscreen
    $scope.close = () ->
      $rootScope.fullscreen_on = false
      Keyboard.remove_listener 'fullscreen'
      $scope.visibility = false
      $location.path("galleries/#{$scope.gallery_id}/normal/#{$scope.current_item + 1}")
      $scope.$emit 'mokarran.reset'
      $scope.$emit 'lamiopsis.reset'
      $scope.$broadcast 'fullscreen.close', {current_item: $scope.current_item, gallery_id: $scope.gallery_id}
      $scope.$emit 'blueShark.jump_to', $scope.offset

    # открывает fullscreen
    $scope.open = (params) ->
      if ($scope.firstTimeOpened)
        $scope.firstTimeOpened = false
        googletag.cmd.push(() ->
          googletag.defineSlot('/136502702/Banner_300x600_galleriesfullpage', [[300, 600], [1, 1]], 'div-gpt-ad-1476267676419-0').addService(googletag.pubads())
          googletag.enableServices()
        )
        $timeout(() ->
          googletag.cmd.push(() ->
            googletag.display('div-gpt-ad-1476267676419-0')
          )
        , 50)

      else
        $scope.change_fullscreen_banner()
      $rootScope.fullscreen_on = true
      Keyboard.remove_listener 'fullscreen'
      $scope.$emit 'blueShark.jump_top'
      #получаем данные галереи
      $scope.visibility = true
      $scope.items = JSON.parse params.items
      $scope.offset = params.offset
      $scope.gallery_id = params.gallery_id
      $scope.current_item = params.current_item
      $scope.title = params.title
      $scope.subtile = params.subtitle
      #подменяем path
      $location.path("galleries/#{$scope.gallery_id}/fullscreen/#{$scope.current_item + 1}")
      $scope.mode = 'gallery'
      $scope.toggle_mode() if 'tiles' == params.fullscreen_mode
      #добавляет события клавиатуры
      Keyboard.add_listener 'fullscreen'
      #перерисовывае высоты элементов
      $scope.$emit 'mokarran.resize', $scope.height
      $scope.$emit 'lamiopsis.resize', $scope.height
      $scope.$emit 'lamiopsis.hide'
      #отправляем статистику
      $scope.send_statistic()

    #перемотка вперед
    $scope.next = ->
      if $scope.items.length > 1
        if $scope.current_item < $scope.items.length - 1
          $scope.current_item += 1
          $scope.image.visibility = false
        else if $scope.current_item == $scope.items.length - 1
          $scope.current_item = 0
          $scope.image.visibility = false
        $location.path("galleries/#{$scope.gallery_id}/fullscreen/#{$scope.current_item + 1}")
        $scope.send_statistic()

    #перемотка назад
    $scope.prev = ->
      if $scope.items.length > 1
        if $scope.current_item > 0
          $scope.current_item -= 1
          $scope.image.visibility = false
        else if $scope.current_item == 0
          $scope.current_item = $scope.items.length - 1
          $scope.image.visibility = false
        $location.path("galleries/#{$scope.gallery_id}/fullscreen/#{$scope.current_item + 1}")
        $scope.send_statistic()

    #переход к элементу
    $scope.goto = (item) ->
      $scope.mode = 'gallery'
      $scope.current_item = item
      $location.path("galleries/#{$scope.gallery_id}/fullscreen/#{$scope.current_item + 1}")

    #переключение режима галереи
    $scope.toggle_mode = ->
      if $scope.mode is 'gallery'
        $scope.mode = 'tiles'
        $location.path("galleries/#{$scope.gallery_id}/fullscreen/#{$scope.current_item + 1}/tiles")
        #перерисовывае высоты элементов
        $scope.$emit 'mokarran.resize', $scope.height
        $scope.$emit 'lamiopsis.resize', $scope.height
        $scope.$emit 'lamiopsis.hide'
      else
        $scope.mode = 'gallery'

#    # Подсвечивает текущую плитку
#    $scope.fade_tiles= (id) =>
#      $(".b-fullscreen-tiles .b-fullscreen-tiles__tile:not(:eq(#{id}))").addClass('fade')
#      return true
#
#    # Подсвечивает все плитки
#    $scope.show_tiles =>
#      $('.b-fullscreen-tiles .b-fullscreen-tiles__tile').removeClass('fade')
#      return true
#
#    # Показывает подсказку
#    $scope.show_tooltip = ->
#      $scope.tooltip_visibility = true
#    # Скрывает подсказку
#    $scope.hide_tooltip = ->
#      $timeout ->
#        $scope.tooltip_visibility = false
#      , 300

  link: (scope, element) ->

    params =
      items: []
      agami_url: seaSettings.agami_url
      length: 0
      visibility: false
      image: {}
      min_width: 1000
      height: 0
      width: 0
      title: ''
      subtitle: ''
      mode: 'gallery'
      jumpTo: 0
      preloader_visibility: true

    for key, value of params
      scope[key] = value

    scope.image =
      max_width: 0
      max_height: 0
      visibility: true
      display: 'none'

    #открывает fullscreen
    $rootScope.$on 'fullscreen.open', (el, params)->
      scope.open params

    # обрабатывает события клавиатуры
    $rootScope.$on 'fullscreen.39', ->
      scope.$apply -> scope.next()

    $rootScope.$on 'fullscreen.37', ->
      scope.$apply -> scope.prev()

    $rootScope.$on 'fullscreen.27', ->
      scope.$apply -> scope.close()

    $rootScope.$on 'blueShark.resize',(el, params) ->
      scope.$apply ->
        scope.image.max_width = if params.width > scope.min_width then params.width - 340 else scope.min_width - 340
        scope.image.max_height = params.height - 30
        scope.height = params.height
        scope.width = scope.image.max_width

        if scope.visibility
          scope.$emit 'mokarran.resize', params.height
          scope.$emit 'lamiopsis.resize', params.height

    $rootScope.$on 'fullscreen.close2', ->
      scope.close() if true == scope.visibility

    $rootScope.$on 'fullscreen.goto', (el, params)->
      scope.goto(params)

    $rootScope.$on 'blueShark.load', (el, params) ->
      scope.$apply ->
        scope.image.max_width = if params.width > scope.min_width then params.width - 340 else scope.min_width - 340
        scope.image.max_height = params.height - 30
        scope.height = params.height
        scope.width = scope.image.max_width
        if scope.visibility
          scope.$emit 'mokarran.resize', params.height
          scope.$emit 'lamiopsis.resize', params.height

#
#    $rootScope.$on 'carcharodon.13', ->
#      scope.$apply ->
#        scope.goto scope.current_item if scope.mode == 'tiles'
