angular.module 'shared'
# Директива результатов поиска
.directive 'search', ->
  controller: ($scope, $timeout, $http, seaSettings) ->
    $scope.search =
      active : false
      focus : false
      results : []
      timer: ""
      delay: 1000

      open: =>
        $scope.search.active = true
        $('#query').focus()
        return true

      close: ->
        $scope.search.active = false
        $scope.search.results.length = 0

      focus: ->
        $scope.search.focus = true

      toggle: ->
        $scope.search.active = if $scope.search.active is true then false else true

      search: (query)->
        $timeout.cancel $scope.search.timer

        $scope.search.timer = $timeout ()->
          $http.get("#{seaSettings.regulus_url}?q=#{query}&sort=date&categories=materials")
          .success (data, status, headers, config) ->
            $scope.search.results = data.found
        , $scope.search.delay
