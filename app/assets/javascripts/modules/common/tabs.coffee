# Директива правого меню
angular.module 'shared'
.directive 'vedTabs', ($timeout, $rootScope, $compile)->
  scope: {}

  controller: ($scope, $element) ->
    $scope.current_tab = 0
    $scope.height = 620
    $scope.load = true

    $scope.correct_array = (classname) ->
      #для определения высоты элемента требуется небольшая задержка
      height = $(".#{classname}").height()
      if height > 552
        while height > 552
          elements = $(".#{classname} li")
          elements.last().remove()
          height = $(".#{classname}").height()

  link: (scope, element, attr)->
    $compile(element.contents())(scope.$new())
    $rootScope.$on 'blueShark.ready', (el,params) ->
      scope.$apply ->
        scope.correct_array('b-tabs__news')
        scope.load = false