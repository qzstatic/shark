angular.module 'shared'
.directive 'vedBannerScroll', ($rootScope, $compile, $window) ->
  link: (scope, element) ->
    scope.position = (bannerHeight, menuOffset, contentHeight, contentOffset, bannerOffset, alsoReadHeight, commentsHeight) ->
      pageOffset = angular.element($window)[0].pageYOffset
      isFixed = (pageOffset + menuOffset > bannerOffset) &&
        (pageOffset + menuOffset - alsoReadHeight - commentsHeight + $(element).height() < contentOffset + contentHeight)
      isScrolled = pageOffset + bannerHeight + menuOffset >= contentHeight + contentOffset
      $(element).toggleClass 'fixed', isFixed && !isScrolled
      $(element).toggleClass 'scrolled', isScrolled

      [isScrolled, isFixed]

    $rootScope.$on 'blueShark.load', ->
      $documentBody = $('.b-document__left')
      if $documentBody.length > 0

        contentHeight = $documentBody.height()
        contentOffset = $documentBody.offset().top
        menuOffset = 70
        bannerOffset = $(element).offset().top
        bannerHeight = $(element).height()

        alsoReadHeight = +$('.b-also-read').outerHeight(true)
        commentsHeight = +$('[ved-popular]').height()

        scope.$on 'document.heightChanged', ()->
          contentHeight = $documentBody.height()
          scope.position  bannerHeight, menuOffset, contentHeight, contentOffset, bannerOffset, alsoReadHeight, commentsHeight

        angular.element($window).bind 'scroll', ->

          if contentHeight > 1800
            scope.position  bannerHeight, menuOffset, contentHeight, contentOffset, bannerOffset, alsoReadHeight, commentsHeight
