# ##Директива счетчиков социальных сетей##
angular.module 'shared'
.directive 'socialCounters', ($http, $timeout, seaSettings) ->
  scope: true

  link: (scope, element, attr) ->
    scope.showCommentsCount = false
    $http.get("#{seaSettings.hawk_url}/stats/#{attr.socialCountersUrl}?id=#{attr.socialCountersId}")
    .then (response) ->
      scope.showSocialCount = true
      scope.fb = if response.data.fb_share_count then response.data.fb_share_count + response.data.fb_comment_count else ''
      scope.vk = if response.data.vk_share_count then response.data.vk_share_count else ''
      scope.twitter = if response.data.twitter_share_count then response.data.twitter_share_count else ''
