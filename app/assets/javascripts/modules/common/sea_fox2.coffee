#Alopias vulpinus (Sea fox) - Лисья акула. Крупная акула, достигающая 6 метров в длину и веса более 510 кг.
# Почти половину длины рыба составляет ее длинный хвост.
#
#![Sea fox](http://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Alopias_vulpinus.jpg/350px-Alopias_vulpinus.jpg)
#
# ##Директива плавающих элементов правого меню##
#
angular.module 'shared'
.directive 'seaFox2Shark',($window, Scroller, $rootScope) ->
  scope: true
  controller: ($scope) ->

  link: (scope, element) ->
    #добавляет ключевые точки
    Scroller.add_breakpoints [
      { event_name: "#{scope.name}.fix", offset_top: scope.breakpoints.start, offset_bottom: scope.breakpoints.end }
      { event_name: "#{scope.name}.unfix_top", offset_top: 0, offset_bottom: scope.breakpoints.start }
      { event_name: "#{scope.name}.unfix_bottom", offset_top: scope.breakpoints.end, offset_bottom: scope.breakpoints.end + 100500 }
    ]

    #генерирует события начало и конца прокручивания баннера (position: fixed)
    $rootScope.$on "#{scope.name}.fix.start", -> scope.$apply -> $(element).addClass('fixed')
    $rootScope.$on "#{scope.name}.fix.stop", -> scope.$apply -> $(element).removeClass('fixed')

    $rootScope.$on "#{scope.name}.unfix_top.start", -> scope.$apply -> $(element).css({top: scope.breakpoints.start - 265 + 'px'})
    $rootScope.$on "#{scope.name}.unfix_bottom.start", -> scope.$apply -> $(element).css({top: scope.breakpoints.end - 265 + 'px'})