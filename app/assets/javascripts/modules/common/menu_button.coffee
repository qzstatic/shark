# кнопка открытия меню в header'е
angular.module 'shared'
.directive 'vedMenuButton', () ->

  link: ($scope, $element)->
    $element.on 'click', ->
      $scope.$emit('menu_button.click')
