angular.module 'shared'
# Директива вывода результатов поиска
.directive 'somniosusShark', ($rootScope, $timeout, $http, seaSettings)->
  templateUrl: 'modules_templates/somniosus.html'
  controller: ($scope) ->

    $scope.somniosus =
      results: []
      agami_url: seaSettings.agami_url
      disable: false

      get_class: (data)->
        switch(data)
          when 'articles'  then 'b-article b-article_list'
          when 'quotes'     then 'b-quote b-quote_list'
          when 'columns'    then 'b-column b-column_list'
          when 'blogs'      then 'b-blog b-blog_list'
          when 'characters' then 'b-character b-character_list'
          when 'news'       then 'b-news-list'
          else 'b-article b-article_list'

      to_page: ()->
        $scope.$emit 'stegostoma.set_from', $scope.somniosus.from
        $scope.$emit 'blueShark.jump_to', 350

  link: (scope) ->
    scope.somniosus.disable = true

    $rootScope.$on 'somniosus.parse_results', (event, params)->
      $timeout ->
        scope.somniosus.disable = false
        scope.somniosus.results = params.results
        scope.somniosus.stat = params.stat
        scope.somniosus.from = params.stat.from / 20 + 1
      , 500

    $rootScope.$on 'somniosus.show_preloader', ->
      scope.somniosus.disable = true