# Акула-молот гигантская (Sphyrna mokarran) cамый крупный представитель семейства - достигает 4,5 и даже 6 м в длину.
# Она широко распространена в тропических водах Тихого, Индийского и Атлантического океанов, но нигде не
# достигает высокой численности. Другие молотоголовые акулы имеют меньшие размеры — до 3,5—4,2 м при весе около 450 кг.
#
# ![Carcharodon carcharias](https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Hammerhead_shark.jpg/350px-Hammerhead_shark.jpg)

# ##Директива работы с body##
angular.module 'shared'
.directive 'mokarranShark',($window, $rootScope) ->
  controller: ($scope) ->
    $scope.mokarran =
      height: 'inherit'
      active: false

  link: (scope, element) ->
    # изменяет размер body
    # 1. el - элемент <br>
    # 2. params - высота элемента
    $rootScope.$on 'mokarran.resize', (el, params) =>
      scope.mokarran.height = params
      scope.mokarran.active = true

    # сбрасывает настройки body
    $rootScope.$on 'mokarran.reset', ->
      scope.mokarran.height = 'inherit'
      scope.mokarran.active = false
