#форма авторизации (сейчас в paywall)
angular.module 'formControllers'
.controller 'signInController', ($scope, $http, $element) ->

  $scope.submit = () ->
    form = $scope['loginForm']
    form.$submit = true

    if form.$valid
      $http.post '/sign_in_with_password.json', {
        utf8:               $($element).find("input[name='utf8']").val()
        authenticity_token: $($element).find("input[name='authenticity_token']").val()
        email:              form.loginEmail.$viewValue
        password:           form.loginPassword.$viewValue
      }
      .success (data, status, headers, config) ->
        window.location.reload()
      .error (data, status, headers, config)->
        $scope.password = ''
        $scope.wrong_data = true
    false
