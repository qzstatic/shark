#форма поиска
angular.module 'formControllers'
.controller 'searchController', ($scope, $http, $element, seaSettings) ->

  $scope.submit = () ->
    if $scope.search.query == undefined then query = '' else query = $scope.search.query
    string = "#{seaSettings.shark_url}/search?query=#{encodeURIComponent(query)}&sort=date&categories=materials"
    window.location = string
    $scope.$emit 'stegostoma.start_search', {string: string, query: query}
