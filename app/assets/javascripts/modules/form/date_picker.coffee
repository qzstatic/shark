# Универсальный datepicker

angular.module 'formControllers'
.directive 'vedDatePicker', () ->
  templateUrl: 'modules_templates/date_picker.html'
  restrict: 'E'
  scope: {
    dateValue: '=',
    maxDate: '=',
    minDate: '='
  }
  controller: ($scope, $timeout)->
    $scope.defaultDate=$scope.dateValue
    $scope.calendar = {}
    today = new Date()
    cur_year = today.getFullYear()
    $scope.years = [1999..cur_year];
    $scope.months = [
      {name: 'Январь',   month: 1},
      {name: 'Февраль',  month: 2},
      {name: 'Март',     month: 3},
      {name: 'Апрель',   month: 4},
      {name: 'Май',      month: 5},
      {name: 'Июнь',     month: 6},
      {name: 'Июль',     month: 7},
      {name: 'Август',   month: 8},
      {name: 'Сентябрь', month: 9},
      {name: 'Октябрь',  month: 10},
      {name: 'Ноябрь',   month: 11},
      {name: 'Декабрь',  month: 12}
    ]
    $scope.normalizeValue = (value)->
      if value < 10 then '0'+value else value+''
    $scope.init = (data) ->

#      if !$scope.maxDate?
#        $scope.maxDate = new Date()
#      if !$scope.minDate?
#        $scope.minDate = Date.parse('Jan 1, 1999');
      data_array = if ($scope.dateValue? && $scope.dateValue.split('-').length == 3) then $scope.dateValue.split('-') else [today.getFullYear(), $scope.normalizeValue(today.getMonth()+1), $scope.normalizeValue(today.getDate())]
      $scope.calendar.month = $scope.months[data_array[1]*1 - 1]
      $scope.calendar.year = $scope.years[$scope.years.indexOf(data_array[0]*1)]
      $scope.presentDate = data_array.join('-')
      $scope.defaultDate =  data_array.join('-')
      console.log(data_array)
      $scope.dateValue = $scope.defaultDate

    $scope.init()
    $scope.advanced_options_visible = true

#    $scope.set_month = (data) ->
#      date_array = $scope.presentDate.split('-')
#      if data.month < 10 then month = "0#{data.month}" else month = data.month
#      $scope.presentDate = "#{$scope.calendar.year}-#{month}-#{date_array[2]}"
#      #    console.log "present_date =" + $scope.presentDate
#      #      $scope.set_newsreleases_data($scope.presentDate)
#      $scope.$emit 'calendar.change', $scope.presentDate
#
#    #преключаем год
#    $scope.set_year = (data) ->
#
#      console.log data
#      date_array = $scope.presentDate.split('-')
#      if $scope.calendar.month.month < 10 then month = "0#{$scope.calendar.month.month}" else month = $scope.calendar.month.month
#      #    console.log date_array[2]
#      $scope.presentDate = "#{data}-#{month}-#{date_array[2]}"
##      $scope.set_newsreleases_data($scope.presentDate)
#      $scope.$emit 'calendar.change', $scope.presentDate

    $scope.validate = () ->
#      $scope.datePickerShow = false
    $scope.chooseDate = () ->

      if $scope.defaultDate?
        current_data = $scope.defaultDate.split('-')
#        $scope.dateValue = $scope.defaultDate
        console.log(current_data)
        $scope.$emit ('calendar.closing')
        $scope.$emit ('calendar.value_changed')
      $timeout ()->
          $scope.datePickerShow = false
        200
    $scope.preventTyping = (e) ->
      e.preventDefault()
      false

  link: (scope, element) ->
    $(document).on 'click', (e) ->
      if (element != e.target && !element[0].contains(e.target))
        if scope.datePickerShow
          scope.$emit ('calendar.closing')
        scope.datePickerShow= false
        scope.$apply()

