angular.module 'formControllers'
.controller 'extendedSearchController', ($scope, seaSettings, $rootScope, $timeout, $location) ->
  #объект формы
  $scope.extendedSearch =
    query: ''
    period: 'all'
    from:
      day: ''
      month: ''
      year: ''
    to:
      day: ''
      month: ''
      year: ''
    source:
      site: true
      paper: true
    documents:
      news: true
      articles: true
      galleries: true
      video: true
    data_error: false
  today = new Date()

  $scope.normalizeValue = (value)->
    if value < 10 then '0'+value else value+''
  today_string = today.getFullYear()+'-'+ $scope.normalizeValue(today.getMonth()+1)+'-'+$scope.normalizeValue(today.getDate())
  $scope.extendedSearch.date_from = $location.search().date_from || today_string
  $scope.extendedSearch.date_to = $location.search().date_to || today_string
  $scope.today_string = today_string
  $rootScope.$on 'calendar.value_changed', (event, params)->

    $scope.extendedSearch.period = 'custom'

  #расставляет галки в форме поиска
  $rootScope.$on 'extendedSearch.set_data', (event, params)->
    $scope.extendedSearch.query = decodeURIComponent params.query || ''

    if params.date_from? && params.date_to?
      $scope.extendedSearch.period = 'custom'

    if params.date_from?
      data = params.date_from.split('-')
      $scope.extendedSearch.from =
        day: data[2]
        month: data[1]
        year: data[0]

    if params.date_to?
      data = params.date_to.split('-')
      $scope.extendedSearch.to =
        day: data[2]
        month: data[1]
        year: data[0]

    #вычисляем газету
    if params.categories? && 'newspaper' == params.categories
      $scope.extendedSearch.source.paper = true
      $scope.extendedSearch.source.site = false

    if params.exclude?
      #ищем статьи
      for item of $scope.extendedSearch.documents
        if params.exclude.indexOf(item) > -1
          $scope.extendedSearch.documents[item] = false

  #устанавливает фокус на первом элементе extendedSearch.from.day
  $scope.focus_day = ->
    $timeout ->
      $('#custom_period').focus()
    , 0

  #расстановка радиокнопок в периоде
  $scope.clear_custom_period = (period)->
    if 'all' == period || 'month' == period || 'year' == period
      $scope.extendedSearch.from =
        day: ''
        month: ''
        year: ''

      $scope.extendedSearch.to =
        day: ''
        month: ''
        year: ''

  #валидация периода на число
  $scope.check_custom_period = (period)->

    #запускаем поиск только если указаны правильные даты
    for item, value of $scope.extendedSearch[period]
      if '' == value || (value * 1) % 1 != 0
        $scope.extendedSearch.data_error = true

  #переводит радиокнопку в положение custom
  $scope.set_custom_period = ()->
    $scope.extendedSearch.period = 'custom'

  #расстановка галок в источнике поиска
  $scope.check_source = (source)->
    if ('site' == source && false == $scope.extendedSearch.source.paper) || ('paper' == source && false == $scope.extendedSearch.source.site)
      $scope.check_documents()
      return true

  #расстановка галок в типе материалов
  $scope.check_documents = (document)->
    #отключаем галереи, видео и новости для газеты
    if true == $scope.extendedSearch.source.paper && false == $scope.extendedSearch.source.site
      $scope.extendedSearch.documents.galleries = false
      $scope.extendedSearch.documents.video =  false
      $scope.extendedSearch.documents.news =  false

      #дизейблим кнопки
      if 'news' == document || 'video' == document || 'galleries' == document
        return true
      else if 'articles'
        return false

  $scope.submit = ->
    #очищаем ошибки custom периода
    $scope.extendedSearch.data_error = false
    #валидация custom периода
    if $scope.extendedSearch.period == 'custom'

#      $scope.check_custom_period('from')
#      $scope.check_custom_period('to')

      #запускаем поиск только если верны даты
      if $scope.extendedSearch.data_error == false
        $scope.set_query($scope.extendedSearch)
        $scope.search_start()

    else
      $scope.set_query($scope.extendedSearch)
      $scope.search_start()
