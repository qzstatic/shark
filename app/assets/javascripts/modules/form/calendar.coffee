angular.module 'formControllers'
.controller 'calendarController', ($scope, $http, $timeout, $compile, $filter, seaSettings) ->
  today = new Date()
  cur_year = today.getFullYear()
  $scope.years = [1999..cur_year];

  $scope.months = [
    {name: 'Январь',   month: 1},
    {name: 'Февраль',  month: 2},
    {name: 'Март',     month: 3},
    {name: 'Апрель',   month: 4},
    {name: 'Май',      month: 5},
    {name: 'Июнь',     month: 6},
    {name: 'Июль',     month: 7},
    {name: 'Август',   month: 8},
    {name: 'Сентябрь', month: 9},
    {name: 'Октябрь',  month: 10},
    {name: 'Ноябрь',   month: 11},
    {name: 'Декабрь',  month: 12}
  ];

  #инициализуемся
  $scope.init = (data) ->
    $scope.presentDate = data
    $scope.defaultDate = data
    $scope.maxDate = new Date()

    $timeout ->
#      console.log data
      data_array = data.split("-")
      $scope.calendar.month = $scope.months[data_array[1]*1 - 1]
      $scope.calendar.year = $scope.years[$scope.years.indexOf(data_array[0]*1)]
#      console.log $scope.presentDate
      $scope.set_newsreleases_data(data)
    , 200
  #получаем даты выхода газеты
  $scope.set_newsreleases_data = (data)->
    current_data = data.split('-');
    $http.get "/newspaper/#{current_data[0]}/#{current_data[1]}/calendar"
    .success (data, status, headers, config)->
      $scope.data_hash = data
      disabled_array = []
      for day in [1..31]
        disabled_array.push "#{current_data[0]}-#{current_data[1]}-#{if day < 10 then '0' + day else day}"

      for value of $scope.data_hash
        index = disabled_array.indexOf "#{current_data[0]}-#{current_data[1]}-#{if value < 10 then '0' + value else value}"
        disabled_array.splice(index, 1)

      $scope.data_hash
      $scope.disabledDates = disabled_array
#      console.log disabled_array

    .error (data, status, headers, config)->
      console.log 'что-то пошло не так'

#  $scope.set_newsreleases_data = ()->
##    console.log $scope.data_hash
#    cells = $('.pickadate-cell .pickadate-enabled');
#    cells.each ()->
#      for value, index of $scope.data_hash
#        if value == $(@).text()
#          $(@).addClass('pickadate-hover')

  #преходим на определенную дату
  $scope.goto_newsrelease = ()->
    if $scope.defaultDate != undefined
      current_data = $scope.defaultDate.split('-')
      console.log current_data
      uri = $filter('uri')('http://' + $scope.data_hash["#{ current_data[2] * 1 }"])
      window.location = seaSettings.shark_url + uri if uri != ''

  #преключаем месяц
  $scope.set_month = (data) ->
    date_array = $scope.presentDate.split('-')
    if data.month < 10 then month = "0#{data.month}" else month = data.month
    $scope.presentDate = "#{$scope.calendar.year}-#{month}-#{date_array[2]}"
#    console.log "present_date =" + $scope.presentDate
    $scope.set_newsreleases_data($scope.presentDate)
    $scope.$emit 'calendar.change', $scope.presentDate

  #преключаем год
  $scope.set_year = (data) ->
    console.log $scope.presentDate
    date_array = $scope.presentDate.split('-')
    if $scope.calendar.month.month < 10 then month = "0#{$scope.calendar.month.month}" else month = $scope.calendar.month.month
#    console.log date_array[2]
    $scope.presentDate = "#{data}-#{month}-#{date_array[2]}"
    $scope.set_newsreleases_data($scope.presentDate)
    $scope.$emit 'calendar.change', $scope.presentDate


#    $rootScope.$on 'calendar.render', (event, params)->
#    $scope.presentDate = params