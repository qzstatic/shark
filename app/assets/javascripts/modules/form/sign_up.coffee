#форма регистрации (сейчас в paywall)
angular.module 'formControllers'
.controller 'signUpController', ($scope, $http, $element) ->
  $scope.same_user = false
  $scope.submit = () ->
    form = $scope['registrationForm']
    form.$submit = true

    if form.$valid
      $http.post '/sign_up_with_password.json', {
        utf8:               $($element).find("input[name='utf8']").val()
        authenticity_token: $($element).find("input[name='authenticity_token']").val()
        user_with_password: {
          email: form.registrationEmail.$viewValue
          password: form.registrationPass.$viewValue
          password_confirmation: form.registrationPassCheck.$viewValue
          nickname: form.registrationNickName.$viewValue
          last_name: form.registrationSecondName.$viewValue
          first_name: form.registrationName.$viewValue
          phone: form.registrationTel.$viewValue
          rules: if form.registrationCheckbox.$viewValue then "1" else "0"
        }
      }
      .success (data, status, headers, config) ->
        window.location.reload()

      .error (data, status, headers, config)->
        console.log 'что-то пошло не так'
        $scope.same_user = true
    false