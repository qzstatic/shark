#форма подписки на новости
angular.module 'formControllers'
.controller 'subscribeFormController', ($scope, $http, $element, seaSettings) ->
  $scope.input_is_visible = true
  $scope.hide_message = () ->
    $scope.message_class = ''
    $scope.input_is_visible = true
  $scope.submit = () ->

    if typeof dataLayer != 'undefined'
      dataLayer.push {
        event: 'vedBanner'
        categoryV: 'yellowBanner'
        bannername: 'yellowBanner'
        actionV: 'submit'
      }
    $scope.subject = ''
    $http.post "#{seaSettings.lago}/subscribe_ajax/newsrelease", { email: $scope.email}
    .success (data, status) ->
      $scope.input_is_visible = false
      $scope.subject = 'Спасибо! Проверьте Вашу почту.<br>На указанный адрес отправлено письмо для подтверждения подписки.'
      $scope.message_class = "b-subscribe-popup__message_success"
      if typeof dataLayer != 'undefined'
        dataLayer.push {
          event: 'vedBanner'
          categoryV: 'yellowBanner'
          bannername: 'yellowBanner'
          actionV: 'subscribe'
        }

    .error (data, status) ->
      if data.errors.indexOf('already subscribed') != -1
        $scope.subject = 'Этот адрес уже подписан на рассылку.<br><span class="b-subscribe-popup__text_back" ng-click="hide_message()">Изменить.</span>'
        $scope.message_class = "b-subscribe-popup__message_warning"
        $scope.input_is_visible = false
        if typeof dataLayer != 'undefined'
          dataLayer.push {
            event: 'vedBanner'
            categoryV: 'yellowBanner'
            bannername: 'yellowBanner'
            actionV: 'already_exist'
          }
      else
        $scope.subject = 'Внимание! Введен некорректный e-mail.<br><span class="b-subscribe-popup__text_back" ng-click="hide_message()">Вернуться.</span>'
        $scope.message_class = "b-subscribe-popup__message_error"
        $scope.input_is_visible = false
        dataLayer.push {
          event: 'vedBanner'
          categoryV: 'yellowBanner'
          bannername: 'yellowBanner'
          actionV: 'mistake'
        }
