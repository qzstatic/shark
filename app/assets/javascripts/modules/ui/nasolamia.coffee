#директива, исправляющая некоторые параметры документа при его выкусывании из рубрики
# используется в Фото и видео
# думаю, что будет глючить с не-статьями
angular.module 'ui'
.directive 'nasolamiaShark', ($compile, $rootScope, seaSettings)->
  scope: {}
  controller: ($scope) ->

  link: (scope, element)->
    $compile(element.contents())(scope.$new())
    $items = $(element).find('li:visible')
    if $items.length > 4
      $items.eq(-1).hide()
      $items = $(element).find('li:visible')
    $item_class =  $items.attr('class').split(' ')[0]
    for element_class in ["#{$item_class}_level1", "#{$item_class}_border-top","#{$item_class}_border-bottom", "#{$item_class}_level1_mutate-to-level2"]
      $items.eq(1).removeClass(element_class)

    for element_class in ["#{$item_class}_padding-bottom","#{$item_class}_padding-top", "#{$item_class}_mutate-to-non-border-top"]
      $items.eq(1).addClass(element_class)

    $items.eq(1).find('.hide-on-medium-screen').remove()
    $items.eq(1).find('.show-on-medium-screen').removeClass('show-on-medium-screen')
    $items.eq(1).addClass("#{$item_class}_level2")

    $rootScope.$on 'blueShark.1024', ->
      $items.eq(1).css({'min-height': $items.eq(0).height() + 6 + 'px'})

    $rootScope.$on 'blueShark.1280', ->
      $items.eq(1).css({'min-height': 'inherit'})

    $rootScope.$on 'blueShark.load', (event, params)->
      $items.eq(1).css({'min-height': $items.eq(0).height() + 6 + 'px'}) if params.width < seaSettings.medium