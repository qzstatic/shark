angular.module 'ui'
.service 'Scroller', () ->

  # Класс {Scroller} отвечает за работу со скроллом сайта
  # на основе входных данных он гененирует 2 события event_name.start и
  # event_name.stop, которые потом можно обрабатывать в соответствующих директивах

  class Scroller
    breakpoints = {}

    # Инициализирует {Scroller}
    constructor: ->

    # Добавляет breakpoint в объект breakpoints
    #
    # @param [string] event название события
    # @param [number] offset_top начало отслеживаемого события
    # @param [number] offset_bottom конец отслеживаемого события
    # @param [boolean] active флаг, который следит, чтобы  $scope.$emit выполнился только 1 раз

    add_breakpoint: (event_name, start, stop)->
      breakpoints[event_name] =
        start: start
        stop: stop
        active: false

    # Добавляет breakpoint'ы из массива event_array
    #
    # @param event_array массив событий

    add_breakpoints: (event_array) ->
      for item in event_array
        @add_breakpoint item.event_name, item.offset_top, item.offset_bottom


    # Удаляет breakpoint
    #
    # @param [number] event название события

    remove_breakpoint: (event_name) ->
      delete breakpoints[event_name]

    # Отдает список всех breakpoint'ов
    #
    # @return [Object] массив breakpoint'ов
    breakpoints: -> breakpoints

  new Scroller

# Директива, реализующая работу со скроллом и генерацию событий
.directive 'vedScroller', ->

  controller: ($scope, $window, Scroller) ->
    angular.element($window).bind "scroll load", ->
      for event_name, event_params of Scroller.breakpoints()
        if event_params.start <= @pageYOffset <= event_params.stop
          if event_params.active == false
            event_params.active = true
            $scope.$emit event_name + '.start'
        else
          $scope.$emit event_name + '.stop'
          event_params.active = false






