
angular.module 'ui'
# Директива новости на странице 'Все новости'
.directive 'newsline', ($compile, Scroller, $timeout, $rootScope, seaSettings, $http, $location, $window)->
  scope : {}
  controller: ($scope) ->
    $scope.open = false
    $scope.shark_url = seaSettings.shark_url
    $scope.news_url = ''
    $scope.element = ''
    $scope.text_flag = false
    $scope.title = ''
    $scope.link =
      open: true
      focus: false
      timer: 0

    $scope.toggle_tab = (e) ->
      if not window.history.pushState?
        e.preventDefault();
      $scope.open = if $scope.open == false then true else false

    $scope.toggle_link = ->
      console.log 'toggle link'
      $scope.link.open = if $scope.link.open == false then true else false

      $scope.link.timer = $timeout ->
        $scope.link.open = false
      ,
      3000

    $scope.update_timer = ->
      $timeout.cancel($scope.link.timer)

    $scope.close_link = ->
      $scope.link.timer = $timeout ->
        $scope.link.open = false
      ,
        400

    $scope.add_body = ->
      prevUrl = $location.absUrl()

    #работа с метатегами
      $('title').remove();
      $('link[rel="canonical"]').remove();
      $('meta[property="og:title"]').remove()
      $('meta[property="og:url"]').remove()
      $('head').append("<title>ВЕДОМОСТИ - #{$scope.title}</title>")
      $('head').append("<link href='#{$scope.canonical_url}' rel='canonical' />")
      $('meta[property="og:type"]').after("<meta content='#{$scope.title}' property='og:title'>")
      $('meta[property="og:type"]').after("<meta content='#{$scope.canonical_url}' property='og:url'>")

      #добавление тела документа
      if $scope.text_flag == false
        result = $http.get $scope.news_body_url
        .success (data, status, headers, config) ->
          $scope.text_flag = true
          body = $compile(data)($scope)
          $scope.element.find('.b-news-item__text').append body
          # Отправка дополнительной статистики
          if ga?
            ga('set', 'page', $location.url())
            ga('send', 'pageview')
            ga('set', 'location', $location.absUrl())

          if _tmr?
            _tmr.push({id: "371008", type: "pageView", start: (new Date()).getTime(), referrer:prevUrl})
          (new Image()).src = '//counter.yadro.ru/hit?t44.1;r'+escape(prevUrl)+(if (typeof(screen)=='undefined') then '' else ';s'+screen.width+'*'+screen.height+'*'+(if screen.colorDepth then screen.colorDepth else screen.pixelDepth))+';u'+escape($location.absUrl())+';'+Math.random()

          if yaCounter3796804?
            yaCounter3796804.hit("#{seaSettings.shark_url}#{$location.url()}")

          fire_event([{ doc_id: $scope.id  }]);
          $scope.$emit('document.heightChanged')

        .error ()->
          console.log 'что-то пошло не так'
      else
        $timeout ()->
          $scope.$emit('document.heightChanged')
        , 200



  link: (scope, element, attrs) ->
    $compile(element.contents())(scope.$new())
    scope.text_flag = attrs.textBody == 'true' ? true : false
    scope.element = $(element)
    scope.id = attrs.id

    $(element).find('.b-news-item__link').removeClass 'hidden'
    scope.news_body_url = "#{seaSettings.shark_url}/body#{attrs.url}"
    scope.canonical_url = "#{$window.location.protocol}#{seaSettings.shark_url}#{attrs.url}"
    scope.title = attrs.title

    $rootScope.$on 'newsline.open', (event, id)->
      if id == attrs.id * 1
        scope.open = true
        scope.add_body()
        element.find('.b-news-item__text').removeClass("showme")
