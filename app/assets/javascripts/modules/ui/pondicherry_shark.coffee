#директива, исправляющая некоторые параметры документа при его выкусывании из рубрики
# используется в Фото и видео
# думаю, что будет глючить с не-статьями
angular.module 'ui'
.directive 'pondicherryShark', ($compile)->
  scope: {}
  controller: ($scope) ->

  link: (scope, element)->
    $compile(element.contents())(scope.$new())
    $items = $(element).find('li:visible')
    if $items.length > 6
      $items.eq(-1).hide()
      $items = $(element).find('li:visible')
    for i in [0,1,2]
      for element_class in ['b-article_padding-top','b-article_border-top','b-article_mutate-to-border-top', 'b-article_mutate-to-padding-top', 'b-test_padding-top','b-test_border-top','b-test_mutate-to-border-top', 'b-test_mutate-to-padding-top']
        $items.eq(i).removeClass(element_class)

    for element_class in ['b-article_mutate-to-border-top','b-article_mutate-to-border-top','b-article_mutate-to-padding-top','b-test_mutate-to-border-top','b-test_mutate-to-border-top','b-test_mutate-to-padding-top']
      $items.eq(2).addClass(element_class)