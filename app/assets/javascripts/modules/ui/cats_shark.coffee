# Обыкновенная кошачья акула, или европейская кошачья акула, или мелкопятнистая кошачья акула, или
# морской пёс (лат. Scyliorhinus canicula) — распространённый вид морских хрящевых рыб семейства кошачьих
# акул отряда кархаринообразных. Эндемик северной и центрально-восточной части Атлантического океана.
# Максимальная длина 1 м.
#
# ![](https://upload.wikimedia.org/wikipedia/commons/thumb/2/2b/A_dogfish_has_rolled.jpg/350px-A_dogfish_has_rolled.jpg)


# ##Директива слайдера газеты##
# использует скоуп ved-magazine! в основном параметр $scope.pages <br>
# использует методы директивы river-shark

angular.module 'ui'
.directive 'catsShark', ($timeout, seaSettings) ->
  controller: ($scope) ->
    # параметры слайдера
    $scope.cats_shark =
      width: 1116
      next_visible: true
      prev_visible: false
      item_padding: seaSettings.col2
      item_width: 288
      slider_width: 828
      length: $scope.pages.length || 0


      # перематывает слайдер вперед
      next: ->
        $scope.cats_shark.left += $scope.cats_shark.step
        if Math.abs($scope.cats_shark.slider_inner_width - $scope.cats_shark.left) > $scope.cats_shark.slider_width -  $scope.cats_shark.item_padding * 2
          $scope.cats_shark.next_visible = true
          $scope.cats_shark.prev_visible = true
        else
          $scope.cats_shark.left = $scope.cats_shark.slider_inner_width - $scope.cats_shark.slider_width + 2 * $scope.cats_shark.item_padding
          $scope.cats_shark.next_visible = false
        $scope.cats_shark.update_counter()

      # перематывает слайдер назад
      prev: ->
        $scope.cats_shark.left -= $scope.cats_shark.step
        if Math.abs $scope.cats_shark.left < $scope.cats_shark.item_padding * 2
          $scope.cats_shark.left = -$scope.cats_shark.item_padding * 2
          $scope.cats_shark.prev_visible = false
        else
          $scope.cats_shark.next_visible = true
          $scope.cats_shark.prev_visible = true

        $scope.cats_shark.update_counter()

      # перематывает слайдер к элементу
      # 1. type - название рубрики
      goto: (type) ->
        # ищет индекс страницы
        part_index = 0
        for item, index in $scope.pages
          if item.type == type
            part_index = index
            break

        #проверяет условие показа кнопок назад/вперед
        if part_index == 0
          $scope.cats_shark.left = -$scope.cats_shark.item_padding * 2
          $scope.cats_shark.prev_visible = false
          $scope.cats_shark.next_visible = true
        else if part_index == $scope.cats_shark.length
          $scope.cats_shark.next_visible = false
          $scope.cats_shark.prev_visible = true
        else
          $scope.cats_shark.left = $scope.cats_shark.step * part_index
          $scope.cats_shark.next_visible = true
          $scope.cats_shark.prev_visible = true
        $scope.cats_shark.update_counter()


      # обновляет счетчик газеты
      update_counter: ->
        current_item_id = 0
        # ищет текущий элемент
        for value, index in $scope.cats_shark.items
          if value.offsetLeft - $scope.cats_shark.left > 0
            current_item_id = index
            break

        # вызывает метод update директивы $scope.river_shark
        # 1. current_item - id текущей страницы
        $scope.river_shark.update(current_item_id)

        # обновляет счетчик
        $scope.cats_shark.counter = "#{current_item_id + 1}-#{current_item_id + 2}/#{$scope.cats_shark.length}"



  link: (scope, element) ->
    # расчитывает некоторые параметры слайдера
    scope.cats_shark.counter = "1-2/#{scope.cats_shark.length}"
    scope.cats_shark.left = - scope.cats_shark.item_padding * 2
    scope.cats_shark.step = scope.cats_shark.item_width + scope.cats_shark.item_padding
    scope.cats_shark.slider_inner_width = scope.cats_shark.length * 324

    # получает элементы после загрузки галереи
    $timeout ->
      scope.cats_shark.items = $(element).find('.b-slider__item')
    ,
      0

    element.bind 'click', ->
      scope.$apply ->
        scope.river_shark.list_visible = false