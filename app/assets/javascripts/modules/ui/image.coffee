angular.module 'ui'
.directive 'imageonload', ()->
  restrict: 'A'
  link: (scope, element, attrs) ->
    element.bind 'load', (element) ->
      scope.$apply attrs.imageonload
