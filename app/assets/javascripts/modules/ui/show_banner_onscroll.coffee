
angular.module 'ui'
# Директива новости на странице 'Все новости'
.directive 'showOnScroll', ($compile, Scroller, $timeout, $rootScope, seaSettings, $http, $location, $window)->
  scope : {}
  controller: ($scope, $element, $attrs) ->



  link: (scope, element, attr) ->

    $rootScope.$on 'blueShark.load', (event, obj) ->

      if $(element).offset().top < obj.height + $(document).scrollTop()
        scope.isShown = true
        sizes = JSON.parse attr.sizes
        googletag.cmd.push( () ->
          slot = googletag.defineSlot(attr.unitPath, sizes, attr.id).addService(googletag.pubads())
          googletag.pubads().enableSingleRequest()
          googletag.enableServices()
          googletag.display(attr.id);
          googletag.pubads().refresh([slot],{changeCorrelator: false})
        )
      else
        Scroller.add_breakpoint("#{attr.id}", $(element).offset().top - obj.height, $(element).offset().top + obj.height)
    scope.isShown = false
    $rootScope.$on "#{attr.id}.start", () ->

      sizes = JSON.parse attr.sizes
      if !scope.isShown
        scope.isShown = true
        console.log('show start ')
        googletag.cmd.push( () ->
          slot = googletag.defineSlot(attr.unitPath, sizes, attr.id).addService(googletag.pubads())
          googletag.pubads().enableSingleRequest()
          googletag.enableServices()
          googletag.display(attr.id);
          googletag.pubads().refresh([slot],{changeCorrelator: false})
        )


