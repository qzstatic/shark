#
# ##Директива подсветки пункта меню в header и header-fixed##
angular.module 'shared'
.directive 'referrer',(localStorageService, $rootScope) ->
  controller: ($scope) ->
    $scope.referrer = {
      newPath: "",
      oldPath: "",
      headerLinks: ['/search', '/newsline']
    }
  link: (scope)->
    $rootScope.$on 'blueShark.ready', ()->

      $('header .b-nav__item a').each ()->
        scope.referrer.headerLinks.push $(@).attr('href')

      scope.referrer.oldPath = localStorageService.get('referrer')
      scope.referrer.newPath = document.location.pathname

      if ($.inArray scope.referrer.newPath, scope.referrer.headerLinks) == -1
        $('header .b-nav__item, .header-fixed .b-nav__item').each ()->
          $(@).addClass 'active' if $(@).find('a').attr('href') == scope.referrer.oldPath

      if scope.referrer.newPath != scope.referrer.oldPath
        localStorageService.set('referrer', scope.referrer.newPath)
