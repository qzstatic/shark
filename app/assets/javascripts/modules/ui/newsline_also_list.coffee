angular.module 'ui'
.directive 'fastNewsList', ()->
  restrict: 'E'
  scope: {}
  link: (scope)->
    scope.fastNews = window.fastNews
    scope.newslineId = window.newslineId
    scope.excludeNewsline = (id) ->
      (item) ->
        `item.id != id`

  templateUrl: 'modules_templates/fast_news.html'
