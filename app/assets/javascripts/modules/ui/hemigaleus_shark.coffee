angular.module 'ui'
.directive 'hemigaleusShark', ($cookies, $compile) ->
  scope: {}
  controller: ($scope) ->
    $scope.set_region_cookie = ->
      $cookies.region_ignore = 1

  link: (scope, element) ->
    $compile(element.contents())(scope.$new())

