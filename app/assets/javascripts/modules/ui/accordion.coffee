ACTIVE_CLASS = "b-subscription-accordion_active"
angular.module 'ui'
.directive "accordion", ->
#  scope:
#    isChecked: "=ngChecked"

  link: (scope, element, attrs) ->
    title = $(element).find('.b-subscription-accordion__title a')
    description = $(element).find('.b-subscription-accordion__description')

    title.on "click", (e) ->
      e.preventDefault()
      element.toggleClass ACTIVE_CLASS

    scope.$on "$destroy", ->
      title.off()
      return

    return
