angular.module 'ui'
.service 'ProperOffsetTop', (Scroller) ->
  class ProperOffsetTop
    constructor: (@element, @eventNamesArray, @callBack)->
      @offsetTop =  @getOffsetTop()
      @run()

    getOffsetTop: ()->
      @setOffsetTop()
      return  @offsetTop

    setOffsetTop: ()->
      @offsetTop = $(@element).offset().top

    resetBreakpoints: ()->
      @setOffsetTop()
      @callBack(@getOffsetTop())
    run: ()->
      window.Scroller = Scroller
      i = 0
      interval = setInterval(()=>
        if i == 9
          @eventNamesArray.forEach((e)->
            Scroller.remove_breakpoint(e)
          )
          @resetBreakpoints()
          clearInterval(interval)

        else
          if @offsetTop < $(@element).offset().top
            i = 0
          @resetBreakpoints()
          i++
      , 200)