angular.module 'ui'
.service 'Keyboard', () ->

  class Keyboard

    # Инициализирует {Keyboard}
    constructor: ->

    listeners = []
    # Добавляет название элемента, реагирующего на событие клавиатуры
    #
    # @param [string] element_name название элемента
    # @param [number] element_action название события клавиатуры
    # @param [number] offset_bottom конец отслеживаемого события
    # @param [boolean] active флаг, который следит, чтобы  $scope.$emit выполнился только 1 раз

    add_listener: (element_name)->
      listeners.push element_name

    # Добавляет breakpoint'ы из массива event_array
    #
    # @param event_array массив событий

    add_listeners: (elements_array) ->
      for item in elements_array
        @add_listener item


    # Удаляет breakpoint
    #
    # @param [number] event название события

    remove_listener: (element_name) ->
      for item, index in listeners
        listeners.splice index, 1 if item == element_name

    # Отдает список всех breakpoint'ов
    #
    # @return [Object] массив breakpoint'ов
    listeners: -> listeners

  new Keyboard

# Директива, реализующая работу со скроллом и генерацию событий
.directive 'vedKeyboard', ->

  controller: ($scope, $window, Keyboard) ->
    angular.element($window).bind "keyup", (event)->
      for item in Keyboard.listeners()
        $scope.$emit("#{item}.#{event.keyCode}")





