angular.module 'ui'
.directive 'socialIcons', ($rootScope, $interval, ProperOffsetTop, $http, $compile, $location, $window) ->
  
  scope: {}
  
  controller: ($scope, $element, $attrs) ->
    if $attrs.slug == 'online' && $attrs.finished == 'false'
      datetime = $('#scribbleCheckTime').val()
      url = $location.absUrl().replace(/\/p\d+$/, '')
      $interval ->
        $http
          .get("#{url}/check_new_posts/#{datetime}").then (res) ->
            if res.data.count > 99
              $scope.broadcasts = '99+'
            else if res.data.count == 0
              $scope.broadcasts = ''
            else
              $scope.broadcasts = res.data.count
      , 3000

    $scope.updateBroadcasts = -> $window.location.reload()
      
  link: (scope, element, attr) ->
    $compile(element.contents())(scope)

    $(document).on 'div-gpt-ad-1449233334715-0', (e)->

      $rootScope.$on 'blueShark.load', (e, i) ->

        mainContainer =  if $('.b-document__body').length > 0 then  $('.b-document__body') else ''
        mainContainerHeight = mainContainer.height()
        socialBlock = $(element)
        socialBlockParent = $(element).parent()
        galleryBlockHeight = +($('.b-gallery').eq(0).height())
        socialBlockHeight = socialBlock.height()
        socialBlockTop = socialBlockParent.offset().top
        topMenuHeight = $('header nav').height()

        if i.width < 1279
          designPaddign = 0
          bottomBreakpoint = socialBlockTop + mainContainerHeight + galleryBlockHeight - 24 # 14 - искусстевво подобранная величина
          cssTop = mainContainerHeight + socialBlock.outerHeight() + galleryBlockHeight + 21 # 34 - это маржин - небольшой отступ элемента под mainContainer
        else
          designPaddign = 30
          bottomBreakpoint = socialBlockTop + mainContainerHeight + galleryBlockHeight - (topMenuHeight + designPaddign + socialBlockHeight)
          cssTop = mainContainerHeight -  socialBlockHeight + galleryBlockHeight - 11 #

        eventArr = ['social.icons.frame','social.icons.after_frame']

        properOffsetTop = new ProperOffsetTop('.b-button__social__wrapper', eventArr, (_offset)-> Scroller.add_breakpoints([
          {
            event_name: 'social.icons.frame',
            offset_top: _offset - topMenuHeight - designPaddign,
            offset_bottom: bottomBreakpoint
          },
          {
            event_name: 'social.icons.after_frame',
            offset_top: bottomBreakpoint,
            offset_bottom: $('html').height()
          }
        ]))

        $rootScope.$on "social.icons.frame.start", ->
          socialBlock.addClass('scrolled')
          socialBlock.removeAttr('style')
        $rootScope.$on "social.icons.frame.stop", ->
          socialBlock.removeClass('scrolled')

        $rootScope.$on "social.icons.after_frame.start", ->
          # console.log bottomBreakpoint,  'bottomBreakpoint', $(document).scrollTop() - 67 , '$(document).scrollTop() - 67'
          socialBlock.css({ top: cssTop })
