#Whale shark
#Китовая акула (лат. Rhincodon typus) — крупная пелагическая акула из семейства ринкодонтовых, или китовых акул (Rhincodontidae) отряда воббегонгообразных (Orectolobiformes).
#Крупнейшая из существующих в настоящее время видов акул, а также крупнейшая из современных рыб. Её размер достигает по меньшей мере 12—14 м, возможно, до 18 м и даже 20 м.

#Карта яндекс
angular.module('ui')
.directive 'whaleShark', () ->
  restrict: 'A',
  scope:
    points: '=',
    noAutoInit: '='

  link: (scope, element, attributes)->

    scope.init = ()->

      scope.myMap = new ymaps.Map 'map',
        center: scope.points[0],
        zoom: 15,
        controls: []
      ,

        searchControlProvider: 'yandex#search'

      for point in scope.points
        scope.myMap.geoObjects.add(new ymaps.Placemark(point, {},
        {
          preset: 'islands#blueCircleDotIconWithCaption',
          iconCaptionMaxWidth: '120'
        }))

      true

    scope.moveToPoint = (point) ->
      scope.myMap.panTo(scope.points[point],
        duration: 1500,
        flying: false,
        safe: true
      )
    attributes.$observe 'activePoint', (value) ->
      if scope.myMap?
        scope.moveToPoint(value)

    #if (!scope.noAutoInit)
    ymaps.ready(scope.init)
    #scope.init()
