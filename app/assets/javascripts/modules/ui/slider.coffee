angular.module 'ui'

# Директива слайдера
#
# требует следующей структуры документа
# .b-slider {ved-slider}
#   ul{ ng-style ='{left: - current_item * 300 + "px"}' }
#     .b-slider__item
#     .b-slider__item
#     .b-slider__item
#         ...
#
#   .b-slider__next{ng-click = 'next()'}
#   .b-slider__prev{ng-click = 'prev()'}
#
#   .b-slider__paginator
#      .b-slider__paginator__item{ng-click='goto(some_index)'}

.directive 'vedSlider', ($compile, $rootScope, seaSettings, $interval, $timeout) ->
  scope: {}
  controller: ($scope) ->
    $scope.width = 1200
    $scope.animation = false
    $scope.delay = 500
    $scope.delay2 = 600
    $scope.delay3 = 4000
    $scope.timer = 0

    # блокирует анимацию
    $scope.block_animation = ->
      $scope.animation = true
      $timeout ->
        $scope.animation = false
      ,
        $scope.delay


    # перематывает слайдер вперед
    $scope.next = ->
      if $scope.animation == false
        $scope.current_item += 1

        if $scope.current_item == $scope.length
          $timeout ->
            $scope.class = 'no-animation'
            $scope.current_item = 0
          ,
            $scope.delay2
          .then ->
            $timeout -> $scope.class = ''
        #блокируем переход вперед до завершения анимации
        $scope.block_animation()

    # перематывает слайдер назад
    $scope.prev = ->
      if $scope.animation == false
        $scope.current_item -= 1

        if $scope.current_item == -1
          $timeout ->
            $scope.class = 'no-animation'
            $scope.current_item = $scope.length - 1
          ,
            $scope.delay2
          .then ->
            $timeout -> $scope.class = ''
        #блокируем переход вперед до завершения анимации
        $scope.block_animation()

    # перематывает слайдер к элементу
    # item - id элемента
    $scope.goto = (item) ->
      if $scope.animation == false
        $scope.current_item = item
        $scope.block_animation()

    #функции работы с таймером
    $scope.timer_stop = ->
      $interval.cancel($scope.timer)

    $scope.timer_start = ->
      $interval.cancel($scope.timer)
      $scope.timer = $interval ->
        $scope.next()
      , $scope.delay3

  link: (scope, element, attr) ->
    #перекомпиляция скоупа
    $compile(element.contents())(scope.$new())

    scope.current_item = attr.currentItem * 1 || 0

    $rootScope.$on 'blueShark.load', (el,params) ->
      scope.$apply ->
        scope.width = if seaSettings.medium < params.width then 1200 else 910
        if attr.autoplay == 'on'
          if !$(element).is(':hover')
            $interval.cancel(scope.timer)
            scope.timer = $interval ->
              scope.next()
            ,
              scope.delay3


    $rootScope.$on 'blueShark.1024', ->
      scope.$apply ->
        scope.width = 900

    $rootScope.$on 'blueShark.1280', ->
      scope.$apply ->
        scope.width = 1200

    scope.length = $(element).find('.b-slider__item').not('.b-slider__item_cloned').length
