# Широкоперая (в буквальном переводе с английского - "широкоплавниковая") акула является очень редким
# представителем семейства серых (пилозубых) акул. Почти ничего неизвестно об образе жизни,
# пищевых пристрастиях и повадках этой рыбы.
#
# ![lamiopsis](http://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Carcharias_temminckii_by_muller_and_henle.png/350px-Carcharias_temminckii_by_muller_and_henle.png)

# ##Директива объекта html##
angular.module 'shared'
.directive 'vedAccordion',($rootScope, $compile, $location, seaSettings) ->
  scope: {}
  controller: ($scope, $attrs) ->
    $scope.toggle = ()->
      $scope.is_open = if $scope.is_open == true then false else true
      new_url = "#{document.location.pathname}##{seaSettings.prefix}#/boxes/" + $attrs.boxid
      document.location.hash = new_url
      # Отправка дополнительной статистики
      if typeof ga != 'undefined'
        ga 'set', 'page', new_url
        ga 'send', 'pageview'
      
      if typeof yaCounter3796804 != 'undefined'
        yaCounter3796804.hit new_url
        
      if _tmr?
        _tmr.push({id: "371008", type: "pageView", start: (new Date()).getTime()})

      (new Image()).src = '//counter.yadro.ru/hit?t44.1;r'+escape(document.referrer)+(if (typeof(screen)=='undefined') then '' else ';s'+screen.width+'*'+screen.height+'*'+(if screen.colorDepth then screen.colorDepth else screen.pixelDepth))+';u'+escape(new_url)+';'+Math.random()

  link: (scope, element)->
    $compile(element.contents())(scope.$new())
