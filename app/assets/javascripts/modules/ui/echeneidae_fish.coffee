#Прилипаловые, или прилипалы (лат. Echeneidae) — семейство костных рыб отряда окунеобразных.
# Длина тела от 30 до 90 см. Характерная особенность: передний спинной плавник смещён на голову и превращён в присоску.
# Плавательный пузырь отсутствует. Широко распространены в тропических и субтропических водах всех океанов.
#
# ##Директива расстановки элементов плавающего меню
#
angular.module 'ui'
.directive 'echeneidaeFish', ($compile, $rootScope)->
  scope: true
  controller: ($scope) ->

# математика
# h - высота баннера
# h2 - расстояние, которое баннер должен 'проплыть'
# h3 - расстояние между баннерами
  link: (scope, element) ->
    h = 600
    h2 = 700
    h3 = 40
    x0 = 265
    x1 = h2 + x0
    x2 = x1 + h + h3
    x3 = x2 + 2 * h
    x4 = x3 + h2
    x5 = x4 + h3 + h

    scope.echeneidae =
      height: $('.b-document__left').height()
      breakpoints: [x0, x1, x2, x3, x4, x5]

    $rootScope.$on 'blueShark.1280', ->
      scope.$apply ->
        scope.echeneidae.height = $('.b-document__left').height()
        $compile(element.contents())(scope.$new())

    $rootScope.$on 'blueShark.1024', ->
      scope.$apply ->
        scope.echeneidae.height = $('.b-document__left').height()
        $compile(element.contents())(scope.$new())

    #перекомпиляция скоупа
    $compile(element.contents())(scope.$new())

