angular.module 'ui'
.directive 'vdStatistic', ($window)->
  restrict: 'AE'
  scope:
    page: '@'
    block: '@'
  link: (scope, element, attrs) ->
    
    $mainLink = $(element).find('a.b-story__inner')
    
    isElementInViewport = (el) ->
      rect = el[0].getBoundingClientRect()
      rect.top >= 0 and rect.left >= 0 and rect.bottom <= (window.innerHeight or document.documentElement.clientHeight) and rect.right <= (window.innerWidth or document.documentElement.clientWidth)
    
    onScroll = ->
      if isElementInViewport($mainLink)
        angular.element($window).off "scroll", onScroll
        ga('send', 'event', 'story', 'view', $mainLink.attr('href'))
    
    if ga?
      $(element).find('a').on "click", ->
        $link = $(@)
        href = $link.attr('href')
        regexp = /^(https?\:)?\/\/(www\.)?vedomosti\.ru/i

        if !regexp.test href
          if !$link.hasClass('b-story__inner')
            ga('send', 'event', 'story', 'click', $link.closest('.b-story__inner').attr('href'), href)
          else
            ga('send', 'event', 'story', 'click', href, href)

      if scope.block == 'story'
        angular.element($window).on "scroll", onScroll            

      # href = URI(href)
      #   .addSearch('vdmsti_click_page', scope.page)
      #   .addSearch('vdmsti_click_block', scope.block)
      # $link.attr('href', href)
