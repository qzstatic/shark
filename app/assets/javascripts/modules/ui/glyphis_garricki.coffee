angular.module 'ui'
#директива, допиливающая материал второго уровня анонсирования при его трансформировании в первый уровень
# используется в Также в рубрике
# думаю, что будет глючить с не-статьями
.directive 'glyphisGarrickiShark', ($compile)->
  scope: {}
  controller: ($scope) ->

  link: (scope, element)->
    $compile(element.contents())(scope.$new())
    $items = $(element).find('li:visible')

    if $items.length == 5
      $items.eq(4).css({'display': 'none'})

    $first_element = $items.eq(0)
    $first_element_type = $first_element.attr('class').split(' ')
    $first_element.removeClass("#{$first_element_type[0]}_mutate-to-padding-top").addClass "#{$first_element_type[0]}_level2_mutate-to-level1 #{$first_element_type[0]}_mutate-to-border-bottom"

    $second_element_img = $items.eq(1).find('img')
    $second_element_img.each ()->
      $(@).remove() if $(@).attr('class') == 'show-on-medium-screen'
      $(@).removeClass('hide-on-medium-screen') if $(@).attr('class') == 'hide-on-medium-screen'
