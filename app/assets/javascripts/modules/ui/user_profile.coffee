angular.module 'ui'

# Директива меню профиля

.directive 'vedUserProfile', ($compile, $timeout, $rootScope) ->
  scope: {}
  controller: ($scope) ->
    timeout = 0
    delay = 1200

    $scope.visibility = false

    $scope.toggle_memu = ->
      $scope.visibility = if true == $scope.visibility then false else true

    $scope.show_menu = ->
      $scope.visibility = true

    $scope.hide_menu =  ->
      $scope.visibility = false

  link: (scope, element) ->
    $compile(element.contents())(scope.$new())
    $(element).find('.b-auth__menu').removeClass('hidden')

    $rootScope.$on 'user_profile.close', ->
      scope.hide_menu()

