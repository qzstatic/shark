# ![Катран](https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/Squalus_acanthias2.jpg/350px-Squalus_acanthias2.jpg)
#
# ##Директива галереи##
#
angular.module 'ui'
.directive 'vedGallery', ($compile, $location, $timeout, seaSettings, Scroller, $rootScope, Keyboard, $interval) ->
  scope: {}
  controller: ($scope, $element) ->
    $scope.current_url = null
    $scope.url = null
    $scope.load_timer = 0
    $scope.banner_counter = 0

    #меняет баннер
    $scope.change_banner = ->
      asd2=[];
      asd2.push(googletag.slot_manager_instance.find("#{seaSettings.media_banner}"))
      googletag.pubads().refresh(asd2,{changeCorrelator: true});

    #открывает fullscreen
    $scope.fullscreen_open = (mode) ->
      Keyboard.remove_listener("g#{$scope.gallery_id}")
      $scope.$emit 'fullscreen.open', { gallery_id: $scope.gallery_id, current_item: $scope.current_item, items: $scope.items, title: $scope.title, subtitle: $scope.subtitle, offset: $scope.offset, fullscreen_mode: mode || 'gallery' }

    $scope.fullscreen_close = (mode) ->
      Keyboard.remove_listener("g#{$scope.gallery_id}")
      Keyboard.remove_listener("g#{$scope.gallery_id}")
      Keyboard.add_listener("g#{$scope.gallery_id}")

    # Отправка дополнительной статистики
    $scope.send_statistic = ()->
      $scope.url = "#{document.location.pathname}##{seaSettings.prefix}#{$location.path()}"

      if ( !$scope.current_url || $scope.url != $scope.current_url ) && !$rootScope.fullscreen_on
        $scope.current_url = $scope.url
        if ga?
          ga('set', 'page', $scope.url)
          ga('send', 'pageview')


        if yaCounter3796804?
          yaCounter3796804.hit($scope.url)
          
        if _tmr?
          _tmr.push({id: "371008", type: "pageView", start: (new Date()).getTime()})

        (new Image()).src = '//counter.yadro.ru/hit?t44.1;r'+escape(document.referrer)+(if (typeof(screen)=='undefined') then '' else ';s'+screen.width+'*'+screen.height+'*'+(if screen.colorDepth then screen.colorDepth else screen.pixelDepth))+';u'+escape($scope.url)+';'+Math.random()
    #скрывает кнопки в галерее
    $scope.hide_controls = ()->
      $scope.timeout = $timeout ->
        $scope.controls_visibility = false
      , 300

    #показывает кнопки в галерее
    $scope.show_controls = ()->
      $scope.controls_visibility = true
      $timeout.cancel $scope.timeout

    # перелистывает вперед
    $scope.next = ->
      $scope.banner_counter++
      $scope.change_banner() if $scope.banner_counter % 2 == 0

      if $scope.current_item < $scope.length - 1
        #перелистываем элемент
        $scope.current_item += 1
        #работа с превьюшками
        $scope.thumbs.set_left()
        $scope.thumbs.next() if $scope.thumbs.last == $scope.current_item
      else if $scope.current_item == $scope.length - 1
        $scope.current_item = 0

      #предзагрузка фотографий
      for index in [$scope.current_item, $scope.current_item - 1, $scope.current_item - 2]
        $scope.load_image(index)
      #подмена урла
      $location.path("galleries/#{$scope.gallery_id}/normal/#{$scope.current_item + 1}")
      #отправляем статистику
      $scope.send_statistic()

    # перелистывает назад
    $scope.prev = ->
      $scope.banner_counter++
      $scope.change_banner() if $scope.banner_counter % 2 == 0

      if $scope.current_item > 0
        #перелистываем элемент
        $scope.current_item -= 1
        #работа с превьюшками
        $scope.thumbs.set_left()
        $scope.thumbs.prev() if $scope.thumbs.first - 1 == $scope.current_item

      else if $scope.current_item == 0
        $scope.current_item = $scope.length - 1
      #предзагрузка фотографий
      for index in [$scope.current_item, $scope.current_item + 1, $scope.current_item + 2]
        $scope.load_image(index)
      #подмена урла
      $location.path("galleries/#{$scope.gallery_id}/normal/#{$scope.current_item + 1}")
      #отправляем статистику
      $scope.send_statistic()

    # загружает изображение (эта функция просто игнорируется для обычной галереи)
    $scope.load_image = (index) ->
      #работа с jquery(вынести в директиву изображения)
      element = $($element).find('.b-gallery__item').eq(index)
      if element
        img = element.find('img')
        if img.data() && img.data().url
          img.before("<img src='#{img.data().url}' width='#{img.data().width}' height='#{img.data().height}' alt= \"#{img.attr('alt')}\">")
          img.remove()

    # перелистывает основню галерею к определенному слайду
    # 1. index - номер элемента
    $scope.goto = (index) ->
      if index != $scope.current_item
        $scope.current_item = index
        $location.path("galleries/#{$scope.gallery_id}/normal/#{$scope.current_item + 1}")
        $scope.thumbs.update_params()

        for index in [$scope.current_item - 1, $scope.current_item, $scope.current_item + 1]
          $scope.load_image(index)

        $scope.send_statistic()

    # галерея превьюшек
    $scope.thumbs =

      # перелистывает галерею превьюшек вперед
      next: ->
        if $scope.thumbs.left != -$scope.thumbs.inner_width && $scope.thumbs.inner_width/91 > $scope.thumbs.frame_items
          hidden_thumbs = Math.abs ($scope.thumbs.left + $scope.thumbs.inner_width - $scope.thumbs.thumb_width * $scope.thumbs.frame_items)
          $scope.thumbs.left -= if hidden_thumbs < $scope.thumbs.thumb_width * $scope.thumbs.frame_items then hidden_thumbs else $scope.thumbs.thumb_width * $scope.thumbs.frame_items
          $scope.thumbs.update_params()

      # перелистывает галерею превьюшек назад
      prev: ->
        if $scope.thumbs.left != 0
          hidden_thumbs = Math.abs $scope.thumbs.left
          $scope.thumbs.left += if hidden_thumbs <  $scope.thumbs.thumb_width * $scope.thumbs.frame_items then hidden_thumbs else $scope.thumbs.thumb_width * $scope.thumbs.frame_items
          $scope.thumbs.update_params()

      # пересчитывает параметры галереи превьюшек:
      # 1. определяет первый и последний элементы, после котрых надо перелистывать галерею
      # 2. запоминает положение галереи (left_old)
      update_params: ->
        $scope.thumbs.first = Math.abs($scope.thumbs.left) / 91
        $scope.thumbs.last = (Math.abs($scope.thumbs.left) + $scope.thumbs.frame_items * 91) / 91
        $scope.thumbs.left_old = if $scope.thumbs.first <= $scope.current_item <= $scope.thumbs.last then $scope.thumbs.left else $scope.thumbs.left_old

      # возвращает галерею превьюшек в состояние, при котором видна текущая фоточка
      #
      # Пример: пользователь перелистнул несколько раз галерею превьюшек, а потом - нажал кнопку вперед основной галереи
      set_left: ->
        $scope.thumbs.left = $scope.thumbs.left_old
        $scope.thumbs.update_params()

      # включает/отключает показ превьюшек
      toggle: -> $scope.thumbs.open = if $scope.thumbs.open is false then true else false

      # перематывает галерею превьюшек в к определенному значению
      goto: (index) ->
        #направление перемотки
        duration = index - $scope.current_item
        if duration >= 0
          while index >= $scope.thumbs.last then $scope.thumbs.next()
        else if duration < 0
          while $scope.thumbs.last - index >= 6 then $scope.thumbs.prev()
        $scope.thumbs.update_params()

  link: (scope, element, attr) ->
    $compile(element.contents())(scope.$new())

    #параметры path
    location_array =  $location.path().split('/')
    gallery_length = $(element).find('.b-gallery__item').length

    #настройки галереи

    params = {
      gallery_id: attr.id
      length: $(element).find('.b-gallery__item').length
      current_item: if location_array[2] == attr.id then location_array[4] - 1 else 0
      items: attr.items
      loaded: true
      title: attr.title
      subtitle: attr.subtitle
      offset: 0
    }


    #настройки превьюшек
    thumbs_params =
      frame_items: 5
      open: false
      left: 0
      first: 0
      last: if gallery_length > 5 then 6 else gallery_length
      left_old: 0
      thumb_width: 91
      inner_width: gallery_length * 91

    for key, value of params
      scope[key] = value

    for key, value of thumbs_params
      scope.thumbs[key] = value

    if params.length > 0
      scope.thumbs.goto(params.current_item)
    for index in [params.current_item - 1, params.current_item, params.current_item + 1]
      scope.load_image(index)

#    работа с клавиатурой
    $rootScope.$on "g#{attr.id}.keyboard.start", ->
      Keyboard.add_listener("g#{attr.id}")

    $rootScope.$on "g#{attr.id}.keyboard.stop", ->
      Keyboard.remove_listener("g#{attr.id}")

    $rootScope.$on "g#{attr.id}.39", ->
      scope.$apply -> scope.next()

    $rootScope.$on "g#{attr.id}.37", ->
      scope.$apply -> scope.prev()

    $rootScope.$on 'blueShark.ready', (el, params) ->
      $(element).removeClass 'loaded'
      scope.$apply ->
        scope.load_timer = $interval ->
          paddingSpace = window.innerHeight - $(element).offset().top
          if $(element)[0].className.search('b-gallery_title-image') != -1
            0
          else if paddingSpace > 0 && window.pageYOffset == 0
            Scroller.add_breakpoint("g#{attr.id}.keyboard", 0, $(element).offset().top + 195)
          else
            # 195px - половина высоты галлереи. Если полгаллереи уже за экраном, то не крутим
            Scroller.add_breakpoint("g#{attr.id}.keyboard", $(element).offset().top - window.innerHeight + 195, $(element).offset().top + 195)
          scope.offset = $(element).offset().top - 150
        , 500

        $timeout ->
          location = $location.path().split('/')
          if scope.gallery_id && location[2] == scope.gallery_id
            console.log 'jump to'
            scope.$emit 'blueShark.jump_to', scope.offset
        , 501

        scope.fullscreen_open() if 'fullscreen' == location_array[3] && scope.gallery_id == location_array[2]


    $rootScope.$on 'blueShark.load', (el, params) ->
      scope.$apply ->
        $interval.cancel(scope.load_timer)
        #console.log "final offset = #{scope.offset}"

    #работа с history api
    $rootScope.$watch(
      () ->  $location.path()
    ,
      (newLocation, oldLocation) ->
        if newLocation != oldLocation && newLocation != ''
          location = $location.path().split('/')
          gallery_params =
            id: location[2]
            type: location[3]
            item: location[4] * 1
            fullscreen_mode: location[5] || 'gallery'

          if scope.gallery_id && gallery_params.id == scope.gallery_id
            if 'normal' == gallery_params.type
              scope.$emit 'fullscreen.close2'
              scope.fullscreen_close()
              scope.goto(gallery_params.item - 1)
            else if 'fullscreen' == gallery_params.type && 'gallery' == gallery_params.fullscreen_mode
              scope.goto(gallery_params.item - 1)
              scope.fullscreen_open()
            else if 'fullscreen' == gallery_params.type && 'tiles' == gallery_params.fullscreen_mode
              scope.goto(gallery_params.item - 1)
              scope.fullscreen_open('tiles')
        else
          scope.$emit 'fullscreen.close2'
    )
