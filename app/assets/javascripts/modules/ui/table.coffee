angular.module 'ui'
.directive 'insetTable', ($rootScope, seaSettings, $compile) ->
  
  restrict: 'AE'
  
  scope:
    json: '='

  link: (scope, element) ->
    $compile(element.contents())(scope)

    contents = element.html()
    headers = {}
    $table = $(element).find('table')
    columns = scope.json.columns
    columns.splice -1, 1
    scope.isSortingEnabled = false
    
    # Дублируем сортировку заголовков если есть подзаголовки
    scope.json.rows.forEach (item, index) ->
      if item.title
        if index == 0 && scope.json.rows[1].title
          modifiedColumns = angular.copy(columns)
          modifiedColumns.forEach (item) -> item.sorting = false
          scope.json.columns = modifiedColumns.concat columns
          
    # Удаляем объединенные заголовки
    $table.find('th').each (index) ->
      colspan = +$(@).attr('colspan')
      rowspan = +$(@).attr('rowspan')
      if colspan > 1 
        scope.json.columns.splice index, colspan, null
      if rowspan > 1
        scope.json.columns.splice index, 1, null
        scope.json.columns.splice index + columns.length, 1, null
    
    columns = []
    scope.json.columns.forEach (item, index) -> if item? then columns.push item
      
    # Указываем заголовки которые нельзя сортировать, или у которых своя сортировка
    columns.forEach (item, index) ->
      if item.sorting == "нет" || !item.sorting
        headers[index] = { sorter: false }
      else if item.sorting == "своя"
        headers[index] = { sorter: 'customParser' }

    # Custom Parser
    $.tablesorter.addParser
      id: 'customParser',
      type: 'number',
      is: -> false
      format: (s) ->
        format = s
        scope.json.formatCells.forEach (row, rowindex) ->
          row.forEach (col, colindex) ->
            if !col then return
            if col.value == (s = $.trim(s)) 
              format = s.replace(s, col.format)
              scope.json.formatCells[rowindex][colindex] = undefined
        format
        
    # Рендер tablesorter 
    tableSorterRender = ->
      $table.tablesorter
        headers: headers
        selectorHeaders: 'th[colspan=1][rowspan=1]'
        textExtraction: (node) ->
          s = node.innerHTML
          s.replace /,/g, ""
      $table
        .bind 'sortStart', (event) -> 
          $(element).find('.expand-placeholder').remove()
          $(element).find('.expand').removeClass('opened')
          scope.isSortingEnabled = true
        .bind 'sortEnd', (event) ->
          el = $(event.target).find('.headerSortDown, .headerSortUp')
          colIndex = el.data('colindex')
          $table.find("td.sorted").removeClass('sorted')
          $table.find("td[data-colindex=#{colIndex}]").addClass('sorted')
  
    tableSorterRender()
  
    # Сброс сортировки
    scope.resetSorting = ->
      element.html contents
      $compile(element.contents())(scope)
      $table = $(element).find('table')
      tableSorterRender()
      scope.isSortingEnabled = false
      null
  
    # Расхлоп
    scope.toggleExpandedContent = ($event) ->
      target = $event.currentTarget
      $expandHtml = $(target).find('.expand-html')
      colSpan = $(target).find('td').length
      if $(target).next('.expand-placeholder').length
        $(target).next('.expand-placeholder').remove()
        $(target).removeClass('opened')
      else
        $(target).after("<tr class='expand-placeholder'><td colspan='#{colSpan}'>#{$expandHtml.html()}</td></tr>")
        $(target).addClass('opened')
      null
