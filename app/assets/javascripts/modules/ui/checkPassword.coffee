angular.module 'ui'
.directive "checkPassword", ->
  require: "ngModel"
  link: (scope, elem, attrs, ctrl) ->
    otherInput = elem.inheritedData("$formController")[attrs.checkPassword]
    ctrl.$parsers.push (value) ->
      if value is otherInput.$viewValue
        ctrl.$setValidity "repeat", true
        return value
      ctrl.$setValidity "repeat", false
      return

    otherInput.$parsers.push (value) ->
      ctrl.$setValidity "repeat", value is ctrl.$viewValue
      value

    return
