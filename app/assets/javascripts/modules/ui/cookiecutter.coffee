# ## Директива подсветки ссылок
# ![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Isistius_brasiliensis.jpg/220px-Isistius_brasiliensis.jpg)
angular.module 'ui'
.directive 'cookiecutterShark', ($rootScope, $timeout) ->
  link: (scope, element) ->
    $rootScope.$on 'blueShark.ready', ->

      $('.b-article__image:not(.b-lifestyle)').hover  ->
        $(@).parent().find('.b-article__title a').addClass('hover')
      , ->
        $(@).parent().find('.b-article__title a').removeClass('hover')

      #для новых иконок видео и галереи
      $('.b-article__title a').hover  ->
        $(@).parents('.b-article').find('.b-article__image').addClass('hover')
      , ->
        $(@).parents('.b-article').find('.b-article__image').removeClass('hover')

      $('.b-subrubric-item__image, .b-newspaper-item__image').hover ->
        $(@).parent().find('.b-subrubric-item__title a, .b-newspaper-item__title a').addClass('hover')
      , ->
        $(@).parent().find('.b-subrubric-item__title a, .b-newspaper-item__title a').removeClass('hover')

      #для inset_file
      $('.b-file__description a, .b-file__title a').hover ->
        $(@).parents('.b-file').find('.b-file__title a').addClass('hover')
      , ->
        $(@).parents('.b-file').find('.b-file__title a').removeClass('hover')
