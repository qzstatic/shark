angular.module 'ui'
# Glyphis garricki — один из видов рода пресноводные серые акулы, семейство Carcharhinidae. Этот вид акул
# обнаружен в рассеянных приливных реках и связанных с ними прибрежных водах северной Австралии и, возможно,
# Папуа — Новой Гвинеи. Он обитает в водоёмах с мутной водой и мягким дном. Молодые акулы переходят из
# пресной воды в солёную и обратно. Этот вид похож на прочих серых акул, у него плотное тело серого цвета с
# высокой спиной, крошечные глаза и широкие плавники. Особи достигают размера 2,5 м.
#
# ![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Glyphis_garricki_csiro-nfc.jpg/350px-Glyphis_garricki_csiro-nfc.jpg)

# ##Директива выпадающего меню газеты##
# скоуп не изолирован от magazine!
# использует массив страниц $scope.pages и методы директивы cats-shark
.directive 'riverShark', () ->

  controller: ($scope) ->
    $scope.river_shark =
      items: []
      current_type: $scope.pages[0].type
      list_visible: false

      # обновление названия рубрики
      update: (index) ->
        $scope.river_shark.current_type =  $scope.pages[index].type

      toggle: ->
        $scope.river_shark.list_visible = if $scope.river_shark.list_visible is true then false else true


    #формирует список разделов газеты (выпадающее меню)
    for item in $scope.pages
      if item.type != current_type
        $scope.river_shark.items.push item.type
        current_type = item.type
      else
        continue

  link: (scope, element) ->

