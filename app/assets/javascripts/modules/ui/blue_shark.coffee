# Синяя акула, или голубая акула, или большая голубая акула, или мокой — вид хрящевых рыб из семейства серых
# акул отряда кархаринообразных, выделяемый в монотипный род Prionace.
# ![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/Prionace_glauca.jpg/350px-Prionace_glauca.jpg)
#

# ##Директива окна браузера##
# генерирует несколько событий окна браузера, которые отслеживаются другими директивами (например,
#<a href='carharodon.html'>carharodon</a> ловит события blueShark.resize и т.д.)
angular.module 'ui'
.directive 'blueShark', ($window, $rootScope, seaSettings, $timeout) ->
  controller: ($scope) ->
    $scope.blue_shark = {
      current_top: 0
      current_width: 0
    }

  link: (scope) ->

    # Ширина без учета скроллбара
    getCrossBrowserWidth = () ->
      if (navigator.userAgent.indexOf('Safari') != -1 || navigator.userAgent.indexOf('MSIE 8') != -1) && navigator.userAgent.indexOf('Chrome') == -1
        return document.documentElement.clientWidth
      else
        return angular.element($window)[0].innerWidth

    # Высота элемента
    getCrossBrowserHeight = () ->
      if (navigator.userAgent.indexOf('Safari') != -1 || navigator.userAgent.indexOf('MSIE 8') != -1) && navigator.userAgent.indexOf('Chrome') == -1
        return document.documentElement.clientHeight
      else
        return angular.element($window)[0].innerHeight

    scope.blue_shark =
      current_width: getCrossBrowserWidth()

    # прокручивает документ наверх при включении fullscreen режима
    $rootScope.$on 'blueShark.jump_top', ->
      scope.blue_shark.current_top = $window.pageYOffset
      angular.element($window)[0].scroll(0,0)

    # возвращает документ в текущую позицию экрана (после закрытия fullscreen режима)
    $rootScope.$on 'blueShark.jump_current_position', ->
      angular.element($window)[0].scroll(0, scope.blue_shark.current_top)

    # прыгает к текущей позиции
    $rootScope.$on 'blueShark.jump_to', (event, params)->
      $timeout ->
        angular.element($window)[0].scroll(0, params)
      ,
      0

    # генерирует событие загрузки страницы
    angular.element($window).bind 'load', ->
      scope.$emit 'blueShark.load', {width: getCrossBrowserWidth(), height: getCrossBrowserHeight()}
      if twttr?
        twttr.widgets.load()

    # генерирует событие загрузки DOM
    $(document).ready ->
      scope.$emit 'blueShark.ready'

    # генерирует события перерисовки страницы
    angular.element($window).bind 'resize', =>
      if 0 < getCrossBrowserWidth() <= seaSettings.medium and scope.blue_shark.current_width != 1024
        scope.$emit 'blueShark.1024'
        scope.blue_shark.current_width = 1024
      else if seaSettings.medium < getCrossBrowserWidth() and scope.blue_shark.current_width != 1280
        scope.$emit 'blueShark.1280'
        scope.blue_shark.current_width = 1280

      # общее событие
      scope.$emit 'blueShark.resize', {width: getCrossBrowserWidth(), height: getCrossBrowserHeight()}
