# Широкоперая (в буквальном переводе с английского - "широкоплавниковая") акула является очень редким
# представителем семейства серых (пилозубых) акул. Почти ничего неизвестно об образе жизни,
# пищевых пристрастиях и повадках этой рыбы.
#
# ![lamiopsis](http://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Carcharias_temminckii_by_muller_and_henle.png/350px-Carcharias_temminckii_by_muller_and_henle.png)

# ##Директива объекта html##
angular.module 'shared'
.directive 'lamiopsisShark',($rootScope) ->
  controller: ($scope) ->
    $scope.lamiopsis =
      height: '100%'

  link: (scope, element)->
    # перерисовывает высоту html для fullscreen режима
    # 1. params - высота элемента
    $rootScope.$on 'lamiopsis.resize', (el, params) ->
      scope.lamiopsis.height = params

    # сбрасывает высоту html
    $rootScope.$on 'lamiopsis.reset', ->
      scope.lamiopsis.height = '100%'
      $(element).removeClass 'overflow'

    $rootScope.$on 'lamiopsis.hide', ->
      $(element).addClass 'overflow'