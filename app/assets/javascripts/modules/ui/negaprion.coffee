# Лимонная акула, или жёлтая акула, или короткорылая острозубая акула, или панамская острозубая акула
# (лат. Negaprion brevirostris) — вид семейства серых акул.
#
# ![Negaprion brevirostris](https://upload.wikimedia.org/wikipedia/commons/thumb/2/21/Lemonshark_%282%29.jpg/350px-Lemonshark_%282%29.jpg)

# ## Директива галереи для carcharodon.mode = 'gallery' ##
# Работает в составе carcharodon <br>
# Скоуп не изолирован от carcharodon! <br>
# Скорее всего, будет дополнена методами, загружающими фотографии на лету.

angular.module 'ui'
.directive 'negaprionShark', () ->

  controller: ($scope) ->
    $scope.carcharodon.update_counter = (item) ->
      $scope.carcharodon.counter = "#{item}/#{$scope.carcharodon.length}"

    # перелистывает carcharodon вперед
    $scope.carcharodon.next = ->
      if $scope.carcharodon.current_item < $scope.carcharodon.length
        $scope.carcharodon.current_item += 1
        $scope.carcharodon.update_counter $scope.carcharodon.current_item

    # перелистывает carcharodon назад
    $scope.carcharodon.prev = ->
      if $scope.carcharodon.current_item > 1
        $scope.carcharodon.current_item -= 1
        $scope.carcharodon.update_counter $scope.carcharodon.current_item

    # переходит к определенному элементу
    # 1. item - номер элемента
    $scope.carcharodon.goto = (item) ->
      $scope.carcharodon.toggle_mode()
      $scope.carcharodon.current_item = item
      $scope.$emit 'rhincodon.update_params'

  link: (scope, element) ->
    scope.carcharodon.current_item = 1
    scope.carcharodon.length = $(element).find('.b-gallery__item').length
    scope.carcharodon.counter = scope.carcharodon.update_counter(scope.carcharodon.current_item)




