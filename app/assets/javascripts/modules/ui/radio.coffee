angular.module 'ui'
.directive "radio", ->
  restrict: 'E'
  scope:
    ngModel: '='
    label: '@'
    value: '@'
    isChecked: '@'
  template:
    '<label class="b-subscription-form__radio" ng-class="{\'b-subscription-form__radio_active\': checked()}">' +
      '<input class="b-subscription-form__radio-input" ng-model="ngModel" type="radio" value="{{value}}">' +
      '<span class="b-subscription-form__radio-text" ng-bind="label"></span>' +
    '</label>'
  controller: [
    "$scope"
    ($scope) ->
      $scope.ngModel = $scope.value  if $scope.isChecked

      $scope.checked = ->
        $scope.value is $scope.ngModel

  ]
  replace: true
