CHECKED_CLASS = "b-subscription-form__checkbox_active"
angular.module 'ui'
.directive "checkbox", ->
  scope:
    isChecked: "=ngChecked"

  link: (scope, element, attrs) ->
    element.parent().addClass CHECKED_CLASS if scope.isChecked

    element.on "change", ->

      scope.$apply ->
        scope.isChecked = !scope.isChecked
        element.parent().toggleClass CHECKED_CLASS
        return

      return

    element.on "click", ->

      this.blur();
      this.focus();
      return

    scope.$on "$destroy", ->
      element.off()
      return

    return
