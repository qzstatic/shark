angular.module 'ui'
# Cлайдер для материалов "условно" второго уровня анонсирования
.directive 'vedSlider2', ($compile, $rootScope, seaSettings, $timeout) ->
  # скоуп данной галереи true - те создается новая дочерняя область видимости!
  # подробнее - [тут](http://habrahabr.ru/post/182670/) <br>
  # Это делается для того, чтобы работала директива ng-repeat внутри Galeocerdo
  scope: true
  controller: ($scope) ->
    #настройки
    $scope.width = 300
    $scope.dot_array = []
    $scope.current_step = 0
    #сдвиг нужен для перерасчета элементов при изменении размера окна браузера
    $scope.derivation = 0
    $scope.items_in_step = 0
    $scope.animation = false
    $scope.delay = 500

    # блокирует анимацию
    $scope.block_animation = ->
      $scope.animation = true
      $timeout ->
        $scope.animation = false
      ,
        $scope.delay

    # перематывает слайдер вперед
    $scope.next = ->
      if $scope.animation == false
        #вычисляем последний шаг
        if $scope.current_step < $scope.steps - 1
  #        console.log $scope.items - $scope.steps * $scope.step
          if $scope.current_step + 1 == $scope.steps - 1 then $scope.derivation = Math.abs ($scope.items - $scope.steps * $scope.items_in_step) else $scope.derivation = 0
          $scope.current_step += 1
        else if $scope.current_step == $scope.steps - 1
          $scope.derivation = 0
          $scope.current_step = 0
        $scope.block_animation()
    # перематывает слайдер назад
    $scope.prev = ->
      if $scope.animation == false
        if $scope.current_step > 0
          if $scope.current_step - 1 < 0 then $scope.derivation = Math.abs ($scope.items - $scope.steps * $scope.items_in_step) else $scope.derivation = 0
          $scope.current_step -= 1
        else if $scope.current_step == 0
          $scope.derivation = Math.abs ($scope.items - $scope.steps * $scope.items_in_step)
          $scope.current_step = $scope.steps - 1
        $scope.block_animation()
    # перематывает слайдер к элементу
    # item - id элемента
    $scope.goto = (item) ->
      if $scope.animation == false
        if item == 0
            $scope.current_step = 0
            $scope.derivation = 0
          else if item == $scope.steps - 1
            $scope.current_step = item
            $scope.derivation = Math.abs ($scope.items - $scope.steps * $scope.items_in_step)
          else
            $scope.current_step = item

  link: (scope, element) ->
    # реагирует на события

    # загрузка окна браузера
    $rootScope.$on 'blueShark.load', (el,params) ->
      scope.$apply ->
        if params.width > seaSettings.medium
          scope.items_in_step = 4
        else
          scope.items_in_step = 3

        scope.items = $(element).find('.b-slider__item').length
        scope.steps = Math.ceil scope.items/scope.items_in_step
        scope.current_step = 0

        for i in [0..scope.steps - 1]
          scope.dot_array.push i


    # ресайз окна браузера
    $rootScope.$on 'blueShark.1024', ->
      scope.$apply ->
        scope.items_in_step = 3
        scope.goto(0)

    $rootScope.$on 'blueShark.1280', ->
      scope.$apply ->
        scope.items_in_step = 4
        scope.goto(0)


    $rootScope.$on 'blueShark.resize', ->
      scope.$apply ->
        scope.steps = Math.ceil scope.items/scope.items_in_step
        scope.dot_array =[]
        for i in [0..scope.steps - 1]
          scope.dot_array.push i