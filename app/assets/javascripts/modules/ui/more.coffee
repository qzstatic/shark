# ##Директива кнопки "загрузить еще"##
#работает только для списочных страниц
angular.module 'shared'
.directive 'vedMore', ($http, $location, seaSettings) ->
  scope: {}
  templateUrl: 'modules_templates/more.html'
  controller: ($scope, $element) ->
    $scope.change_banner = ->
      asd2=[]
      asd2.push(googletag.slot_manager_instance.find("#{seaSettings.list_banner}"))
      googletag.pubads().refresh(asd2,{changeCorrelator: true})
    #счетчик количества загруженных материалов
    $scope.counter = 2
    $scope.loading = false
    $scope.load = ->
      $scope.loading = true
      $http.get "#{$location.absUrl()}/?page=#{$scope.counter}"
      .then (response)->
        $scope.counter++
        $scope.change_banner()
        $scope.loading = false
        if (response.data.html?)
          $($element).before $(response.data.html).find('.b-subrubric__items li')
          $scope.finished = response.data.hide_more_button

        $scope.$emit('document.heightChanged')
      , ()->
        console.log 'что-то пошло не так'

