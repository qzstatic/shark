window.to_www = ->
  cookie.set('view_version', 'www', { domain: options.domain, path: '/' })
  document.location.reload(true)

window.to_mobile = ->
  cookie.set('view_version', 'm', { domain: options.domain, path: '/' })
  document.location.reload(true)
