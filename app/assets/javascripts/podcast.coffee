#переменные
#тег аудио
podcast = document.getElementById('podcast')

play_button = document.getElementById('play')
timeline = document.getElementById('timeline')
timeline_bg = document.getElementById('timeline_bg')
timer1 = document.getElementById('timer1')
timer2 = document.getElementById('timer2')
volume_bg = document.getElementById('volume_bg')
volume = document.getElementById('volume')
volume_area = document.getElementById('volume_area')
volume_button = document.getElementById('normal')
mute_button = document.getElementById('mute')
volume_wrapper = document.getElementById('volume_wrapper')
current_volume = 1

timeline.style.width = 0
timer1.innerHTML = '0:00'
timer2.innerHTML = '0:00'
volume.style.width = '33px' if volume
podcast.volume = current_volume

#воспроизводит аудио
podcast_play = ->
  if podcast.paused then podcast.play() else podcast.pause()
  play_button.className = if play_button.className == 'play' then '' else 'play'

#клик по полосе прокрутки
timeline_click = (event)->
  console.log timeline_bg.offsetLeft
  podcast.currentTime = podcast.duration  * (event.pageX - timeline_bg.offsetLeft) / timeline_bg.clientWidth

#форматирует время подкаста в 0:00 (мин:сек)
format_time = (seconds) ->
  minutes = Math.floor seconds / 60
  seconds = Math.floor seconds % 60
  seconds = if seconds >= 10 then seconds else "0#{seconds}"
  return minutes + ":" + seconds

#обновляет таймер(для полосы прокрутки и времени)
time_update = ->
  timer1.innerHTML = format_time(podcast.currentTime)
  timeline.style.width = "#{Math.round((podcast.currentTime * timeline_bg.clientWidth) / podcast.duration )}px"
  if timeline.style.width == "#{timeline_bg.clientWidth}px"
    play_button.className = ''

#вычисляет длину ролика
set_duration = ->
  timer2.innerHTML = format_time podcast.duration

#меняет громкость воспроизведения
volume_click = (event)->
  current_width = event.pageX - volume_bg.offsetLeft
  while current_width % 3 != 0
    current_width++
  current_volume = current_width / 33
  podcast.volume = switch
    when current_volume >= 1 then 1
    when current_volume < 0.1 then 0
    else current_volume
  volume.style.width = "#{current_width}px"

mute = ->
  current_volume = podcast.volume
  volume.style.width = "0px"
  podcast.volume = 0
  volume_wrapper.className = 'mute'

unmute = ->
  console.log current_volume
  volume.style.width = "#{33 * current_volume}px"
  podcast.volume = current_volume
  volume_wrapper.className = ''

#блок событий
play_button.addEventListener 'click', podcast_play, false
podcast.addEventListener 'timeupdate', time_update, false
podcast.addEventListener 'loadedmetadata', set_duration, false
timeline_bg.addEventListener 'click', timeline_click, false
volume_area.addEventListener 'click', volume_click, false if volume_area
volume_button.addEventListener 'click', mute, false if volume_button
mute_button.addEventListener 'click', unmute, false if mute_button
