$ ->
  $multimediaTop = $('#multimediaTop')

  $multimediaTop.dragend
    onSwipeEnd: ->
      first = @pages[0]
      last = @pages[@pages.length - 1]
      $('.prev, .next').removeClass 'deactivated'
      $('.nav li').removeClass 'active'
      if first == @activeElement
        $('.prev').addClass 'deactivated'
      if last == @activeElement
        $('.next').addClass 'deactivated'
      $('.nav li').eq(@page).addClass 'active'
