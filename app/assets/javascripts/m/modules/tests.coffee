angular.module 'ui'
.directive 'tests', ($rootScope, $compile, $window, $location, localStorageService, md5, seaSettings) ->
  templateUrl: 'm/modules_templates/tests.html'
  controller: ($scope) ->
    $scope.main_title = testObj.title
    $scope.controllerInit = () ->
      if localStorageService.isSupported



        if localStorageService.get(testObj.id) == null
          localStorageService.set(testObj.id, {})

        $scope.getValue = (key)->
          return if localStorageService.get(testObj.id) && typeof(localStorageService.get(testObj.id)[key])== 'undefined' then null else localStorageService.get(testObj.id)[key]

        $scope.setValue = (key, value)->
          currentState = localStorageService.get(testObj.id)
          currentState[key] = value
          localStorageService.set(testObj.id, currentState)

        lastStep = $scope.getValue('lastStep')
        #        testObj.
        if isNaN(testObj.step)
          testObj.step = 1

        if typeof(lastStep) == 'undefined' || lastStep == null
          lastStep = 1
          $scope.setValue('lastStep', lastStep )
        testObj.step = lastStep
        $scope.setValue('curStep', parseInt(testObj.step))

    $scope.controllerInit()


  link: (scope, element, attr) ->
    scope.init = () ->


      scope.steps =  scope.questions = scope.ResultIsShown = scope.totalWeight = scope.disabled = scope.last = undefined
      scope.curStep =   scope.choices =   scope.test_title =   scope.test_img_url = scope.test_description =   scope.weight =   scope.testStyle =   scope.wrongText =   scope.rightText = undefined
      scope.steps = testObj.questions.length
      scope.questions = testObj.questions
      scope.ResultIsShown = false
      scope.totalWeight = +(scope.getValue('totalWeight'))
      scope.disabled = true
      scope.last = false

      if parseInt(testObj.step) - 1 < testObj.questions.length

        scope.step = parseInt(testObj.step) - 1
        scope.curStep = testObj.step
        scope.choices = testObj.questions[scope.step].choices
        scope.test_title = testObj.questions[scope.step].title
        scope.test_img_url = if testObj.questions[scope.step].image.versions? then seaSettings.agami_url + testObj.questions[scope.step].image.versions.default.url else testObj.mainImg
        scope.test_description = testObj.questions[scope.step].text
        scope.weight = testObj.questions[scope.step].weight
        scope.testStyle = testObj.questions[scope.step].type
        scope.wrongText = testObj.questions[scope.step].wrongtext
        scope.rightText = testObj.questions[scope.step].righttext


      if  parseInt(testObj.step) - 1 >= testObj.questions.length
        answersArr = scope.getValue('answers')
        scope.ResultIsShown = true
        scope.curStep = scope.steps
        scope.totalWeight = 0
        answersArr.forEach((answ, i) ->
          scope.totalWeight += +testObj.questions[i]?.choices[answ]?.weight
        )
        testObj.results.forEach((el, i) ->
          if el.min_weight <= scope.totalWeight <= el.max_weight
            scope.test_title  = el.text
            scope.resultIndex = i
        )

        scope.test_img_url = if testObj.results[scope.resultIndex].image.versions? then seaSettings.agami_url + testObj.results[scope.resultIndex].image.versions.default.url else  testObj.mainImg


        scope.correctAnsversCounter = 0
        testObj.questions.forEach((q, i)->
          if q.choices[answersArr[i]].weight > 0

            scope.correctAnsversCounter++
        )
        scope.md5hash = md5.createHash(scope.correctAnsversCounter.toString())

        scope.fbLinkParams = 'https://www.facebook.com/sharer/sharer.php?u=' + encodeURI('http://vedomosti.ru' +
              testObj.testUrl + '/s' + scope.resultIndex + '?hash=' + scope.md5hash)

        scope.vkLinkParams = 'http://vk.com/share.php?url=' + encodeURI('http://vedomosti.ru' +
              testObj.testUrl + '/s' + scope.resultIndex + '?hash=' + scope.md5hash)

        title = attr.testsTitle
        if title?
          scope.twitterLinkParams = 'https://twitter.com/intent/tweet?url=' + encodeURI('http://vedomosti.ru' +
                testObj.testUrl + '/s' + scope.resultIndex + '?hash=' + scope.md5hash) + '&text='+encodeURIComponent(title)+'&via=Vedomosti'
        else
          scope.twitterLinkParams = 'https://twitter.com/intent/tweet?url=' + encodeURI('http://vedomosti.ru' +
                testObj.testUrl + '/s' + scope.resultIndex + '?hash=' + scope.md5hash)
        scope.sharingImage = "#{seaSettings.puffin_url}/vedomosti.ru#{testObj.testUrl}/s#{scope.resultIndex}_#{scope.correctAnsversCounter}.jpg"

      scope.share = (social, e) =>
        e.preventDefault()
        window.open(social, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes')
      if !scope.href?
        if parseInt(scope.curStep) == parseInt(scope.steps)
          scope.last = true
          scope.href =  testObj.testUrl + "/r1"
        else
          scope.href =  testObj.testUrl + "/q"+ (parseInt(scope.curStep))
      if window.history?.pushState
        $location.url(scope.href);
        $location.replace();
        window.history.pushState(null, '', $location.absUrl());

      if parseInt(scope.curStep) == parseInt(scope.steps)
        scope.last = true
        scope.href =  testObj.testUrl + "/r1"

      else
        scope.href =  testObj.testUrl + "/q"+ (parseInt(scope.curStep) + 1)

      scope.showReload = false
      if scope.last
        scope.showReload = true

      scope.voted = false

      scope.answerClass = ''
      scope.answerResultIsShow = false

      lastStep = scope.getValue('lastStep')
      answersIndexArr = scope.getValue('answers')
      voted = false
      if answersIndexArr != null && typeof(answersIndexArr[(scope.curStep - 1 )]) != 'undefined'
        voted = true


    #      if lastStep == testObj.questions.length && voted && window.location.href.indexOf('/r1') == -1
    #        $window.location.href = testObj.testUrl + "/r1"
    #
    #      else if lastStep < testObj.questions.length  && window.location.href.indexOf('/r1') > -1
    #        $window.location.href = testObj.testUrl + "/q1"
    #
    #      else if  lastStep != scope.getValue('curStep') && window.location.href.indexOf('/r1') == -1
    #        $window.location.href = testObj.testUrl + "/q" + lastStep



    scope.colorize = (step, index)->
      if typeof step != 'undefined' && !scope.ResultIsShown
        answersIndexArr = scope.getValue('answers')
        if answersIndexArr != null && typeof(answersIndexArr[step]) != 'undefined'
          if typeof index != undefined
            if answersIndexArr[step] == index
              scope.weight = testObj.questions[scope.step].choices[index].weight
              scope.answerResultIsShow = true
              scope.disabled = false
              scope.voted = true
              return 'active'

          if testObj.questions[step].choices[answersIndexArr[step]].weight > 0
            return 'right'
          else
            return 'wrong'

        return ''

    scope.vote = (index, event)->
      if !scope.voted

        $(event.currentTarget).addClass('active')
        scope.disabled = false
        scope.weight = testObj.questions[scope.step].choices[index].weight
        if scope.getValue('answers') == null
          scope.setValue('answers', [index] )
        else
          arr = scope.getValue('answers')
          arr[scope.step] = index
          scope.setValue('answers', arr)

        scope.voted = true

    scope.submite = () ->
      if scope.voted
#        if !scope.last
        scope.setValue('lastStep', parseInt(testObj.step) + 1 )
        scope.setValue('totalWeight', scope.totalWeight + parseInt(scope.weight) )
        testObj.step =  +testObj.step + 1
        if (window.history?.pushState?)
          if scope.href?
            if ga?
              ga('set', 'page', scope.href)
              ga('send', 'pageview')
            if yaCounter3796804?
              yaCounter3796804.hit(scope.href)
            if _tmr?
              _tmr.push({id: "371008", type: "pageView", start: (new Date()).getTime()})

            (new Image()).src = '//counter.yadro.ru/hit?t44.1;r'+escape(document.referrer)+(if (typeof(screen)=='undefined') then '' else ';s'+screen.width+'*'+screen.height+'*'+(if screen.colorDepth then screen.colorDepth else screen.pixelDepth))+';u'+escape(scope.href)+';'+Math.random()
          scope.controllerInit()
          scope.init()
          asd2=[]
          asd2.push(googletag.slot_manager_instance.find("#{seaSettings.media_banner}"))
          asd2.push(googletag.slot_manager_instance.find("#{seaSettings.announce_banner}"))
          googletag.pubads().refresh(asd2,{changeCorrelator: false});

#          history.pushState({}, "Вопрос "+testObj.step, scope.href);
        else
          $window.location.href = scope.href

    scope.reloadTest = ()->
      localStorageService.set(testObj.id, {})
      $window.location.href = testObj.testUrl + "/q1"

    scope.init()