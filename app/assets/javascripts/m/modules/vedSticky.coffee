angular.module 'ui'
.directive 'vedSticky', ['$window', '$rootScope', ($window, $rootScope) ->
  scope: true
  link: (scope, element, attr) ->
    getTopOffset = (elem) ->
      box = elem.getBoundingClientRect()
      body = document.body
      docElem = document.documentElement
      scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop
      clientTop = docElem.clientTop || body.clientTop || 0
      top  = box.top +  scrollTop - clientTop
    console.log(element[0].offsetHeight)
    if isNaN(attr.vedStickyUntil) && document.getElementById(attr.vedStickyUntil)?
      scope.bottomBorder = getTopOffset(document.getElementById attr.vedStickyUntil)
    else if !isNaN(attr.vedStickyUntil)
      scope.bottomBorder = 1 * attr.vedStickyUntil



    topClass = attr.vedSticky
    offsetTop = getTopOffset element[0]
    initialOffset = offsetTop
    scope.sticked = window.pageYOffset >= offsetTop
    if (scope.sticked)
      element.addClass(topClass)

    $rootScope.$on('header.show', () ->
      offsetTop = initialOffset - attr.vedStickyOffset

      if scope.sticked
        element.css({ top: '65px'})
      else
        element.css('top', '')

    )
    $rootScope.$on('header.hide', () ->
      offsetTop = initialOffset

      if scope.sticked
        element.css({ top: '0'})
      else
        element.css('top', '')

    )
    angular.element($window).on 'scroll', (e) ->
      if (window.pageYOffset >= offsetTop) && (window.pageYOffset < scope.bottomBorder  - element[0].offsetHeight)
        scope.sticked = true
        element.addClass(topClass)
        element.css({'margin-top': 0})
      else if (window.pageYOffset >= scope.bottomBorder - element[0].offsetHeight)
        scope.sticked = false
        element.css('top', '')
        element.removeClass(topClass)
        element.css({'margin-top': scope.bottomBorder - element[0].offsetHeight + 'px'})
      else
        scope.sticked = false
        element.removeAttr('style')
        element.removeClass(topClass)


]