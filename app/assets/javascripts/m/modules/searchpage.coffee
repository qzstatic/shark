$ ->
  uri = URI document.location.href
  query = URI.parseQuery uri.search()
  url = '//www.vedomosti.ru/api/v1/search/documents/published/?sort=date&categories=materials&q=' + query.q
  if uri.path()=="/search"
    $.ajax
      url: url
      method: 'GET'
    .success (data) ->
      documents = data.found
      $.get '/assets/m/templates/search.mst', (template) ->
        rendered = Mustache.render template, {documents: documents}
        $('.search-results').append rendered
    .error (data) -> console.log data
