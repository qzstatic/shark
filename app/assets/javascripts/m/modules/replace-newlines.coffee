angular.module 'ui'
.directive "replaceNewlines", ->
  restrict: 'EA'
  link: (scope, element, attrs) ->
    html = '<p>' + $.trim(element.html()) + '</p>'
    html = html.replace(/\n/g, '</p><p>')
    element.html(html)
