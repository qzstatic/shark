$ ->
#  UGLIEST HACK EVER
  window.header = {}
  window.header.isEventSet = false
  window.header.setEvent = () ->
    window.header.isEventSet = true
    lastScrollTop = 0
    $fixedHeader = $('.fixed-header')
    $(window).scroll ->
      navbarHeight = $fixedHeader.outerHeight(true)
      offsetTop = $('#root .b-banner:first-child').outerHeight(true) + $('.mobile-warning').outerHeight(true)
      scrollTop = $(window).scrollTop()

      if scrollTop >= offsetTop
        $fixedHeader.addClass 'is-active'
        $fixedHeader.next().css 'padding-top', 56

      if scrollTop < offsetTop
        $fixedHeader.removeClass 'is-active'
        $fixedHeader.next().css 'padding-top', '0px'

      if (scrollTop >= offsetTop + navbarHeight) && scrollTop > lastScrollTop
        $fixedHeader.css('top', -navbarHeight + 'px')

      if scrollTop < lastScrollTop
        $fixedHeader.css('top', 0)

      lastScrollTop = $(window).scrollTop()
  window.header.removeEvent = () ->
    window.header.isEventSet = false
    $(window).off('scroll')

  window.header.setEvent()