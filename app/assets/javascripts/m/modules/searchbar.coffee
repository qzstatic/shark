$ ->
  query = document.getElementById('searchQuery')
  clearQuery = document.getElementById('clearQuery')

  initClearQueryLink = (query, clearQuery) ->
    clearQuery.addEventListener 'mousedown', _clearQueryBox, true
    query.addEventListener 'keyup', _handleClearQueryLink, false

  _handleClearQueryLink = ->
    if query.value.length > 0
      clearQuery.style.display = 'inline'
      clearQuery.style.visibility = 'visible'
    else
      clearQuery.style.display = 'none'
      clearQuery.style.visibility = 'hidden'

  _clearQueryBox = (event) ->
    query.value = ''
    clearQuery.style.display = 'none'
    clearQuery.style.visibility = 'hidden'
    if event then event.preventDefault()
    $(query).focus()

  if query != null && clearQuery != null
    initClearQueryLink query, clearQuery
    _handleClearQueryLink()
