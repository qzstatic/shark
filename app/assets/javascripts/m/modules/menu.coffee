$ ->
  $menu = $('#menu')
  $banner = $('.b-banner.b-banner_topline')
  $fixedHeader = $('.fixed-header')
  $searchBasic = $('#searchQuery')
  toggleMenu = ()->

    $menu.toggleClass 'opened'

    $('#search:not(.show)').removeClass('opened')
    $('body').toggleClass('no-scrolling')

    if !$fixedHeader.hasClass('is-active')
      $fixedHeader.addClass('is-active');
      $fixedHeader.next().css 'padding-top', 56
    $fixedHeader.addClass('is-opened')

    document.activeElement.blur()
    $searchBasic.blur()

    $menu.one 'transitionend webkitTransitionEnd oTransitionEnd otransitionend', ->
      if $(@).height() == 0 then $fixedHeader.removeClass('is-opened')

  $('#menuToggler').on 'click', () ->
#    openMenu()
    topOffset = $banner.height()
    if (!$menu.hasClass('opened')) && ($('body').scrollTop()<$banner.height())
      $('body').animate
        scrollTop: if topOffset>1 then topOffset else 0
      , 300
      , () ->
          toggleMenu()
    else
      toggleMenu()


  $('#searchToggler').on 'click', (e) ->
    e.stopPropagation()
    e.preventDefault()
    $menu.removeClass 'opened'
    $('body').removeClass('no-scrolling')

    $('#search').toggleClass('opened').one 'transitionend webkitTransitionEnd oTransitionEnd otransitionend', ->
      if $(@).height() == 0
        document.activeElement.blur()
        $searchBasic.blur()
