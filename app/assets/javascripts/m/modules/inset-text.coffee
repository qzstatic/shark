$ ->

  $('.b-text-box').each ->
    if $(@).height() > 250
      $(@).addClass 'collapsed'
      $(@).find('.b-text-box-show').show()

  $('.b-text-box').on 'click', '.b-text-box-show', ->
    $box = $(@).parent()
    $box.removeClass 'collapsed'
    $box.find('.b-text-box-show').hide()
    $box.find('.b-text-box-hide').show()
    $box.addClass 'opened'

  $('.b-text-box').on 'click', '.b-text-box-hide', ->
    $box = $(@).parent()
    $box.addClass 'collapsed'
    $box.find('.b-text-box-show').show()
    $box.find('.b-text-box-hide').hide()
    $box.removeClass 'opened'
