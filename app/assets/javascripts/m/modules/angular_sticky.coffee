angular.module 'ui'
.directive 'vedSticky', ($window)->
  scope: true,
  link: (scope, element, attr) ->

    getOuterHeight = (element) ->
      elm = element[0]
      if elm?
        if(document.all)
  #          IE
          elmHeight = parseInt(elm.currentStyle.height)
          elmMargin = parseInt(elm.currentStyle.marginTop, 10) +    parseInt(elm.currentStyle.marginBottom, 10)
        else
  #          Mozilla
          elmHeight = parseInt(document.defaultView.getComputedStyle(elm, '').getPropertyValue('height'))
          elmMargin = parseInt(document.defaultView.getComputedStyle(elm, '').getPropertyValue('margin-top')) + parseInt(document.defaultView.getComputedStyle(elm, '').getPropertyValue('margin-bottom'))
        (elmHeight+elmMargin)
      else
        0



    lastScrollTop = 0
    navbarHeight = getOuterHeight(element)
    offset = attr.stickyOffset || navbarHeight
    console.log(navbarHeight)
    angular.element($window).on 'scroll', () ->
      offsetTop = getOuterHeight($(document.querySelector('#root .b-banner:first-child'))) + getOuterHeight($(document.querySelector('.mobile-warning')))
      scrollTop = $(window).scrollTop()

      if scrollTop >= offsetTop
        element.addClass 'is-active'
        element.next().css 'padding-top', offset + 'px'

      if scrollTop < offsetTop
        element.removeClass 'is-active'
        element.next().css 'padding-top', '0px'

      if (scrollTop >= offsetTop + navbarHeight) && scrollTop > lastScrollTop
        element.css('top', -navbarHeight + 'px')

      if scrollTop < lastScrollTop
        element.css('top', 0)

      lastScrollTop = $(window).scrollTop()
