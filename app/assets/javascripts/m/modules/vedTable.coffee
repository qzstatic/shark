angular.module 'ui'
.directive 'vedTable', ['$rootScope', ($rootScope) ->
  scope: true,
  link: (scope, element) ->
    if (!$rootScope.showHelpMessage?)
      $rootScope.showHelpMessage = true
    scope.initialized = false
    stickyTableInitialized = []

    scope.hideHelpMessage = () ->
      $rootScope.showHelpMessage = false
    scope.closeTable = (id) ->
      $('.fixed-header').show()
      document.getElementById(id).style.display = 'none'
      document.getElementsByTagName('html')[0].style.overflow='auto'
      document.body.style.overflow = 'auto'
      0
    scope.openTable = (id) ->
      document.body.style.overflow = 'visible'
      document.body.style.position = 'relative'
      document.getElementsByTagName('html')[0].style.overflow = 'hidden'
      document.getElementsByTagName('html')[0].style.position = 'relative'
      document.getElementById(id).style.display = 'block'
      $('.fixed-header').hide();
      if (!scope.initialized)
        initializeStickyTable(id)
        scope.initialized=true;
      0


    scope.currentZoom = 1;

    scope.zoomFor = (value) ->
      $('.sticky-wrap').trigger('scale', [value])
      0
]