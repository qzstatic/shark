angular.module('ui')

.service 'GalleryService', (seaSettings) ->
  cache = {}
  (id) ->
    if cache[id]
      return cache[id]
    galleryArray = []
    arr = []
    storage[id].forEach (element, i, array) ->
      descr = undefined
      url = undefined
      if (element.author && element.credits)
        credits = "#{element.author} / #{element.credits}"
      else if element.author
        credits = element.author
      else if element.credits
        credits = element.credits
      else
        credits = ''
      url = element.image.mobile_high.url
      descr = element.description
      galleryArray.push
        id: i + 1
        counter: "#{i+1}/#{array.length}"
        label: 'slide #' + i + 1
        img: seaSettings.agami_url + url
        desc: descr
        credits: credits
    cache[id] = galleryArray
    galleryArray

.directive 'angularCarousel', ->
  {
    scope: 
      methodFromController: '='
      galleryId: '='
      hideGallery: '='
      carouselIndex: '='
    restrict: 'E'
    template: '''<div class="b-gallery js-mediator-article" ng-class="{'b-gallery--no-padding': banner}" >
                  <div class="header">ФОТО<span class="back close" ng-click="hideGallery(galleryId)"></span></div>
                  <div class="gallery-wrapper" cycle-gallery gallery-init="onGalleryInit" gallery-index='galleryIndex'>
                    <ul class="gallery-slider" cycle-gallery-holder>
                      <li gallery-repeater="slide in list" class="gallery-item">
                        <div class="bgimage">
                          <div class="bgimage__img">
                            <img ng-src="{{ slide.img }}" alt="{{ slide.desc }}" class="b-main-image" />
                            <div class="bgimage__credits">{{slide.credits}}</div>
                          </div>
                          <div class="image-credits">
                            <div class="credits">
                              <span class="counter">{{slide.counter}}</span>
                            </div>
                            <div class="alt" ng-bind-html="slide.desc | trust_to_html"></div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div class="b-banner b-banner--line no-print">
                    <div id='div-gpt-ad-1478015404388-0' style='width:100%;overflow: hidden;position: relative;'>
                    </div>
                  </div>
                </div>'''
    controller: ($scope, GalleryService, $attrs, $timeout)->
#      $scope.carouselIndex = $attrs.start || 0

      googletag.pubads().addEventListener 'slotRenderEnded', (event) ->
        if event.slot.getSlotId().getName() == '/136502702/Mob_version_Galleries_Banner'
          if event.isEmpty
            $scope.banner = true
          else
            $scope.banner = false
          if(!$scope.$$phase)
            $scope.$apply()
      $scope.sendStatistic = (url)->
        if ga?
          ga('set', 'page', url)
          ga('send', 'pageview')
        if yaCounter3796804?
          yaCounter3796804.hit(url)
        if _tmr?
          _tmr.push({id: "371008", type: "pageView", start: (new Date()).getTime()})
        (new Image()).src = '//counter.yadro.ru/hit?t44.1;r'+escape(document.referrer)+(if (typeof(screen)=='undefined') then '' else ';s'+screen.width+'*'+screen.height+'*'+(if screen.colorDepth then screen.colorDepth else screen.pixelDepth))+';u'+escape(url)+';'+Math.random()
      $timeout (() ->
        triggered = false
        $scope.$watch('galleryIndex', (newValue) ->
          $scope.banner = true
          if triggered
            $scope.$emit('slide.change', $scope.galleryId)
          triggered = true
          currentUrl = window.location.pathname + "#/galleries/#{$scope.galleryId}/normal/#{newValue + 1}"
          $scope.sendStatistic(currentUrl)


        )
      )
      $scope.list = GalleryService($scope.galleryId)
      $scope.length = $scope.list.length
      $scope.galleryIndex = 0
      $scope.onGalleryInit = (gallery) ->
        $scope.galleryIndex = $scope.carouselIndex || 0

  }

.controller 'boxesController', ($scope, $timeout, $window) ->
  $scope.galleries = []

  $scope.setScrolling = (position) ->

    html = angular.element document.getElementsByTagName('html')[0]
    body = angular.element document.body
    root = angular.element document.getElementById('root')
    if position
      angular.element(document.querySelector('.b-gallery.js-mediator-article')).off('touchmove')
      html.css
        'position': ''
        'overflow': ''
      body.css
        'height': ''
        'overflow': ''
        'position': ''
        '-webkit-overflow-scrolling': ''
      root.css
        'max-height': ''
        'overflow': ''
        'position': ''
        '-webkit-overflow-scrolling': ''
    else

      angular.element(document.querySelector('.b-gallery.js-mediator-article')).on('touchmove', (e) ->
        e.stopPropagation()
      )
      html.css
        'position': 'relative'
        'overflow': 'hidden'
      body.css
        'position': 'relative'
        'overflow': 'hidden'
        '-webkit-overflow-scrolling': 'none'
      root.css
        'max-height': '100%'
        'overflow': 'hidden'
        'position': 'relative'
        '-webkit-overflow-scrolling': 'none'
  imageVertical = (id) ->
    result = false
    nodes = angular.element(document.querySelector("angular-carousel[gallery-id='#{id}']")).find('li')
    element = nodes[nodes.length % 2 + 1]
    if (element?)
      rect = angular.element(element).find('img')[0]?.getBoundingClientRect()
      result = rect.width<=rect.height
    result
  $scope.change_fullscreen_banner = (event, id) ->
    if !imageVertical(id)
      document.querySelector("angular-carousel[gallery-id='#{id}'] .b-banner--line")?.style.display = ''
      document.querySelector("angular-carousel[gallery-id='#{id}'] .b-gallery")?.style.paddingBottom = ''
      if (!window.galleryOpened)
        window.galleryOpened = true
        googletag.cmd.push(() ->
          googletag.defineSlot('/136502702/Mob_version_Galleries_Banner', [1, 1], 'div-gpt-ad-1478015404388-0').addService(googletag.pubads())
          googletag.enableServices()
        )
        $timeout(() ->
          googletag.cmd.push(() ->
            googletag.display('div-gpt-ad-1478015404388-0')
          )
        , 50)
      else if window.innerHeight > window.innerWidth
        asd2=[]
        asd2.push(googletag.slot_manager_instance.find("/136502702/Mob_version_Galleries_Banner"))
        googletag.pubads().refresh(asd2,{changeCorrelator: true})
    else
      document.querySelector("angular-carousel[gallery-id='#{id}'] .b-banner--line")?.style.display = 'none'
      document.querySelector("angular-carousel[gallery-id='#{id}'] .b-gallery")?.style.paddingBottom = '0'
  $scope.$on('slide.change', $scope.change_fullscreen_banner)
  $scope.showGallery = (id)->
    window.header.removeEvent()
    $scope.scrollPos = $window.scrollY
    $scope.setScrolling(false)
    $scope.galleries[id] = true
    if window.innerHeight > window.innerWidth
      if (!window.galleryOpened)
        window.galleryOpened = true
        googletag.cmd.push(() ->
          googletag.defineSlot('/136502702/Mob_version_Galleries_Banner', [1, 1], 'div-gpt-ad-1478015404388-0').addService(googletag.pubads())
          googletag.enableServices()
        )
        $timeout(() ->
          googletag.cmd.push(() ->
            googletag.display('div-gpt-ad-1478015404388-0')
          )
        , 50)

      else
        $timeout(() ->
          $scope.change_fullscreen_banner()
        , 50
        )

    $('.b-footer, .fixed-header').hide()
    0
  $scope.hideGallery = (id)->
    if !window.header.isEventSet
      window.header.setEvent()
    $scope.setScrolling(true)
    $scope.galleries[id] = false
    $('.b-footer, .fixed-header').show()
    $timeout () ->
      $window.scrollTo(0, $scope.scrollPos || 0)
    , 0
    0

.filter 'trust_to_html', [
  '$sce'
  ($sce) ->
    (text) ->
      $sce.trustAsHtml text
]