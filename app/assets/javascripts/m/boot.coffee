angular.module('Sea', ['templates', 'settings', 'ui', 'LocalStorageModule', 'angular-md5', 'cycleGallery', 'ngTouch'])
.run ($rootScope, $location, seaSettings, $sce)->
  if window.user_options?
    $rootScope.user_options = window.user_options
  if window.paywall_options?
    $rootScope.paywall_options = window.paywall_options
  $rootScope.hosts =
    metynnis : seaSettings.metynnis_url
  if window.paywall && window.paywall.length > 0
    console.log window.paywall
    $rootScope.paywall = window.paywall
    $rootScope.mobile = window.mobile


    $rootScope.state =
      loading: false
    #автоплатеж
      buyAutopayment:
        enabled: true
        message: ''
        update: (product)->
          if product.texts.autopayment != ''
            @.enabled = true
            @.message = $sce.trustAsHtml(product.texts.autopayment)
          else
            @.enabled = false
            @.message = ''
    #переключалки пейвола
      buyPaywall:
        enabled: true
        product: 'product1'
        setProduct: (product)->
          @.product = product

      buyError:
        enabled: false

    #отправка подписки
      buySubmit:
        checkbox:
          enabled: false
        button:
          enabled: false
        cash_text:
          enabled: false
        checkEnabled: ->
          @.button.enabled = !!($rootScope.state.buyProduct.message.visibility == false && @.checkbox.enabled == true)
        clear: ->
          @.button.enabled = false
          @.checkbox.enabled = false

      buyPayment:
        current: {}

      buyProduct:
        message:
          visibility: false

    $rootScope.state.buyAutopayment.message = $sce.trustAsHtml(paywall[0].texts.autopayment)
    $rootScope.state.buyPayment.current = 'card'
    $rootScope.paymentKit = paywall[0]

if true == window.html5mode || window.testObj?
  angular.module('Sea').config ($locationProvider) ->
  #включает html5mode для newsline
    $locationProvider.html5Mode(true)