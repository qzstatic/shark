#= require angular
#= require angular-rails-templates
#= require angular-local-storage
#= require settings

#= require ./boot
#= require jquery
#= require_tree ./vendors

#= require ./../vendors/jquery.cookie.js
#= require ./../vendors/cookie.js
#= require ./../vendors/boom
#= require ./../vendors/uri.min.js
#= require ./../vendors/mustache.min.js

#= require ./../versions

#= require ./common
#= require ./../vendors/angular-md5
#= require_tree ./modules_templates
#= require_tree ./modules

#= require ./../modules/ui/whale_shark
#= require ./../modules/ui/accordion

#= require ./common


