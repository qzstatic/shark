// define a new console
var console=(function(oldCons){
  return {
    log: function(text){
      //oldCons.log(text);
    },
    info: function (text) {
      //oldCons.info(text);
    },
    warn: function (text) {
      //oldCons.warn(text);
    },
    error: function (text) {
      //oldCons.error(text);
    }
  };
}(window.console));

//Then redefine the old console
window.console = console;