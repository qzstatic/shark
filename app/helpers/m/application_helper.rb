module M::ApplicationHelper
  
  # Типы документов с картинками в левом верхнем углу
  DOCUMENTS_WITH_ICON = [:video, :gallery, :online]

  def find_gallery(document)
    document.boxes.find { |box| box.type == 'gallery' }
  end

  # Документ входит в список документов со специальным типом?
  def document_with_icon?(document)
    DOCUMENTS_WITH_ICON.include?(part_for_document(document))
  end

  # Возвращает дополнительный тип документа
  def part_for_document(document)
    document.categories.has_parts? ? document.categories.parts.slug.singularize.to_sym : false
  end

  # Хелпер рисует анонсирующую картинку с типизацией видео/аудио 
  # в левом верхнем углу
  #
  # TODO: переписать через capture
  def announce_image(document)
    if document.has_image? && document.image.has_version?(:mobile_low)
      icon = ''

      link_to relative_link(document.url), class: 'b-main-image' do
        part = part_for_document(document)
        
        icon = "<span class='b-part-icon b-part-icon_#{part}'/>" if document_with_icon?(document) || document.test?
        raw("<img src='#{image_url(document.image.mobile_low.url)}'>#{icon}")
      end    
    end
  end
end
