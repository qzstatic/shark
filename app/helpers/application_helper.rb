module ApplicationHelper
  include Amp::UrlHelper
  #формирует рубрику для таргетинга Rubric
  def rubric_targeting_params
    if !@story.present? && @document.present? && @document.has_rubric?
      @document.categories.rubrics.map &:slug
    elsif !@story.present? && !@document.present? && @rubric.present?
      [@rubric.slug]
    else
      []
    end
  end

  #формирует подрубрику для таргетинга
  def subrubric_targeting_params
    if @document.present? && @document.has_rubric?
      @document.categories.rubrics.select(&:has_subrubrics?).map { |category| category.subrubrics.slug }
    elsif !@story.present? && !@document.present? && @subrubric.present?
      [@subrubric.slug]
    else
      []
    end
  end

  #формирует название сюжета для таргетинга Rubric
  def story_targeting_params
    if @document.present? && @document.has_story?
      @document.categories.story.slug
    elsif @story.present?
      @story
    else
      ''
    end
  end

  #формирует тип страницы для таргетинга Pagetype
  def pagetype_targeting_params
    case params[:controller].split('/').last
    when 'main'
      if ['rubric', 'subrubric'].include? params[:action]
        'rubrics'
      elsif params[:action] == 'story'
        'story'
        else
        'main'
      end
    when 'newsline'
      'news'
    when 'document'
      if @document.gallery?
        'gallery'
      elsif @document.has_parts? && @document.part.slug == 'news'
        'news'
      else
        'article'
      end
    when 'stories'
      'story'
    when 'tests'
      'tests'
    else
      'other'
    end
  end

  #формирует url подкаста на основе даты
  def podcast_url(date)
    "http://#{Settings.hosts.shark}/podcasts/#{date.strftime('%Y/%m/%d')}"
  end

  # Возвращает список авторов для статистики
  def authors_for_tags
    export = if @document.present?
               authors = []

               authors << @document.author if @document.has_author?
               authors << @document.second_author if @document.has_second_author?

               if @document.links.present? && @document.links[:authors].present?
                 authors += @document.links[:authors].map do |link|
                   link.bound_document
                 end
               end

               authors.map do |author|
                 "author:#{author.id}"
               end
             else
               []
             end

    raw export.to_json
  end

  def render_gallery(id, items, type)
    if items.kind_of?(Array) && items.length == 1
      render 'common/gallery', id: id, type: 'image', items: items[0]
    else
      render 'common/gallery', id: id, type: type, items: items
    end
  end

  # добавляет Копирайт ведомостей в урл
  def render_title(title, prefix='ВЕДОМОСТИ - ')
    content_for :title, "#{prefix}#{title}"
  end

  def render_description(description, prefix='Ведомости. ')
    content_for :description, "#{prefix}#{description}"
  end

  def announced_rss
    render partial: 'common/announced_rss', formats: [:html]
  end

  # добавляет в seaCache данные для галерей (сейчас не используется)
  def only_galleries(boxes)
    result = {}
    boxes.each_with_index do |box, index|
      result[index] = box if 'gallery' == box.type
    end

    result
  end

  # страница подписки пункты меню
  def subscription_link(name, path, classes = { all: 'b-nav__item', active: 'active', link: 'b-nav__item-link' })
    all = [classes[:all]]
    all << classes[:active] if current_page?(path)
    content_tag :li, class: all.join(' ') do
      link_to name, path, { class: classes[:link] }
    end
  end

  # Рендерит шаблон или комментарий, если шаблон не найден
  def render_if_exist(options = {}, local_assigns = {}, &block)
    render(options, local_assigns, &block)
  rescue
    if fail_template = local_assigns[:fail_template]
      render(fail_template)
    else
      raw('<!-- template not found or template error -->')
    end
  end

  def kind_of?(document, type)
    type.to_s == document.categories.parts.slug.singularize
  end

  def render_banner(banner, width, height, fullscreen = false)
    if fullscreen == true && cookies[:adriver_stop] == 'true'
      content_tag 'div', :style => 'position: absolute; top: 0; left: 0; background: rgba(255,0,0, 0.3)' do
        "Fullscreen-баннер отключен. Баннерные позиции выводятся без форматирования"
      end
    elsif cookies[:adriver_stop] == 'true'
      content_tag 'div', :style => 'width: 100%; text-align: center' do
        image_tag "http://placehold.it/#{width}x#{height}"
      end
    else
      render "common/banners/#{banner}"
    end
  end

  def render_exclude_css(document)
    content_tag "style", ".b-#{name_for_item(document)}_#{document.id} { display: none!important; }", :type => 'text/css'
  end

  # создает обертку для svg-объекта, которая позволяет вешать на него события ng-click, ng-show и т.д
  #
  # требует наличия 2х картинок в assets/svg и assets/icon
  # названия картинок должны совпадать
  def svg_tag(name, width, height, el_class = '')
    if name.empty?
      ''
    else
      content_tag :div, nil, class: "svg__wrapper #{el_class}", style: "width: #{width}px; height:#{height}px;" do
        concat content_tag :div, nil, class: 'svg__inner'
        concat content_tag :object,
                           content_tag(:i, nil, class: "b-icon icon-#{name}"),
                           data:   asset_path("svg/#{name}.svg"),
                           width:  width,
                           height: height,
                           type:   "image/svg+xml"
      end
    end
  end

  def site_url(url)
    [request.protocol, 'www.', (url || '').gsub(/(https?:\/\/)?(www\.)?/, '')].join('')
  end

  def targeting_url(url)
    Digest::MD5.hexdigest(['//', 'www.', (url || '').gsub(/(https?:\/\/)?(www\.)?/, '')].join(''))
  end

  def image_url(url)
    'https://' + Settings.hosts.agami + url
  end

  def image_absolute_url(url)
    'https://' + Settings.hosts.agami + url
  end

  def asset_absolute_url(url)
    asset_url(url, host: "https://#{Settings.hosts.agami}")
  end

  def image_url_social(document)
    "#{Settings.hosts.puffin}/#{document.url}.jpg?#{document.try(:last_published_at).to_i}"
  end

  # подменяет урл картинки в зависимости от настроек
  def image(relative_url, alt = '', image_class = '')
    full_url = image_url(relative_url)
    image_tag full_url, alt: alt, class: image_class
  end

  def image_version_size(image_version)
    if image_version.has_width? && image_version.has_height?
      "#{image_version.width}x#{image_version.height}"
    end
  end

  # Картинка с размерами и всеми необходимыми атрибутами
  def image_with_dimension(image, version, alt: nil, image_class: nil)
    image_version = image.public_send(version.to_sym)
    image_tag image_url(image_version.url), alt: alt, class: image_class, size: image_version_size(image_version)
  end

  def cdn_url(relative_url)
    'https://' + Settings.hosts.agami + relative_url
  end

  def current_user
    User.new(session[:user])
  end

  def logged_in?
    session.key?(:user)
  end

  # Returns relative URL if base domain.
  # Returns absolute URL if subdomain.
  # Returns # if nil or empty URL.
  def relative_link(url)
    if url.nil? || url.empty?
      '#'
    else
      if url.start_with?('vedomosti.ru')
        url.sub(/(http:\/\/(www.)?)?vedomosti.ru/, '')
      else
        [request.protocol, url].join('')
      end
    end
  end

  # отдает урл страницы с комментариями
  def comment_link(url)
    "/comments#{relative_link(url)}" if url
  end

  def select_options(type)
    options_from_collection_for_select(Option.send(type), :id, :value)
  end

  def rubric_path_unless_current(name, slug)
    nav_link(name, rubric_path(rubric_slug: slug))
  end

  # ссылки в header
  def nav_link(name, path)
    classes = %w(b-nav__item)
    classes << 'active' if current_page?(path)
    content_tag :li, class: classes do
      link_to name, path, :target => '_self'
    end
  end

  # хеш рубрик
  def rubrics
    {
        business:   'Бизнес',
        economics:  'Экономика',
        finance:    'Финансы',
        opinion:    'Мнения',
        politics:   'Политика',
        technology: 'Технологии',
        realty:     'Недвижимость',
        auto:       'Авто',
        management: 'Менеджмент',
        lifestyle:  'Стиль жизни'
    }
  end

  # верхняя навигация по рубрикам
  def nav_links
    rubrics.map do |slug, name|
      rubric_path_unless_current(name, slug)
    end.join.html_safe
  end

  # верхняя навигация по рубрикам
  def nav_static_links
    rubrics.map do |slug, name|
      content_tag :li, class: 'b-nav__item' do
        link_to name, "http://www.#{Settings.hosts.shark}#{rubric_path(rubric_slug: slug)}"
      end
    end.join.html_safe
  end

  # возвращает шаблон подрубрики
  def template_for(list)
    if list.has_template?
      "common/rubric/#{list.template}"
    else
      "common/rubric/small_no_banner"
    end
  end

  # формирует название подрубрики
  def link_for(list)
    link_to list.header, list.relative_url
  end

  # ищет галереи среди боксов
  def gallery
    @document.boxes.find { |box| box.type == 'gallery' }
  end

  def freshest(documents)
    documents.sort_by { |doc| doc.published_at }.last
  end

  # рендерит боксы документа (необходим для новости на странице fast-news)
  def render_document_body(document)
    render partial: 'document/box', collection: boxes_with_links(document)
  end

  # возвращает название шаблона документа(страница документа!) на основе part.slug для страницы документа
  def name_for_document(document)
    return 'advert_article' if document.categories.has_library? && document.categories.library.slug=='advert_articles'
    if document.part
      case document.part.slug
      when 'quotes' then
        'quote'
      when 'columns' then
        'column'
      when 'blogs' then
        'blog'
      when 'characters' then
        'character'
      when 'news' then
        'news'
      when 'galleries' then
        'gallery'
      when 'video' then
        'video'
      when 'test' then
        'test'
      when 'maps' then
        'map'
      else
        'article'
      end
    else
      'article'
    end
  end

  # возвращает название шаблона документа на основе part.slug (для страниц: рубрик, подрубрик, списочных и тд)
  def name_for_item(document)
    case document.part.slug
    when 'quotes' then
      'quote'
    when 'columns' then
      'column'
    when 'blogs' then
      'blog'
    when 'characters' then
      'character'
    when 'test' then
      'test'
    else
      'article'
    end
  end

  # формирует путь для шаблона документа (для страниц: рубрик, подрубрик, списочных и тд)
  def template_for_item(document, level)
    "common/items/#{level}/#{name_for_item(document)}"
  end

  # формирует класс документа (для страниц: рубрик, подрубрик, списочных и тд)
  def class_for_item(document, classes)
    classes.map do |element_class|
      "b-#{name_for_item(document)}_#{element_class}"
    end.join(' ') if classes.present?
  end

  # формирует класс для lifestyle - документа
  def class_for_lifestyle(classes)
    classes.map do |element_class|
      "b-lifestyle_#{element_class}"
    end.join(' ') if classes.present?
  end

  def label_data(document, template, page)
    check_library = Proc.new {
      if document.categories.has_library?
        return {
            title: document.categories.library.title,
            url:   library_path(slug: document.categories.library.slug)
        }
      end
    }

    check_opinion = Proc.new {
      if document.editorial?
        return {
            title: document.subrubric.title,
            url:   subrubric_path(rubric_slug: document.rubric.slug, subrubric_slug: document.subrubric.slug)
        }
      end
    }

    check_detail = Proc.new {
      if document.has_section?
        return {
            title: document.section.title,
            url:   section_path(rubric_slug: document.rubric.slug, subrubric_slug: document.subrubric.slug, section_slug: document.section.slug)
        }
      end
    }

    check_theme = Proc.new {
      if document.has_theme? && document.has_theme_cycle?
        return {
            title: document.theme_cycle.title,
            url:   theme_path(slug: document.theme_cycle.slug)
        }
      end
    }

    if page == :index
      check_library.call
      check_detail.call
      check_theme.call

      if template == 'main'
        if document.has_rubric?
          check_opinion.call

          return { title: document.rubric.title, url: rubric_path(rubric_slug: document.rubric.slug) }
        end
      else
        if document.has_rubric? && document.has_subrubric?
          return {
              title: document.subrubric.title,
              url:   subrubric_path(rubric_slug: document.rubric.slug, subrubric_slug: document.subrubric.slug)
          }
        end
      end
    else
      if template == 'main'
        check_library.call
        check_detail.call
        check_theme.call

        if document.has_rubric? && document.has_subrubric?
          check_opinion.call

          return {
              title: document.subrubric.title,
              url:   subrubric_path(rubric_slug: document.rubric.slug, subrubric_slug: document.subrubric.slug)
          }
        end
      end
    end
    return nil
  end

  def label_link(document, template, page)
    l = label_data(document, template, page)
    if !l.nil?
      link_to l[:title], l[:url], :target => '_self'
    end
  end

  # отрисовывает шаблон документа (для страницы рубрики, подрубрики, списочных и тд)
  def render_item(document, level, class_array, tag, template, double_image = false, target = '_self', alternative_title = nil, alternative_subtitle = nil)
    render template_for_item(document, level),
           document:             document,
           element_class:        class_for_item(document, class_array),
           element_tag:          tag,
           rubric_template:      template,
           double_image:         double_image,
           target:               target,
           alternative_title:    alternative_title,
           alternative_subtitle: alternative_subtitle
  end

  #отрисовка шаблона списочной страницы
  def render_list_item(document, index)
    render "common/items/list/#{name_for_item(document)}", document: document, index: index
  end

  def render_lifestyle(document, class_array, tag, template)
    render 'common/items/lifestyle', document: document, element_class: class_for_lifestyle(class_array), element_tag: tag, rubric_template: template
  end

  def render_multimedia(document, class_array, tag, template)
    render 'common/items/multimedia', document: document, element_class: class_for_lifestyle(class_array), element_tag: tag, rubric_template: template
  end

  # отображение анонса сюжета на главной странице и в рубриках
  def render_list_story(list)
    render 'includes/story', story: Document.new(list.as_json.merge(categories: {})), documents: list.documents
  end

  # Render SSI include directive for nginx in production mode
  # and call sub-request in other environments.
  def render_block(path)
    unless Rails.env.development?
      raw %Q(<!--# include virtual="#{path}" -->)
    else
      ssi_replacements = {
          '$cookie_access_token' => cookies[:access_token],
          '$OWL'                 => Settings.owl.response,
          '$mobile_operator'     => Settings.owl.mobile_operator,
          '$OWL_SMART'           => Settings.owl.smart_response
      }
      path.gsub!(/\$[a-zA-Z_]+/, ssi_replacements)

      full_url                = "http://#{request.host}:#{request.port}#{path}"
      mock_env                = Rack::MockRequest.env_for(full_url).merge('REMOTE_ADDR' => request.ip)
      mock_env["HTTP_COOKIE"] = "access_token=#{cookies[:access_token]}"

      if (body = Rails.application.call(mock_env).last).respond_to?(:join)
        raw(body.join)
      end
    end
  end

  def render_paywall
    render_block('/includes/metynnis/paywall_subscriptions')
  end

  def render_paywall_mobile
    render_block("/includes/metynnis/paywall_subscriptions/$mobile_operator?returnUrl=#{CGI::escape(return_url)}")
  end

  def render_paywall_newspaper
    render_block("/includes/metynnis/paywall_newspaper")
  end

  def render_paywall_mobile_newspaper
    render_block("/includes/metynnis/paywall_newspaper/$mobile_operator?returnUrl=#{CGI::escape(return_url)}")
  end

  def stub_error
    raw '<!--# block name="error" --><!-- ERROR Processing include --><!--# endblock -->' unless Rails.env.development?
  end

  def categories_for_swift(category)
    childs = category.keys.reject { |k| %i(title slug position).include?(k) }

    if category_real?(category) && childs.size == 0
      return category[:slug]
    else
      h = childs.each_with_object({}) do |k, hsh|
        if category[k].kind_of? Array
          hsh[k] = category[k].map { |el| categories_for_swift(el) }
        else
          hsh[k] = categories_for_swift(category[k])
        end
      end

      if category_real?(category)
        { category[:slug].to_sym => h }
      else
        h
      end
    end
  end

  def category_real?(category = {})
    category.has_key?(:title) && category.has_key?(:slug)
  end

  def categories_to_tags(category)
    out = []
    category.each_pair do |k, v|
      out += get_ctags(k, v)
    end
    out
  end

  def get_ctags(key, value)
    out = []
    if value.kind_of? Array
      value.each do |cvalue|
        out += get_ctags(key, cvalue)
      end
    else
      childs = value.keys.reject { |k| %i(title slug position).include?(k) }
      out << "#{key}:#{value[:slug]}"
      if childs.size > 0
        childs.each do |ckey|
          out += get_ctags(ckey, value[ckey])
        end
      end
    end

    out
  end

  def mobile_url
    url = URI.parse(request.original_url)
    ['http://m.', url.host.sub(/^(www|m)\./, ''), url.path].join
  end

  # расстановка ссылок на компании
  def boxes_with_links(document, range = nil)
    boxes = range.nil? ? document.boxes : Array(document.boxes[range])
    return boxes unless document.links.has_key?(:companies)
    bound_documents = document.links[:companies].map(&:bound_document)
    boxes.map do |box|
      if box.has_type? && box.type == 'paragraph' && box.has_kind? && box.kind == 'plain' && box.has_body?
        changed = false
        newbody = box.body.gsub(/<a entry="(\d+)">/) do |match|
          link_document_id = $1.to_i
          if doc = bound_documents.find { |d| d.id == link_document_id }
            changed = true
            url     = doc.url.sub(/\A(https?:\/\/)?(www\.)?vedomosti\.ru/, '')
            %Q(<a href="#{url}" target="_self">)
          else
            match
          end
        end

        if changed
          Roompen::Box.new(box.as_json.merge(body: newbody))
        else
          box
        end
      else
        box
      end
    end
  end

  def sharing_facebook_link(document)
    q = URI.encode_www_form(u: site_url(document.url))
    URI::HTTPS.build(host: 'www.facebook.com', path: '/sharer/sharer.php', query: q)
  end

  def sharing_vk_link(document)
    q = URI.encode_www_form(url: site_url(document.url), title: document.title)
    URI::HTTP.build(host: 'vk.com', path: '/share.php', query: q)
  end

  def sharing_twitter_link(document)
    q = URI.encode_www_form(url: site_url(document.url), text: document.title, via: 'Vedomosti')
    URI::HTTPS.build(host: 'twitter.com', path: '/intent/tweet', query: q)
  end

  def paywall_options_json
    if !Rails.env.development?
      p = [
          '"owl_status": "<!--# echo var="owl_status" default="Forbidden" -->"',
          '"owl_reason": "<!--# echo var="owl_reason" default="not_subscriber" -->"',
          '"owl_state": "<!--# echo var="owl_state" default="guest" -->"'
      ]
      raw "{#{p.join(',')}}"
    else
      owl_status, owl_reason, owl_state = Settings.owl.response.split('/')
      raw({
              owl_status: owl_status,
              owl_reason: owl_reason,
              owl_state:  owl_state
          }.to_json)
    end
  end

  # функция для создания amp ссылки для изображения
  def image_srcset(image)
    default_image = image.has_version?('normal') ? image.normal : image.default
    url           = [
        image_absolute_url(default_image.url),
        ' 1x, '
    # default_image.width.to_s + 'W,'
    ].join(' ')
    image.has_version?('mobile_high') && url << image_absolute_url(image.mobile_high.url) + ' 2x'
    # + image.mobile_high.width.to_s + 'W'
    url
  end

  def get_post_network(url)
    $1 if url =~ /(vk|facebook|instagram|twitter)/
  end

  def get_tweet_id(url)
    results = url.match(/\/(\d+)/).captures
    if results.empty?
      ''
    else
      results[0]
    end
  end

  def has_map(doc)
    !doc.boxes.find { |s| s.type == 'map' }.nil?
  end

  def responsive_image_tag(image, versions = [{ type: 'default', size: '1x' }, { type: 'retina', size: '2x' }], options={})
    begin
      srcset = versions.map { |version| "#{image_absolute_url(image[version[:type]][:url])} #{version[:size]}" }.join(',')
      image_tag(image[versions[0][:type]][:url], options.merge(srcset: srcset))
    rescue
      "<img />"
    end
  end

  def allowed?
    case request.format.symbol
    when :html
      %w(subscriber ip).include?(@owl_reason)
    when :mobile
      %w(mobile_subscriber subscriber ip).include?(@owl_reason)
    else
      false
    end
  end

  def return_url
    "#{request.scheme}://#{request.host}/#{@document.try(:url).to_s.sub('vedomosti.ru/', '')}"
  end

  # функция для разделения массива на 2 части
  def split_array(array)
    [array[0..(array.length+1)/2 - 1], array[(array.length+1)/2 ..-1]]
  end

end
