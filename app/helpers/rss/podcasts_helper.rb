module Rss::PodcastsHelper
  def rss_podcast_link(item)
    "http://#{Settings.hosts.shark}/podcasts/#{item.published_at.strftime('%Y/%m/%d')}"
  end
end