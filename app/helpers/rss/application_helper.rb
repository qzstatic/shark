module Rss::ApplicationHelper
  include ::ApplicationHelper

  def rss_title
    '"Ведомости". Ежедневная деловая газета'
  end
  
  def rss_news_title
    '"Ведомости". Новости'
  end
  
  def rss_news_description
    "\"Ведомости\", Новости, #{Date.today.strftime('%d.%m.%Y')}"
  end
  
  def rss_issue_description(document)
    "\"Ведомости\", #{document.published_at.strftime('%d.%m.%Y')}, #{document.title}"
  end
  
  def rss_site_url
    "http://www.#{Settings.domain}/"
  end
  
  def rss_news_url
    "http://www.#{Settings.domain}/newsline"
  end
  
  def rss_feed_image
    'https://cdn.vedomosti.ru/assets/rss_logo.gif'
  end
  
  def rss_yandex_logo_square
    'https://cdn.vedomosti.ru/assets/vedomosti_180x180.png'
  end
  
  def rss_item_link(item)
    "http://www.#{item.url}"
  end
  
  def rss_item_pdalink(item)
    "http://m.#{item.url}"
  end
  
  def rss_newsstand_summary
    'Ежедневная деловая газета «Ведомости» — уникальный проект, для реализации которого впервые в истории объединили свои силы две ведущие бизнес-газеты мира — Financial Times и The Wall Street Journal. Совместно с крупнейшим российским издательским домом Sanoma Independent Media они выпускают «Ведомости» с 1999 г.'
  end
  
  def rss_google_tracker
    'utm_source=google&utm_medium=news&utm_campaign=top-news'
  end
  
  def rss_body_html(body)
    simple_format(sanitize(body.gsub("\n", "\n\n"), tags: %w(i b strong em a), attributes: %w(href)))
  end
  
  def rss_also_links(documents)
    list = content_tag :ul do
      list = documents[0...3].map do |document|
        content_tag :li do
          content_tag(:a, document.title, href: rss_item_link(document))
        end
      end.join.html_safe
    end
    
    "<p>Читайте также: #{list}</p>".html_safe
  end

  def rss_podcast_url
    "http://#{Settings.hosts.shark}/rss/podcast"
  end

  def yandex_broadcasting_url(item)
    "#{rss_site_url}rss/yandex/broadcasting#{item.id}.xml"
  end

  def video_player_url(box)
    if box.type == 'dulton_media'
      "http://vedomosti.media.eagleplatform.com/index/player?record_id=#{box.dulton_id}&player=new"
    elsif box.type == 'inset_media' && box.body =~ /src="(.*?)"/
      $1
    else
      nil
    end
  end

  def strip_tags_for_cdata(str)
    Nokogiri::HTML(str.to_s).text
  end

end
