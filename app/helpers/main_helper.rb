module MainHelper
  def banners
    [
      Settings.banners.dblclick.banner1,
      Settings.banners.dblclick.banner2,
      Settings.banners.dblclick.banner3,
      Settings.banners.dblclick.banner4
    ]
  end
end
