module Amp::UrlHelper
  # проверяет, имеется ли доступ у пользоваля к данному докоменту
  def access_amp_url?(document_url, secret_key)
     secret_amp_key(document_url) == secret_key
  end
  # относительный путь для amp документа
  def document_amp_url(document_url)
    document_url = document_url.gsub("#{Settings.domain}\/",'')
    Rails.application.routes.url_helpers.amp_document_path(secret_amp_key(document_url), document_url)
  end
  # полный пусть до amp документа
  def document_absolute_amp_url(document_url)
    document_url = document_url.gsub("#{Settings.domain}\/",'')
    Rails.application.routes.url_helpers.amp_document_url(secret_amp_key(document_url), document_url,:host => Settings.hosts.shark)
  end
  # функция создания secret_key для доступа к документу. Важно! url НЕ должен начинаться со "/"
  def secret_amp_key(url)
    Digest::MD5.hexdigest(url.to_s + Settings.amp.secret_salt)[0..9]
  end
end
