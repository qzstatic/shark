class PageShow
  OPINION_DETAILS_SLUG_PATTERN = /^rubrics-opinion-details-[a-z]+_of_the_week$/
  
  # mode: %i(desktop phone tablet)
  def initialize(page_type, slug = nil, mode: :desktop)
    page_slug = [page_type, slug].compact.join('-')
    @page = Roompen.page(page_slug)
    @mode = mode.to_sym
  end
  
  def news
    list = page.lists.find { |list| list.slug == "#{page.slug}-news" }
    documents_from(list)
  end
  
  def top
    list = page.lists.find { |list| list.slug == "#{page.slug}-top" }
    documents_from(list, limit: top_limit)
  end
  
  def lists
    page.lists.reject do |list|
      [news_slug, top_slug, *opinion_details.map(&:slug)].include?(list.slug)
    end.select do |list|
      if mobile?
        list.slug !~ /theme_cycles/
      else
        true
      end
    end.tap { |lists| lists.unshift(opinions_details_list) if opinion? }.map do |list|
      list_attributes = list.as_json
      limit = sliders_slugs.include?(list.slug) ? slider_limit : list_limit

      if slug = story_slug(list.slug)
        limit = story_limit

        url = [Settings.domain, 'story', URI.escape(slug)].join('/')
        story = Roompen.document(url)

        list_attributes.merge! story.as_json.slice(*%i(title subtitle url image slider))
      end

      list_attributes[:documents] = documents_from(list, limit: limit)
      
      Roompen::List.new(list_attributes)
    end
  end
  
  def as_json(*)
    {
      top:   subtitle_strip_tags(top),
      news:  news,
      lists: subtitle_strip_tags_lists(lists)
    }
  end
  
  def opinion?
    page.slug == 'rubrics-opinion'
  end
  
private
  attr_reader :page, :mode
  
  def phone?
    mode == :phone
  end

  def tablet?
    mode == :tablet
  end
  
  def mobile?
    %i(phone tablet).include?(mode)
  end
  
  def documents_from(list, limit: nil)
    if list.nil?
      []
    elsif limit.nil?
      list.documents
    else
      list.documents.first(limit)
    end
  end
  
  def news_slug
    "#{page.slug}-news"
  end
  
  def top_slug
    "#{page.slug}-top"
  end

  def sliders_slugs
    %w(rubrics-opinion-details-top rubrics-lifestyle-interview-top)
  end
  
  def top_limit
    8 if phone?
  end
  
  def list_limit
    if phone?
      3
    elsif tablet?
      4
    end
  end

  def story_limit
    4
  end

  def slider_limit
    list_limit if !tablet?
  end
  
  def opinion_details
    @opinion_details ||= page.lists.select do |list|
      list.slug =~ OPINION_DETAILS_SLUG_PATTERN
    end
  end

  def story_slug(slug)
    if m = /^story-(\w+)-/.match(slug)
      m[1]
    end
  end

  def opinion_details_documents
    opinion_details.map do |list|
      list.documents.first
    end.tap do |documents|
      documents.sort! { |a, b| b.published_at <=> a.published_at } if mobile?
    end
  end

  def opinions_details_list
    Roompen::List.new(
      header:       'Детали',
      relative_url: '/rubrics/opinion/details',
      template:     'details',
      slug:         'rubrics-opinion-details-top',
      documents:    opinion_details_documents
    )
  end
  
  def subtitle_strip_tags(documents)
    documents.as_json.tap do |documents_data|
      documents_data.each do |document_data|
        document_data[:subtitle] = Nokogiri::HTML(document_data[:subtitle]).text
      end
    end
  end
  
  def subtitle_strip_tags_lists(lists)
    lists.as_json.tap do |list_data|
      list_data.each do |list|
        list[:documents] = subtitle_strip_tags(list[:documents])
      end
    end
  end
end
