class SendPasswordResetEmail
  attr_reader :ip, :email
  
  def initialize(email, ip)
    @email = email
    @ip = ip
    @success = false
  end
  
  def call
    response = aquarium_client.post(ip, '/shark/send_password_reset_link', email: email)
    @success = response.success?
  end
  
  def success?
    @success
  end

private
  def aquarium_client
    @aquarium_client ||= AquariumClient.new
  end
end
