class SignUpWithOauthToken
  attr_reader :user
  
  def initialize(user_attributes, ip, access_token)
    @user = User::WithOauth.new(ip: ip, access_token: access_token, **user_attributes.symbolize_keys)
  end
  
  def call
    unless provider_data.nil?
      user.provider    = provider_data[:provider]
      user.external_id = provider_data[:external_id]
      user.save && remove_data_from_redis
    end
  end
  
  def errors
    user.errors
  end
  
private
  def provider_data
    @provider_data ||= begin
      json = Shark::Redis.connection.get("oauth:#{user.access_token}")
      Oj.load(json) unless json.nil?
    end
  end
  
  def remove_data_from_redis
    Shark::Redis.connection.del("oauth:#{user.access_token}")
  end
end
