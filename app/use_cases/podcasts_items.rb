class PodcastsItems
  attr_accessor :newsreleases

  def initialize(limit: 10, offset: 0, exclude_ids: [])
    @newsreleases = Roompen.categorized(%w(project vedomosti), %w(kinds newsrelease), limit: limit, offset: offset, full: true)
    if exclude_ids.present?
      @newsreleases = @newsreleases.reject { |n| exclude_ids.include?(n.id) }
    end
  end

  def call
    newsreleases.map do |newsrelease|
      if box = podcast_box(newsrelease)
        box.merge!({
          title:           newsrelease.title,
          newsrelease_id:  newsrelease.id,
          newsrelease_url: newsrelease.url,
          published_at:    newsrelease.published_at
        })
      end
    end.compact
  end

private
  def podcast_box(newsrelease)
    if podcast_box = newsrelease.has_boxes? && newsrelease.boxes.find { |box| box.type == 'newsrelease_podcast' }
      Hashie::Mash.new(podcast_box.podcast[:original])
    end
  end
end