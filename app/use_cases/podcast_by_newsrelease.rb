class PodcastByNewsrelease
  attr_reader :newsrelease

  def initialize(newsrelease)
    @newsrelease = newsrelease
  end

  def call
    if box = podcast_box
      box.merge(
        title:           newsrelease.title,
        newsrelease_id:  newsrelease.id,
        newsrelease_url: newsrelease.url,
        published_at:    newsrelease.published_at
      )
    end
  end

private

  def podcast_box
    @podcast_box ||= begin
      if box = newsrelease && newsrelease.has_boxes? && newsrelease.boxes.find { |b| b.type == 'newsrelease_podcast' }
        Hashie::Mash.new(box.podcast[:original]) if box.has_podcast?
      end
    end
  end
end
