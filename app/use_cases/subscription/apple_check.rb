module Subscription
  class AppleCheck < Base
    attr_reader :ip, :access_token, :uuid, :response

    def initialize(ip, access_token, uuid)
      @ip = ip
      @access_token = access_token
      @uuid = uuid
    end

    def call
      response = aquarium_client.post(
        ip,
        '/shark/orders/apple/check',
        access_token: access_token,
        uuid: uuid
      )
      @response = response.body
      @success = response.success?
    end
  end
end
