module Subscription
  class ProductsList < Base
    attr_reader :ip, :target, :response

    def initialize(ip, target)
      @ip = ip
      @target = target
    end

    def call
      response = aquarium_client.get(
        ip,
        "/shark/orders/mobile_subscriptions/#{target}",
      )
      @response = response.body
      @success = response.success?
    end
  end
end
