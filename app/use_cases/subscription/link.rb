module Subscription
  class Link < Base
    attr_reader :ip, :access_token, :response

    def initialize(ip, access_token)
      @ip = ip
      @access_token = access_token
    end

    def call
      response = aquarium_client.post(
        ip,
        '/shark/orders/link_subscription',
        access_token: access_token,
      )
      @response = response.body
      @success = response.success?
    end
  end
end
