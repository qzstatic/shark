module Subscription
  class Base
    def success?
      @success
    end
  
  private
    def aquarium_client
      @aquarium_client ||= AquariumClient.new
    end
  end
end
