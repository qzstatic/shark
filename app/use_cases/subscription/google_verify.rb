module Subscription
  class GoogleVerify < Base
    attr_reader :ip, :access_token, :subscription_id, :token, :response

    def initialize(ip, access_token, subscription_id, token)
      @ip = ip
      @access_token = access_token
      @subscription_id = subscription_id
      @token = token
    end

    def call
      response = aquarium_client.post(
        ip,
        '/shark/orders/google',
        access_token: access_token,
        subscription_id: subscription_id,
        token: token
      )
      @response = response.body
      @success = response.success?
    end
  end
end
