module Subscription
  class AppleVerify < Base
    attr_reader :ip, :access_token, :receipt, :response

    def initialize(ip, access_token, receipt)
      @ip = ip
      @access_token = access_token
      @receipt = receipt
    end

    def call
      response = aquarium_client.post(
        ip,
        '/shark/orders/apple',
        access_token: access_token,
        receipt: receipt
      )
      @response = response.body
      @success = response.success?
    end
  end
end
