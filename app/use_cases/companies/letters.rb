module Companies
  class Letters
    attr_reader :companies

    SORT_PATTERN = [
      'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ',
      ('A'..'Z').to_a.join,
      (0..9).to_a.join,
      '#'
    ].join.freeze

    def initialize
      @companies = Roompen.categorized(%w(kinds companies), limit: 250)
    end

    def call(columns_num = 3)
      english_index = SORT_PATTERN.index('A')
      Hashie::Mash.new({
        russian: columns(companies.select { |c| letter_index(c.title[0]) < english_index }, columns_num),
        english: columns(companies.select { |c| letter_index(c.title[0]) >= english_index }, columns_num)
      })
    end

  private

    def columns(companies_list, columns_num)
      letters = {}
      companies_list.sort_by(&:title).each do |company|
        letter = company.title[0].upcase
        letter = '#' unless SORT_PATTERN.index(letter)
        letters[letter] ||= []
        letters[letter] << company
      end

      columns = []
      current_column = 0
      limit = companies_list.count / columns_num
      letters.keys.sort_by { |letter| letter_index(letter) }.each do |letter|
        columns[current_column] ||= []
        columns[current_column] << { letter: letter, companies: letters[letter] }
        companies_in_column = columns[current_column].inject(0) { |count, part| count + part[:companies].count }
        current_column += 1 if companies_in_column > limit && current_column < columns_num - 1
      end

      columns
    end

    def letter_index(letter)
      SORT_PATTERN.index(letter.upcase) || SORT_PATTERN.index('#')
    end
  end
end
