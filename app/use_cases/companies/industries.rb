module Companies
  class Industries
    attr_reader :companies

    def initialize
      @companies = Roompen.categorized(%w(kinds companies), limit: 250, full: true)
    end

    def call(columns_num = 3)
      industries = {}
      companies.sort_by(&:title).each do |company|
        industry = company.links.fetch(:industry,[]).first
        next unless industry
        industry_title = industry.bound_document.title
        industries[industry_title] ||= { industry: industry.bound_document, companies: [] }
        industries[industry_title][:companies] << company
      end

      columns = []
      current_column = 0
      limit = companies.count / columns_num
      industries.keys.sort.each do |industry_title|
        columns[current_column] ||= []
        columns[current_column] << Hashie::Mash.new(industries[industry_title])
        companies_in_column = columns[current_column].inject(0) { |count, part| count + part.companies.count }
        current_column += 1 if companies_in_column > limit && current_column < columns_num - 1
      end

      columns
    end
  end
end
