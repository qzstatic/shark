module Companies
  class Tabs
    TABS = [
      {
        slug:     :about,
        regexp:   /^О компании$/,
        title:    'О компании',
        tab_name: 'О компании'
      },
      { 
        slug:     :kso,
        regexp:   /^(?:Политика КСО|Политика корпоративной социальной ответственности)(.*)$/,
        title:    'Политика корпоративной социальной ответственности\1',
        tab_name: 'Политика КСО'
      },
      {
        slug:     :policy,
        regexp:   /^(Кадровая политика.*)$/,
        title:    '\1',
        tab_name: 'Кадровая политика'
      }
    ].map { |t| Hashie::Mash.new(t) }.freeze

    attr_reader :company

    def initialize(company)
      @company = company
    end

    def call
      current_tab = TABS.first
      tabs = { current_tab.slug => current_tab.merge(boxes: []) }

      company.boxes.each do |b|
        if b.type == 'paragraph' && b.kind == 'h1' && tab = find_tab(b.body)
          current_tab = tab
        else
          tabs[current_tab.slug] ||= current_tab.merge(boxes: [])
          tabs[current_tab.slug][:boxes] << b
        end
      end

      Hashie::Mash.new(tabs)
    end

    def tab_names
      TABS.map{ |t| t.slice(:slug, :tab_name) }
    end

  private
    def find_tab(text)
      TABS.each do |t|
        title = text.sub(t.regexp, t.title)
        return t.merge(title: title) if Regexp.last_match
      end
      return nil
    end
  end
end