class SignInWithOauthToken
  attr_reader :user, :provider_data
  
  def initialize(ip, access_token, provider, oauth_token, oauth_token_secret)
    @user = User::WithToken.new(ip: ip, access_token: access_token)
    
    assign_attributes(
      Shark::SocialNetworkClient.user_info_for(
        provider:     provider,
        token:        oauth_token,
        token_secret: oauth_token_secret
      )
    )
  end
  
  def call
    data = {
      provider:     user.provider,
      external_id:  user.external_id,
      access_token: user.access_token
    }
    
    if user.provider == 'facebook'
      old_id = Shark::FacebookIdConvertor.old_id_for(user.external_id)
      data[:old_external_id] = old_id unless old_id.nil?
    end
    
    response = aquarium_client.post(user.ip, User::WithOauth.sign_in_url, data)
    
    if response.success?
      assign_attributes(response.body)
    else
      save_provider_data_in_redis
    end
    
    response.success?
  end
  
private
  def assign_attributes(attributes)
    attributes.each do |attr_name, attr_value|
      user.public_send("#{attr_name}=", attr_value)
    end
  end
  
  def save_provider_data_in_redis
    key = "oauth:#{user.access_token}"
    value = Oj.dump(provider: user.provider, external_id: user.external_id)
    
    Shark::Redis.connection.set(key, value)
    Shark::Redis.connection.expire(key, 10.minutes)
  end
  
  def aquarium_client
    @aquarium_client ||= AquariumClient.new
  end
end
