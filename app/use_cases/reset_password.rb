class ResetPassword
  attr_reader :ip, :password, :password_reset_token, :access_token
  
  def initialize(password, password_reset_token, access_token, ip)
    @password = password
    @password_reset_token = password_reset_token
    @access_token = access_token
    @ip = ip
  end
  
  def call
    response = aquarium_client.post(
      ip,
      '/shark/reset_password',
      password: password,
      password_reset_token: password_reset_token,
      access_token: access_token
    )
    @success = response.success?
  end
  
  def success?
    @success
  end
  
private
  def aquarium_client
    @aquarium_client ||= AquariumClient.new
  end
end
