class MPageShow
  attr_reader :use_case, :view

  def initialize(*args)
    @view = args.last.is_a?(Hash) && args.pop[:view]
    @use_case = PageShow.new(*args)
  end

  def page
    use_case.page
  end

  def top
    if %w(phone tablet).include?(view)
      reject_online!(use_case.top)
    end
    
    view == 'tablet' ? use_case.top : use_case.top.first(8)
  end

  def news
    use_case.news
  end

  def lists
    lists = if view == 'tablet'
      use_case.lists
    else
      use_case.lists.map { |l| l.extend(ListShow) }
    end
    
    if page.slug == 'rubrics-opinion'
      lists.reject! { |l| /theme_cycles/ =~ l.slug }
    end
    
    if page.slug == 'main'
      lists.reject! { |l| /story-/ =~ l.slug }
    end
    
    lists
  end
  
  def reject_online!(documents)
    documents.delete_if { |document| document.categories.parts.slug == 'online' }
  end

  def as_json(*)
    {
      top: top,
      news: news,
      lists: lists
    }
  end

  module ListShow
    LIMIT = 3
    
    def documents
      super.first(LIMIT)
    end

    # TODO: Осторожно! Этот код надо переписать.
    def reject_online!(documents)
      documents.delete_if do |document|
        if document.is_a?(Hash)
          document[:categories][:parts][:slug] == 'online'
        else
          document.categories.parts.slug == 'online'
        end
      end
    end
    
    def as_json(*)
      super.tap do |attrs|
        attrs[:documents] = reject_online!(attrs[:documents]).first(LIMIT)
      end
    end
  end
end
