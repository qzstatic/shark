class FeedbackSend
  extend Forwardable
  
  def_delegators :form, :success?
  
  attr_reader :form
  
  def initialize(form)
    @form = form
  end
  
  def call
    form.save
  end
end
