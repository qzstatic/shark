class AcceptPromo
  attr_reader :promo_name, :access_token, :ip

  def initialize(promo_name, access_token, ip)
    @promo_name = promo_name
    @access_token = access_token
    @ip = ip
  end

  def call
    response = aquarium_client.patch(
      ip,
      "/shark/users/#{access_token}/accept_promo",
      promo_name: promo_name
    )
    @success = response.success?
  end

  def success?
    @success
  end

private
  def aquarium_client
    @aquarium_client ||= AquariumClient.new
  end
end

