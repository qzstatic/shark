class DocumentController < ApplicationController
  before_filter :find_document, only: %i(show comment body)
  skip_before_action :handle_session, only: :owl_show
  
  def show
    if @document.redirect?
      set_cookie(:original_referer, request.referrer) if cookies[:original_referer].nil?
      redirect_to "http://www.#{@document.location}", status: :moved_permanently
    else
      expires_in 1.year, public: true, must_revalidate: true
      response.headers['Last-Modified'] = @document.last_published_at.httpdate
      unset_cookie(:original_referer)
      
      @paywall_visit_url = @document.url if @document.pay_required?
      
      render_document
    end
  end
  
  def owl_show
    find_document_by_full_url(params[:url])
    find_document_by_full_url(@document.location) if @document.redirect?
    
    @owl_status = params[:owl_status].to_s.downcase
    @owl_reason = params[:owl_reason]
    @owl_state  = params[:owl_state]
    
    @sign_in_form = SignInForm.new
    @registration_form = PasswordRegistrationForm.new
    
    render layout: false
  end
  
  def preview
    @document = Roompen.document_preview(params[:document_id])
    redirect_to "/preview/tests/#{params[:document_id]}" and return if @document.test?
    @preview = true
    @owl_status = 'ok'
    @owl_reason = 'subscriber'
    @owl_state  = 'guest'
    render_document
  end
  
  def comment
    if @document.redirect?
      domain, path = @document.location.split('/', 2)
      canonical_url = ['http:/', Settings.hosts.shark, 'comments', path].join('/')
      
      redirect_to canonical_url, status: :moved_permanently
    else
      respond_to do |format|
        format.html { render 'main/comments' }
        format.json { render json: { document: @document } }
      end
    end
  end
  
  def body
    if @document.redirect?
      domain, path = @document.location.split('/', 2)
      body_url = ['http:/', Settings.hosts.shark, 'body', path].join('/')
      
      redirect_to body_url, status: :moved_permanently
    else
      render layout: false
    end
  end
  
  def by_id
    document = Roompen.document_by_id(params[:id])
    render json: document
  end

  # контроллер для показа старых новостей, которые начинаются с /newsline/news
  def oldnews
    params[:url] = "newsline/news/#{params[:oldurl]}"
    find_document
    show
  end
  
private
  def find_document
    url = [Settings.domain, URI.escape(params[:url])].join('/')
    @document = Roompen.document(url)
  end
  
  def find_document_by_full_url(url = nil)
    @document = Roompen.document(url)
  end
  
  def render_document
    categories_for_stats(@document.categories.as_json)

    @seo.description = @document.has_subtitle? ? "#{@document.title} #{@document.subtitle}" : @document.title
    @seo.keywords = make_keywords(@seo.description)

    respond_to do |format|
      format.html { render 'document/main'}
      format.json do
        render json: {
          document: @document
        }
      end
    end
  end

  def make_keywords(text)
    text.split(/[,.?!"';: ]+/).select { |t| t.length>3 }.join(',')
  end
end
