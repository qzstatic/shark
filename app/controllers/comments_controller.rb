class CommentsController < ApplicationController
  def show_popular
    response = client.get(request.ip, "/shark/popular_comments/#{Settings.comments.widget_id}/#{params[:xid]}")
    if response.success?
      @comment = Comment.new(response.body)
    end
    
    @document = Roompen.document_by_id(params[:xid])
    
    respond_to do |format|
      format.html { render partial: 'comments/popular' }
      format.json { render json: {comment: @comment} }
    end
  end
  
  def update_popular
    if check_hc_signature(params[:time], params[:data], params[:signature])
      events = Oj.load(params[:data]).map { |event| event.slice(:xid, :widget_id) }.uniq
      
      events.map do |event|
        client.patch(
          request.ip,
          "/shark/popular_comments/#{event[:widget_id]}/#{event[:xid]}",
          params.slice(:data, :time, :signature)
        )
      end
      
      head :ok
    else
      Rails.logger.warn "Popular comments update failed: #{params.slice(:data, :time, :signature)}"
      
      head :forbidden
    end
  end
  
private
  def check_hc_signature(time, data, signature)
    signature == Digest::MD5.hexdigest("#{Settings.hypercomments.secret_key}#{data}#{time}")
  end
end
