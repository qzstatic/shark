class NewspaperController < ApplicationController
  def by_date
    @date = newsrelease_date(params)
    @newsrelease = @date.nil? ? last_newsrelease : Roompen.document(newspaper_url)

    respond_to do |format|
      format.html
      format.json do
        render json: {
          newsrelease: @newsrelease
        }
      end
    end
  end

  def flash
    @date = newsrelease_date(params)
    @newsrelease = @date.nil? ? last_newsrelease : Roompen.document(newspaper_url)
    @pages = newsrelease_archive_box(@newsrelease).children

    @pages_data = @pages.map do |page|
      page.category_page.slice(:id, :title) if page.has_category_page?
    end.uniq.reject(&:nil?)

    @pages_xml_items = @pages_data.map{|item| item[:title]}

    render layout: 'flash'
  end

  def calendar
    date_begin = Time.new(params[:year], params[:month])
    date_end = date_begin + 1.month

    newsreleases = Roompen.categorized(%w(kinds newsrelease), limit: 31, from: date_begin, to: date_end)

    days = newsreleases.each_with_object({}) do |newsrelease, h|
      h[newsrelease.published_at.day] = newsrelease.url
    end

    render json: days
  end

  def owl_newspaper_issue
    use_case = NewsreleaseShow.new(**params.symbolize_keys)
    use_case.call

    @newsrelease = use_case.newsrelease
    @pages = use_case.pages
    @date = newsrelease_date(params)

    @has_flash = has_flash?(@newsrelease)
    @epub = epub_info(@newsrelease)
    @podcast = PodcastByNewsrelease.new(@newsrelease).call

    @owl_status = params[:owl_status].to_s.downcase
    @owl_reason = params[:owl_reason]
    @owl_state  = params[:owl_state]

    respond_to do |format|
      format.html { render layout: false }
      format.mobile { render layout: false }
      format.json { render json: { date: @date, newsrelease: @newsrelease, pages: @pages, epub: @epub, podcast: @podcast } }
    end
  end

private
  def last_newsrelease
    newsreleases = Roompen.categorized(%w(project vedomosti), %w(kinds newsrelease), without: [%w(parts news)], limit: 1, full: true)
    if newsreleases.size == 1
      return newsreleases.first
    else
      raise ActionController::RoutingError.new('Not Found')
    end
  end

  def newsrelease_date(params)
    if [:year, :month, :day].all? { |key| params[key] }
      Date.new(params[:year].to_i, params[:month].to_i, params[:day].to_i)
    else
      nil
    end
  end

  def newspaper_url
    [Settings.domain, 'newspaper', @date.strftime('%Y/%m/%d')].join('/')
  end

  def newsrelease_archive_box(newsrelease)
    newsrelease.has_boxes? && newsrelease.boxes.find{|box| box.type == 'newsrelease_archive'}
  end

  def has_flash?(newsrelease)
    newsrelease.has_boxes? && (newsrelease_archive_box(newsrelease).present? && newsrelease_archive_box(newsrelease).has_children?)
  end

  def epub_info(newsrelease)
    if epub_box = newsrelease.has_boxes? && newsrelease.boxes.find{|box| box.type == 'newsrelease_epub'}
      Hashie::Mash.new(epub_box.epub[:original])
    end
  end
end
