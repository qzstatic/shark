class CompaniesController < ApplicationController
  include Catalogue
  INDEX = 'companies'

  before_action :init_index!, only: %i(main by_filter by_group)
  before_action :init_groups!, only: %i(by_filter by_group)

  # GET /:filter
  def main
    raise Roompen::NotFound if @index.blank?
    redirect_to URI.escape("/companies/#{filter}/#{@index.first}") if is_mobile?
    init_items!("#{index_name}/#{filter}")
  end

  # GET /:filter/:slug
  def by_filter
    @group = @groups.first
    init_items!("#{index_name}/#{filter}/#{slug}/groups/#{group}")
    render 'companies/index'
  end

  # GET /index/:letter/groups/:group
  def by_group
    init_items!("index/#{letter}/groups/#{group}")
    render 'companies/index'
  end

  def search
    init_items!('companies/search', q: search_query)
  end

  def show
    find_company
    separate_tabs(@company)
    @tab_slug = params[:tab].present? ? params[:tab].to_sym : :about

    respond_to do |format|
      format.html
      format.json { render json: { company: @company, press_releases: @press_releases, tabs: @tabs, tab_slug: @tab_slug, tab_names: @tab_names } }
    end
  end

  def press_releases
    find_company
    # последние прессрелизы
    @press_releases = Roompen.bound_documents(@company.id, section: 'company', limit: 20)
    last_year = @press_releases.first ? @press_releases.first.published_at.year : Time.now.year
    @years = (press_release_first_year..last_year).to_a.reverse

    separate_tabs(@company)
    @tab_slug = :press_releases

    respond_to do |format|
      format.html
      format.json { render json: { company: @company, press_releases: @press_releases, years: @years, tabs: @tabs, tab_slug: @tab_slug, tab_names: @tab_names } }
    end
  end

  def company_messages
    find_company

    # последний год с сообщениями
    last_message = Roompen.bound_documents(@company.id, section: 'messages_partner', limit: 1).first
    last_year = last_message ? last_message.published_at.year : Time.now.year

    @years = (company_message_first_year..last_year).to_a.reverse

    @year = params[:year].to_i
    @year = last_year if @year < 2000 || @year > last_year

    @page = params[:page].to_i
    @page = 1 if @page<1

    @limit = company_messages_limit
    @offset = @limit * (@page - 1)

    # сообщения за указанный год
    @company_messages = Roompen.bound_documents(
      @company.id,
      section: 'messages_partner',
      from: Time.new(@year).beginning_of_year,
      to: Time.new(@year).end_of_year,
      limit: @limit,
      offset: @offset
    )

    separate_tabs(@company)
    @tab_slug = :company_messages

    respond_to do |format|
      format.html
      format.json { render json: {
        company:          @company,
        company_messages: @company_messages,
        years:            @years,
        year:             @year,
        page:             @page,
        limit:            @limit,
        offset:           @offset,
        tabs:             @tabs,
        tab_slug:         @tab_slug,
        tab_names:        @tab_names
      } }
    end
  end

  def press_release
    find_press_release
    @company = @press_release.company if @press_release.has_company?
    @press_releases = if @company
      Roompen.bound_documents(@company.id, section: 'company', limit: 20).reject { |release| release.id == @press_release.id }.first(10)
    else
      []
    end
    
    respond_to do |format|
      format.html
      format.json { render json: { press_release: @press_release, press_releases: @press_releases, company: @company } }
      format.xml
    end
  end

  def company_message
    find_company_message
    @company = @company_message.links[:messages_partner].first.bound_document if @company_message.links[:messages_partner].present?
    @company_messages = if @company
      Roompen.bound_documents(@company.id, section: 'messages_partner', limit: company_messages_limit).reject { |message| message.id == @company_message.id }.first(10)
    else
      []
    end
    
    respond_to do |format|
      format.html
      format.json { render json: { company_message: @company_message, company_messages: @company_messages, company: @company } }
      format.xml
    end
  end

  def all_press_releases
    find_company
    # последние прессрелизы
    @press_releases = Roompen.bound_documents(@company.id, section: 'company', limit: 20)
    last_year = @press_releases.first ? @press_releases.first.published_at.year : Time.now.year

    @years = (press_release_first_year..last_year).to_a.reverse

    respond_to do |format|
      format.html
      format.json { render json: { company: @company, press_releases: @press_releases, years: @years } }
    end
  end

  def press_releases_by_year
    find_company
    @year = params[:year]
    # прессрелизы за указанный год
    @press_releases = Roompen.bound_documents(
      @company.id,
      section: 'company',
      from: Time.new(@year).beginning_of_year,
      to: Time.new(@year).end_of_year,
      limit: 200
    )

    # последний год с прессрелизами
    last_pressrelease = Roompen.bound_documents(@company.id, section: 'company', limit: 1).first
    last_year = last_pressrelease ? last_pressrelease.published_at.year : Time.now.year

    @years = (press_release_first_year..last_year).to_a.reverse

    separate_tabs(@company)
    @tab_slug = :press_releases

    respond_to do |format|
      format.html   { render template: 'companies/press_releases' }
      format.mobile { render template: 'companies/press_releases' }
      format.json   { render json: { company: @company, press_releases: @press_releases, years: @years, year: @year, tabs: @tabs, tab_slug: @tab_slug, tab_names: @tab_names } }
    end
  end

private
  def find_company
    url = [Settings.domain, 'companies', URI.escape(params[:slug])].join('/')
    @company = Roompen.document(url)
  end

  def find_press_release
    url = [Settings.domain, 'press_releases', URI.escape(params[:suburl])].join('/')
    @press_release = Roompen.document(url)
  end

  def find_company_message
    url = [Settings.domain, 'company_messages', URI.escape(params[:suburl])].join('/')
    @company_message = Roompen.document(url)
  end

  def press_release_first_year
    # первый год с прессрелизами (необходимо указание сортировки в roompen):
    first_pressrelease = Roompen.bound_documents(@company.id, section: 'company', limit: 1, order: :asc).first
    first_pressrelease ? first_pressrelease.published_at.year : Time.now.year
  end

  def company_message_first_year
    # первый год с сообщениями компании:
    first_message = Roompen.bound_documents(@company.id, section: 'messages_partner', limit: 1, order: :asc).first
    first_message ? first_message.published_at.year : Time.now.year
  end

  def company_messages_limit
    Settings.companies.company_messages_on_page.to_i
  end

  def separate_tabs(company)
    @press_releases = Roompen.bound_documents(@company.id, section: 'company', limit: 1) if @press_releases.nil?
    @company_messages = Roompen.bound_documents(@company.id, section: 'messages_partner', limit: 1) if @company_messages.nil?

    uc = Companies::Tabs.new(company)
    @tabs = uc.call
    @tabs['press_releases'] = true if @press_releases.present?
    @tabs['company_messages'] = true if @company_messages.present?
    
    add_tabs = []
    add_tabs << Hashie::Mash.new(slug: :company_messages, tab_name: 'Информация для абонентов') if @company_messages.present?
    add_tabs << Hashie::Mash.new(slug: :press_releases, tab_name: 'Пресс-релизы')
    
    @tab_names = uc.tab_names.insert(
      1,
      *add_tabs
    )
  end

  def index_name
    INDEX
  end

  def init_industries!
    @index = get("/#{index_name}/industries")
  end

  def init_groups_of_industry!
    @groups = get("/#{index_name}/industries/#{industry}/groups")
  end

  def industry
    @industry ||= params.require(:industry)
  end

  def group
    @group ||= params.require(:group)
  end
end
