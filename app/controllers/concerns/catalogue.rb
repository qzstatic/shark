module Catalogue
  def init_index!
    @index = get("#{index_name}/#{filter}/list")
  end

  def init_groups!
    @groups = get("#{index_name}/#{filter}/#{slug}/groups")
  end

  def init_items!(url, options = {})
    @items = get(url, options)
  end

  def filter
    params.require(:filter)
  end

  def slug
    @slug ||= params.require(:slug)
  end

  def group
    @group ||= params.require(:group)
  end

  def search_query
    params[:q] || '*'
  end

  def get(url, options = {})
    response = regulus_client.get(URI.escape(url), options)
    response.success? ? response.body : {}
  end

  def index_name
    throw 'You must implement `index_name` method in your class'
  end
end