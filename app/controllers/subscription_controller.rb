class SubscriptionController < ApplicationController
  layout 'subscription2'

  def engineering_works

  end

  def step1
    set_return_to_cookie
    
    if @prices = request_prices
      user_data
    else
      head :unprocessable_entity
    end
  end
  
  def prices
    @prices = request_prices
    render json: @prices
  end
  
  def step2
    if @prices = request_prices
      user_data
      set_return_to_cookie(subscription_step3_url)
    else
      head :unprocessable_entity
    end
  end
  
  def step3
    user_data
    unset_cookie(:return_to)
  end
  
  def corporate
  end
  
  def gift
  end
  
  def representational
  end
  
  # RS card
  def payment_card
    response = client.post(request.ip, "/shark/orders/card", order_data)
    
    if response.success? && response.body.fetch(:wait, {}).key?(:uuid)
      url = "//#{Settings.hosts.shark}/subscription/card_order_check/#{response.body[:wait][:uuid]}"
      render json: { url: url }
    else
      head :unprocessable_entity
    end
  end

  # Yandex Money
  def payment_yandex
    response = client.post(request.ip, "/shark/orders/yandex", order_data)
    
    if response.success? && response.body.key?(:post)
      render json: { url: response.body[:post], attributes: response.body[:attributes] }
    else
      head :unprocessable_entity
    end
  end
  
  def card_order_check
    response = client.get(request.ip, "/shark/orders/card/check?uuid=#{params[:uuid]}")
    
    if response.success?
      render json: response.body
    else
      head :unprocessable_entity
    end
  end
  
  def card_callback
    data = { trans_id: params.required(:trans_id) }
    response = client.post(request.ip, '/shark/orders/card/callback', data)

    if response.success? && response.body.fetch(:wait, {}).key?(:uuid)
      @url = "//#{Settings.hosts.shark}/subscription/card_order_check/#{response.body[:wait][:uuid]}"
      @order = response.body.fetch(:order, {})
    else
      @error = 'Ошибка'
    end
  end
  
private
  def user_data
    @user = user(access_token)
  end

  def request_prices
    response = client.get(request.ip, '/shark/prices/active')
    response.body if response.success?
  end
  
  def order_data
    {
      order: params.require(:order).permit(%i(product period student auto_renewing)),
      access_token: access_token
    }
  end
end
