class TestsController < ApplicationController
  before_action :set_document!,         except: %i(preview preview_result preview_share)
  before_action :set_preview_document!, only:   %i(preview preview_result preview_share)
  before_action :set_test!, if: -> { @document }

  # GET /tests/*url или /tests/*url/q:step
  # Рендерит html с тестом
  def show
    @step = params[:step]
    render_test
  end

  
  # GET /preview/tests/:document_id(/q:step)
  def preview
    @preview = true
    show
  end

  # GET /tests/*url/r:result_id
  # Показывает страницу результата теста
  def result
    @result_id = params[:result_id]
    respond_to do |format|
      format.mobile {render 'document/test' }
      format.html { render 'document/main' }
      format.json { render json: { document: @document, result_id: @result_id } }
    end
  end

  # GET /preview/tests/:document_id/r:result_id
  def preview_result
    @preview = true
    result
  end

  # GET /tests/*url/s:index
  def share
    @correct_answers = (0..(@test_json['questions'].length + 1)).to_a.map{|i| Digest::MD5.hexdigest(i.to_s)}.index(params[:hash])
    @test_result =  @test_json['results'][params[:index].to_i]

    render_test
  end

  # GET /preview/tests/:document_id/s:index
  def preview_share
    @preview = true
    share
  end

private

  # Создает @test_json переменную.
  # Достает её из Tanagra, используя gem Roompen
  # @return [Hash] json теста
  def set_document!
    url = [Settings.domain, params[:rubric] ? "#{params[:rubric]}/test" : 'tests', URI.escape(params[:url])].join('/')
    @document = Roompen.document(url)
    if @document.redirect?
      set_cookie(:original_referer, request.referrer) if cookies[:original_referer].nil?
      redirect_to request.url.sub(url, @document.location), status: :moved_permanently
    end
  end

  def set_preview_document!
    @document = Roompen.document_preview(params[:document_id])
  end

  def set_test!
    @test_json = JSON.parse(@document.boxes.find { |box| box.type == 'test' }.json)
  end

  def render_test
    respond_to do |format|
      format.mobile { render @step ? 'document/test' : 'document/test_start_page' }
      format.html { @step ||= 1; render 'document/main' }
      format.json { render json: { document: @document, step: @step || 1 } }
    end
  end
end
