class BroadcastingsController < ApplicationController
  PER_PAGE = 15

  before_action :find_document

  # GET /check_new_posts/:start_date
  # Рендерит количество новых постов с момента params[:start_date]
  def check_new_posts
    start_date = begin
      Time.parse(params[:start_date].to_s)
    rescue ArgumentError
      nil
    end
    boxes = @document.boxes
    # Заглушка на случай, если у бокса пустой created_at.
    boxes.select! { |box| (start_date < Time.parse(box.created_at)) rescue false } if start_date
    render json: { count: boxes.count }
  end

  # GET /, /p:page
  # Рендерит страницу боксов, номер страницы передается через URL, по умолчанию page = 1
  def show
    if @document.redirect?
      set_cookie(:original_referer, request.referrer) if cookies[:original_referer].nil?
      redirect_to "http://www.#{@document.location}", status: :moved_permanently
    else
      expires_in 1.year, public: true, must_revalidate: true
      response.headers['Last-Modified'] = @document.last_published_at.httpdate
      unset_cookie(:original_referer)

      render_boxes_chunk
    end
  end

  private

  # Достает документ из tanagra, используя gem Roompen
  def find_document
    url = [Settings.domain, params.require(:rubric), 'online', URI.escape(params[:url])].join('/')
    @document = Roompen.document(url)
  end

  # Рендерит страницу с боксами
  def render_boxes_chunk
    offset = (params.require(:page).to_i - 1) * PER_PAGE
    @range = offset..offset + PER_PAGE - 1
    @PER_PAGE = PER_PAGE
    @current_page = params.require(:page).to_i
    respond_to do |format|
      format.html { render 'broadcastings/show' }
      format.mobile { render 'm/document/owl_show'}
    end
  end
end
