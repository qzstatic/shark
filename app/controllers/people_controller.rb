class PeopleController < ApplicationController
  include Catalogue
  INDEX = 'people'

  before_action :init_index!, only: %i(main by_filter by_group search)
  before_action :init_groups!, only: %i(by_filter by_group)

  # GET /:filter
  def main
    raise Roompen::NotFound if @index.blank?
    redirect_to URI.escape("/people/#{filter}/#{@index.first}") if is_mobile?
    init_items!("#{index_name}/#{filter}")
  end

  # GET /:filter/:slug
  def by_filter
    @group = @groups.first
    init_items!("#{index_name}/#{filter}/#{slug}/groups/#{group}")
    render 'people/index'
  end

  # GET /:filter/:slug/groups/:group
  def by_group
    init_items!("#{index_name}/#{filter}/#{slug}/groups/#{group}")
    render 'people/index'
  end

  # GET /search
  def search
    init_items!("#{index_name}/search", q: search_query)
  end

  def show
    find_person
  end

  private

  def find_person
    url = [Settings.domain, 'people', URI.escape(params[:url])].join('/')
    @person = Roompen.document(url)
  end

  def index_name
    INDEX
  end
end
