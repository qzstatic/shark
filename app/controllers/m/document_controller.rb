class M::DocumentController < ApplicationController
  def show
    url = [Settings.domain, URI.escape(params[:url])].join('/')
    @document = Roompen.document(url)
    
    if @document.redirect?
      set_cookie(:original_referer, request.referrer) if cookies[:original_referer].nil?
      redirect_to "http://m.#{@document.location}", status: :moved_permanently
    else
      unset_cookie(:original_referer)
      categories_for_stats(@document.categories.as_json)

      @paywall_visit_url = @document.url if @document.pay_required?
      
      respond_to do |format|
        format.html
        format.json {
          render json: {
            document: DocumentSerializer.new(@document, bubo_reason),
            also_in_rubric: @also_in_rubric,
            popular24: @popular24
          }
        }
      end
    end
  end
end
