class ArchiveController < ApplicationController
  helper LaterDude::CalendarHelper
  
  def years
  end
  
  def year
    @year = params[:year].to_i
  end
  
  def day
    @year = params[:year].to_i
    
    @date_begin = Time.new(params[:year], params[:month], params[:day])
    date_end    = @date_begin + 1.day
    
    @documents = Roompen.categorized(%w(kinds materials), without: [%w(library advert_articles), %w(project ampersand)], from: @date_begin, to: date_end, limit: 500)
    
    respond_to do |format|
      format.html
      format.json { render json: { date: @date_begin.strftime('%Y-%m-%d'), documents: @documents } }
    end
  end
end
