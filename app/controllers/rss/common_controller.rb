class Rss::CommonController < Rss::ApplicationController
  layout 'rss/common/common'
  
  def rubric
    @items = Roompen.uncached_list("rubrics-#{params[:rubric_slug]}-top", limit: 30, full: true).documents
    @bodies = bodies(@items)
  
    render_feed
  end
  
  def library
    @items = Roompen.categorized(%w(project vedomosti), %w(kinds materials), ['library', params[:library_slug]], without: [%w(parts news)], limit: 30, full: true, only: :free)
    @bodies = bodies(@items)
  
    render_feed
  end
  
  def subrubric
    @items = Roompen.uncached_list(
      "rubrics-#{params[:rubric_slug]}-#{params[:subrubric_slug]}-top",
      only: :free,
      limit: 30,
      full: true
    ).documents
    @bodies = bodies(@items)
    
    render_feed
  end
  
  def news
    list_slug = if params[:rubric_slug].present?
      "rubrics-#{params[:rubric_slug]}-news"
    else
      'main-news'
    end
    @news_items = Roompen.uncached_list(list_slug, limit: 30, full: true).documents
    @bodies = bodies(@news_items)
    
    respond_to do |format|
      format.xml  { render template: 'rss/common/news' }
      format.json { render json: @news_items }
    end
  end
  
  def articles
    @news_items = Roompen.uncached_list('main-top', limit: 30, full: true).documents
    @bodies = bodies(@news_items)
    
    respond_to do |format|
      format.xml  { render template: 'rss/common/news' }
      format.json { render json: @news_items }
    end
  end
  
  def issue
    use_case = NewsreleaseShow.new(full: true)
    use_case.call
    @news_items = use_case.documents
    @bodies = bodies(@news_items)
    @document = use_case.newsrelease
    
    @title = '"Ведомости". Ежедневная деловая газета. Материалы'
    @description = "#{@document.title} от #{@document.published_at.strftime('%d.%m.%Y')}"
    
    respond_to do |format|
      format.xml  { render template: 'rss/common/news' }
      format.json { render json: @news_items }
    end
  end
  
  def screensaver
    @scr_items = Roompen.uncached_list('main-top', limit: 4).documents
    
    respond_to do |format|
      format.xml  { render template: 'rss/common/screensaver', layout: false }
      format.json { render json: @scr_items }
    end
  end
  
  def sitemap
    @sitemap_items = []
    
    sitemap_main
    sitemap_parts
    sitemap_rubrics
    sitemap_documents
    
    respond_to do |format|
      format.xml  { render layout: 'rss/common/sitemap' }
      format.json { render json: @sitemap_items }
    end
  end
  
  def sitemap_google_news
    news = Roompen.categorized(
      %w(project vedomosti),
      %w(kinds materials),
      %w(parts news),
      without: [%w(library advert_articles)],
      from: 2.days.ago,
      to: Time.now,
      limit: 1000,
      only: :free
    )
    
    articles = Roompen.categorized(
      %w(project vedomosti),
      %w(kinds materials),
      %w(parts articles),
      %w(rubrics),
      without: [%w(library advert_articles), %w(rubric opinion)],
      from: 2.days.ago,
      to: Time.now,
      limit: 1000,
      only: :free
    )
    
    @sitemap_google_news_items = news + articles
    @sitemap_google_news_items.reject! { |d| d.part.slug == 'news' && d.source_url[/vedomosti/].nil? }
    
    respond_to do |format|
      format.xml  { render layout: 'rss/common/sitemap_google_news' }
      format.json { render json: @sitemap_items }
    end
  end
  
  def monitor
    @monitor_items = Roompen.uncached_list('main-news', limit: 100).documents
    @quotes = quotes
    
    respond_to do |format|
      format.xml  { render template: 'rss/common/monitor', layout: false }
      format.json { render json: @monitor_items }
    end
  end
  
private
  def quotes
    response = client.get(request.ip, '/shark/quotes/main_page')
    response.body if response.success?
  end

  def render_feed
    respond_to do |format|
      format.xml  { render template: 'rss/common/common' }
      format.json { render json: @items }
    end
  end
  
  def sitemap_main
    @sitemap_items += [
      {
        url: root_url(subdomain: 'www'),
        lastmod: Time.now,
        changefreq: 'hourly',
        priority: 1.0
      },
      {
        url: newsline_url(subdomain: 'www'),
        lastmod: 1.hour.ago,
        changefreq: 'hourly',
        priority: 0.9
      }
    ]
  end
  
  def sitemap_parts
    @sitemap_items += Roompen.child_categories_of(['parts']).each_with_object([]) do |part, array|
      array << {
        url: part_url(part_slug: part.slug, subdomain: 'www'),
        lastmod: 1.hour.ago,
        changefreq: 'hourly',
        priority: 0.9
      }
    end
  end
  
  def sitemap_rubrics
    @sitemap_items += Roompen.child_categories_of(['rubrics']).each_with_object([]) do |rubric, array|
      array << {
        url: rubric_url(rubric_slug: rubric.slug, subdomain: 'www'),
        lastmod: 1.hour.ago,
        changefreq: 'hourly',
        priority: 0.9
      }
      
      array << {
        url: rubric_news_url(rubric_slug: rubric.slug, subdomain: 'www'),
        lastmod: 1.hour.ago,
        changefreq: 'hourly',
        priority: 0.9
      } unless rubric.slug == 'opinion'

      Roompen.child_categories_of(['rubrics', rubric.slug, 'subrubrics']).each do |subrubric|
        array << {
          url: subrubric_url(rubric_slug: rubric.slug, subrubric_slug: subrubric.slug, subdomain: 'www'),
          lastmod: 1.hour.ago,
          changefreq: 'hourly',
          priority: 0.9
        }
      end if !rubric.slug.eql?('partner')
    end
  end
  
  def sitemap_documents
    documents = Roompen.categorized(%w(project vedomosti), %w(kinds materials), without: [%w(library advert_articles)], limit: 2000)
    @sitemap_items += documents.map do |document|
      {
        url: "http://www.#{document.url}",
        lastmod: document.published_at,
        changefreq: 'daily',
        priority: 0.8
      }
    end
  end
end
