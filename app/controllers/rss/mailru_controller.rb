class Rss::MailruController < Rss::ApplicationController
  def issue_news
    news = Roompen.uncached_list('main-news', limit: 30, full: true).documents
    use_case = NewsreleaseShow.new(full: true)
    use_case.call
    
    @items = news + use_case.documents
    @bodies = bodies(@items)
    
    respond_to do |format|
      format.xml  { render layout: 'rss/mailru/issue_news' }
      format.json { render json: @items }
    end
  end
  
  def news
    @news_items = Roompen.uncached_list('main-news', limit: 30, full: true).documents
    @bodies = bodies(@news_items)
    
    respond_to do |format|
      format.xml  { render layout: 'rss/mailru/issue_news' }
      format.json { render json: @news_items }
    end
  end
  
  def informer
    list_name = params[:type] == 'manual' ? 'for_mailru' : 'main-top'
    documents = Roompen.uncached_list(list_name, limit: 10).documents
    
    news = documents.map do |document|
      {
        img:      "https://#{Settings.hosts.agami}/assets/mail_informer_dummy.png",
        title:    document.title,
        datetime: Russian.strftime(document.published_at, "%d %B"),
        url:      "https://www.#{document.url}"
      }
    end
    
    render json: {
      logo: "https://#{Settings.hosts.agami}/assets/mail_informer.png",
      news: news
    }
  end
  
  def articles
    @news_items = Roompen.categorized(%w(project vedomosti), %w(kinds materials), without: [%w(parts news), %w(library advert_articles)], full: true, limit: 200, only: :free)
    @bodies = bodies(@news_items)
    
    respond_to do |format|
      format.xml  { render layout: 'rss/mailru/articles' }
      format.json { render json: @news_items }
    end
  end
end
