class Rss::ApplicationController < ApplicationController

  def not_found
    render layout: false, status: :not_found
  end

private
  def bodies(items)
    items.each_with_object(Hash.new) do |document, bodies|
      bodies[document.id] = rssify(document)
    end
  end
  
  def rssify(document)
    paragraphs = document.boxes.select { |box| box.type == 'paragraph' }.map(&:body)
    
    images = document.boxes.select { |box| box.type == 'inset_image' }.map do |box|
      box.image.fullscreen.url if box.has_image?
    end.compact
    
    galleries = document.boxes.select { |box| box.type == 'gallery' }.map(&:children).flatten.select(&:has_image?).map(&:image).map(&:fullscreen).map(&:url).compact
    paragraphs << document.announce.to_s if document.part.try(:slug) == 'galleries'
    
    authors = []
    if document.has_author?
      authors << document.author.title
    else
      authors += document.authors.map(&:bound_document).map(&:title)
    end
    
    {
      description: paragraphs.first,
      body:        paragraphs.join(?\n),
      rubric:      (document.rubric.title if document.has_rubric?),
      author:      authors.join(', '),
      images:      images + galleries
    }
  end
end
