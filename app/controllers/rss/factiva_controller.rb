class Rss::FactivaController < Rss::ApplicationController
  layout 'rss/factiva/factiva'
  
  def issue
    use_case = NewsreleaseShow.new(full: true)
    use_case.call
    @items = use_case.documents
    @bodies = bodies(@items)
    @document = use_case.newsrelease
    
    @title = '"Ведомости". Ежедневная деловая газета. Материалы'
    @description = "#{@document.title} от #{@document.published_at.strftime('%d.%m.%Y')}"
    
    respond_to do |format|
      format.xml
      format.json { render json: @items }
    end
  end
  
  def news
    @news_items = Roompen.uncached_list('main-news', limit: 30, full: true).documents
    @bodies = bodies(@news_items)
    
    @title = '"Ведомости". Ежедневная деловая газета. Новости'
    @description = 'Новости с сайта Vedomosti.ru'
    
    respond_to do |format|
      format.xml
      format.json { render json: @news_items }
    end
  end
  
  def articles
    @news_items = Roompen.uncached_list('main-top', limit: 30, full: true).documents
    @bodies = bodies(@news_items)
    
    @title = '"Ведомости". Ежедневная деловая газета. Онлайн материалы.'
    @description = 'Материалы с сайта Vedomosti.ru'
    
    respond_to do |format|
      format.xml  { render template: 'rss/factiva/news' }
      format.json { render json: @news_items }
    end
  end
end
