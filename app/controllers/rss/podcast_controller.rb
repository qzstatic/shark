class Rss::PodcastController < Rss::ApplicationController
  def podcast
    @items = PodcastsItems.new(limit: 20).call
    
    respond_to do |format|
      format.xml  { render layout: 'rss/podcast/podcast' }
      format.json { render json: @items }
    end
  end
end
