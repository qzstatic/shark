class Rss::AnewsController < Rss::ApplicationController
  def news
    @news_items = Roompen.uncached_list('main-news', limit: 30, full: true, only: :free).documents
    @bodies = bodies(@news_items)
    
    respond_to do |format|
      format.xml  { render template: 'rss/common/news', layout: 'rss/common/common' }
      format.json { render json: @news_items }
    end
  end
  
  def all
    @news_items = Roompen.categorized(
      %w(project vedomosti), %w(kinds materials),
      without: [%w(library advert_articles)],
      limit: 30,
      full: true,
      only: :free
    )
    
    @bodies = bodies(@news_items)
    
    respond_to do |format|
      format.xml  { render template: 'rss/common/news', layout: 'rss/common/common' }
      format.json { render json: @news_items }
    end
  end
  
  def issue_articles
    articles = Roompen.uncached_list('main-top', limit: 30, full: true, only: :free).documents
    use_case = NewsreleaseShow.new(full: true)
    use_case.call
    
    @news_items = articles + use_case.documents
    @bodies = bodies(@news_items)
    
    respond_to do |format|
      format.xml  { render template: 'rss/common/news', layout: 'rss/common/common' }
      format.json { render json: @news_items }
    end
  end
end
