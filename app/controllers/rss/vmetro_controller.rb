class Rss::VmetroController < Rss::ApplicationController
  def all
    @items = Roompen.categorized(
      %w(project vedomosti),
      %w(kinds materials),
      without: [%w(library advert_articles)],
      limit: 30,
      full: true,
      only: :free
    )
    @bodies = bodies(@items)
    @popular24 = Roompen.list('popular24')
    
    respond_to do |format|
      format.xml  { render layout: 'rss/vmetro/all' }
      format.json { render json: @items }
    end
  end
end
