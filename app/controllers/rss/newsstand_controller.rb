class Rss::NewsstandController < Rss::ApplicationController
  def newsstand
    newsreleases = Roompen.categorized(%w(project vedomosti), %w(kinds newsrelease), without: [%w(parts news)], limit: 5, only: :free)
    
    if !newsreleases.empty?
      @newsrelease = newsreleases.first
    else
      raise ActionController::RoutingError.new('Not Found')
    end
    
    @newsstand = newsreleases.find { |document| document.has_newsstand? }.newsstand
    
    respond_to do |format|
      format.xml  { render layout: false }
      format.json { render json: @newsrelease }
    end
  end
end
