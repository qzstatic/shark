class Rss::MedialogiaController < Rss::ApplicationController
  def news
    news = Roompen.categorized(
      %w(project vedomosti),
      %w(kinds materials),
      %w(parts news),
      limit: 200,
      full: true
    )

    press_releases = Roompen.categorized(
      %w(project vedomosti),
      %w(kinds press_releases),
      limit: 200,
      full: true
    )
    
    @news_items = (news + press_releases).sort{ |item1, item2| item2.published_at <=> item1.published_at }[0..199]
    @bodies = bodies(@news_items)
    
    respond_to do |format|
      format.xml  { render 'rss/yandex/news', layout: 'rss/yandex/news' }
      format.json { render json: @news_items }
    end
  end
end
