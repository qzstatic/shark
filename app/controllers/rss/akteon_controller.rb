class Rss::AkteonController < Rss::ApplicationController

  def newsrelease
    use_case = NewsreleaseShow.new(full: true, year: year, month: month, day: day)
    use_case.call
    @documents = use_case.documents.reverse
    @document = use_case.newsrelease

    @title = '"Ведомости". Ежедневная деловая газета. Материалы'
    @description = "#{@document.title} от #{@document.published_at.strftime('%d.%m.%Y')}"

    respond_to do |format|
      format.xml  { render layout: false }
      format.json { render json: @documents }
    end
  end

  private

  def year
    params.fetch(:year, Date.today.year)
  end

  def month
    params.fetch(:month, Date.today.month)
  end

  def day
    params.fetch(:day, Date.today.day)
  end
end
