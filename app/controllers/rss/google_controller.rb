class Rss::GoogleController < Rss::ApplicationController
  layout 'rss/google/google'
  
  def rubric
    @title = '«Ведомости». Материалы'
    @items = Roompen.categorized(%w(project vedomosti), %w(kinds materials), ['rubrics', params[:rubric_slug]], without: [%w(library advert_articles)], limit: 30, full: true, only: :free)
    @bodies = bodies(@items)
    
    render_feed
  end
  
  def news
    @title = '«Ведомости». Новости'
    @items = Roompen.uncached_list('main-news', limit: 30, full: true, only: :free).documents
    @bodies = bodies(@items)
    
    render_feed
  end
  
  def top
    @title = '«Ведомости». Ежедневная деловая газета'
    @description = '«Ведомости». Ежедневная деловая газета. Топ новости.'
    @top_items = Roompen.uncached_list('popular24', limit: 5, full: true).documents
    @bodies = bodies(@top_items)
    
    render layout: 'rss/google/top'
  end
  
private
  def render_feed
    @description = @title
    
    respond_to do |format|
      format.xml  { render template: 'rss/google/news' }
      format.json { render json: @items }
    end
  end
end
