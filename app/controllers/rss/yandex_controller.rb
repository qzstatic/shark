class Rss::YandexController < Rss::ApplicationController
  def issue
    @items = Roompen.categorized(%w(project vedomosti), %w(kinds materials), without: [%w(parts news), %w(library advert_articles)], full: true, only: only, limit: 200)
    @bodies = bodies(@items)
    
    respond_to do |format|
      format.xml  { render layout: 'rss/yandex/issue' }
      format.json { render json: @items }
    end
  end

  def broadcasting
    @document = Roompen.document_by_id(params[:id])
    render nothing: true and return unless @document && @document.categories.parts.slug == 'online'
    @broadcastings = @document.boxes.select { |box| %w(paragraph text quote).include?(box.type) }
    respond_to do |format|
      format.xml { render layout: 'rss/yandex/broadcasting'}
    end
  end
  
  def news
    @news_items = Roompen.categorized(%w(project vedomosti), %w(kinds materials), %w(parts news), limit: 200, full: true)
    @bodies = bodies(@news_items)
    
    respond_to do |format|
      format.xml  { render layout: 'rss/yandex/news' }
      format.json { render json: @news_items }
    end
  end
  
  def comments
    use_case = NewsreleaseShow.new(full: true, only: :free)
    use_case.call
    @items = use_case.pages.select { |page| page.slug == 'opinion' }.first.documents
    @bodies = bodies(@items)
    @document = use_case.newsrelease
    
    respond_to do |format|
      format.xml  { render layout: 'rss/yandex/comments' }
      format.json { render json: @items }
    end
  end
  
  def widgets
    @title = "Ведомости"
    @link = "http://#{Settings.hosts.shark}"
    @description = "Материалы выпуска Ведомости"
    
    if params[:region_slug]
      main_top_limit = 2
      
      @region = Roompen.category(['regions', params[:region_slug]])
      
      @title += " — #{@region.title}"
      @link  += "#{@region.slug}"
      @description += " — #{@region.title}"
      
      region_items = Roompen.uncached_list("regions-#{params[:region_slug]}-top", limit: 3, full: true).documents
    else
      main_top_limit = 5
      region_items = []
    end
    
    @items = Roompen.uncached_list('main-top', limit: main_top_limit, full: true).documents + region_items
    @bodies = bodies(@items)
    
    respond_to do |format|
      format.xml  { render layout: 'rss/yandex/widgets' }
      format.json { render json: @items }
    end
  end

  private

  def only
    params.fetch(:paying, :free)
  end
end
