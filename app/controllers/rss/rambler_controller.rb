class Rss::RamblerController < Rss::ApplicationController

  def finance
    @items = Roompen.uncached_list('rubrics-finance-top', limit: 30, full: true, only: :free).documents.select do |doc|
      doc.source_url.to_s.include?('vedomosti.ru')
    end
    @bodies = bodies(@items)

    respond_to do |format|
      format.xml  { render template: 'rss/common/common', layout: 'rss/common/common' }
      format.json { render json: @items }
    end
  end

  def news
    @news_items = Roompen.uncached_list('main-news', limit: 30, full: true).documents
    @bodies = bodies(@news_items)
    
    respond_to do |format|
      format.xml  { render layout: 'rss/rambler/news' }
      format.json { render json: @news_items }
    end
  end
end
