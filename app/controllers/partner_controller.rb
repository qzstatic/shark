class PartnerController < ApplicationController
  layout 'ampersand'

  def index
    @documents = Roompen.categorized(%w(kinds ampersand_release), bound_limit: 4, full: true, bound_order: :asc)

    respond_to do |format|
      format.html
      format.mobile
      format.json { render json: { documents: @documents }}
    end
  end

  def by_date
    @ampersand_release = release_date.nil? ? last_ampersand_release : Roompen.document(release_url)
    @documents = Roompen.bound_documents(@ampersand_release.id, full: false, order: :asc)

    respond_to do |format|
      format.html
      format.mobile
      format.json do
        render json: {
                   ampersand_release: @ampersand_release,
                   documents:         @documents
               }
      end
    end
  end

  def show
    @document = Roompen.document(document_url)
    if @document.redirect?
      set_cookie(:original_referer, request.referrer) if cookies[:original_referer].nil?
      redirect_to "http://www.#{@document.location}", status: :moved_permanently
    else
      expires_in 1.year, public: true, must_revalidate: true
      response.headers['Last-Modified'] = @document.last_published_at.httpdate
      unset_cookie(:original_referer)

      render_document
    end
  end

  private

  def release_url
    [Settings.domain, 'partner', release_date.strftime('%Y/%m/%d')].join('/')
  end

  def document_url
    [Settings.domain, 'partner', params[:url]].join('/')
  end

  def release_date
    @date ||= if [:year, :month, :day].all? { |key| params[key] }
                Date.new(params[:year].to_i, params[:month].to_i, params[:day].to_i)
              else
                nil
              end
  end

  def last_ampersand_release
    newsreleases = Roompen.categorized(%w(kinds ampersand_release), limit: 1, full: true)
    if newsreleases.size == 1
      newsreleases.first
    else
      raise ActionController::RoutingError.new('Not Found')
    end
  end

  def render_document
    @seo.description = @document.has_subtitle? ? "#{@document.title} #{@document.subtitle}" : @document.title
    @seo.keywords = make_keywords(@seo.description)

    respond_to do |format|
      format.html { render 'partner/show'}
      format.mobile { render 'partner/show'}
      format.json do
        render json: {
                   document: @document
               }
      end
    end
  end
  def make_keywords(text)
    text.split(/[,.?!"';: ]+/).select { |t| t.length>3 }.join(',')
  end
end
