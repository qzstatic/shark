class LibrariesController < ApplicationController
  PAGES_LIMIT = 10

  def index
    @libraries = [
      { slug: 'characters',     title: 'Действующие лица' },
      { slug: 'investigations', title: 'Расследования' },
    ]

    respond_to do |format|
      format.html
      format.mobile
      format.json   { render json: { libraries: @libraries } }
    end
  end

  def show
    @library = Roompen.category(['library', params[:slug]])
    @documents = Roompen.categorized(%w(project vedomosti), %w(kinds materials), ['library', params[:slug]], without: [%w(parts news)], limit: limit, offset: offset)
    hide_more_button?

    respond_to do |format|
      format.html {
        if page > 1
          render json: { html: render_to_string('libraries/show'), hide_more_button: hide_more_button? }
        else
          render
        end
      }
      format.mobile
      format.json   { render json: {libraries: @library, documents: @documents } }
    end
  end

  private

  def page
    @page ||= (page = (params[:page] || 1).to_i) > PAGES_LIMIT ? 1 : page
  end

  def limit
    50
  end

  def offset
    @offset ||= limit * (page - 1)
  end

  def hide_more_button?
    @hide_more_button ||= (page > PAGES_LIMIT - 1 || (@documents || []).length < limit)
  end
end
