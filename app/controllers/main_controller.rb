class MainController < ApplicationController
  skip_before_action :handle_session, only: :fast_news_owl

  PAGES_LIMIT = 10

  def index
    announce_rss_for(:index)

    @page_type = :index
    mode = is_mobile? ? :phone : :desktop
    use_case = PageShow.new('main', mode: mode)

    if is_desktop?
      @region = params[:region_slug] if params[:region_slug].present?
      region = Roompen.child_categories_of(%w(regions)).find { |region| region.slug == params[:region_slug] }
      @region_name = region.title unless region.nil?
    end

    render_page(use_case)
  end

  def rubric
    @rubric = Roompen.category(['rubrics', params[:rubric_slug]])
    announce_rss_for(:rubric, @rubric)

    mode = is_mobile? ? :phone : :desktop
    use_case = PageShow.new('rubrics', params[:rubric_slug], mode: mode)
    @page_type = :rubric
    @page_title = 'Главная'

    render_page(use_case)
  end

  def subrubric
    @rubric    = Roompen.category(['rubrics', params[:rubric_slug]])
    @subrubric = Roompen.category(['rubrics', params[:rubric_slug], 'subrubrics', params[:subrubric_slug]])

    announce_rss_for(:subrubric, @rubric, @subrubric)

    @documents = Roompen.uncached_list("rubrics-#{params[:rubric_slug]}-#{params[:subrubric_slug]}-top", limit: limit, offset: offset ).documents
    hide_more_button?

    respond_to do |format|
      format.html {
        if page > 1
          render json: { html: render_to_string('main/subrubric'), hide_more_button: hide_more_button? }
        else
          render
        end
      }
      format.mobile { render 'blocks/_list', locals: { list: @documents } }
      format.json   { render json: { documents: @documents, subrubric: @subrubric } }
    end
  end

  def section
    @section = Roompen.category(['rubrics', params[:rubric_slug], 'subrubrics', params[:subrubric_slug], 'sections', params[:section_slug]])
    @documents = Roompen.uncached_list(['rubrics', params[:rubric_slug], params[:subrubric_slug], params[:section_slug]].join('-'), limit: limit).documents

    respond_to do |format|
      format.html
      format.json { render json: { documents: @documents, section: @section } }
    end
  end

  def theme
    # Запрос документа типа /themes/neft-prodolzhaet-padenie
    # чтобы узнать категории, по которым выбрать остальные документы цикла.
    url = [Settings.domain, 'themes', params[:slug]].join('/')
    @document = Roompen.document(url)

    cycle_type = @document.categories.themes.slug
    cycle_list = @document.categories.themes.cycle_list.slug

    # работа с массивом документов
    @documents = Roompen.categorized(%w(project vedomosti), ['themes', cycle_type, 'cycle_list', cycle_list], %w(kinds materials), without: [%w(parts news)], limit: 50)

    respond_to do |format|
      format.html
      format.json { render json: { cycle_info: @document, documents: @documents } }
    end
  end

  def region
    @region = Roompen.category(['regions', params[:region_slug]])
    @documents = Roompen.uncached_list("regions-#{params[:region_slug]}-top", limit: limit).documents

    respond_to do |format|
      format.html
      format.json { render json: { documents: @documents, region: @region } }
    end
  end

  def part
    @part = Roompen.category(['parts', params[:part_slug]])
    @documents = page > PAGES_LIMIT ? [] : Roompen.categorized(%w(project vedomosti), ['parts', params[:part_slug]], %w(kinds materials), limit: limit, offset: offset)
    hide_more_button?

    respond_to do |format|
      format.html {
        if page > 1
          render json: { html: render_to_string('main/part'), hide_more_button: hide_more_button? }
        else
          render
        end
      }
      format.json { render json: { documents: @documents, part: @part } }
    end
  end

  def multimedia
    page = Roompen.page('multimedia')
    @lists = page.lists
    @multimedia = true
    respond_to do |format|
      format.html{render params[:action]}
      format.json { render json: { lists: @lists, multimedia: true } }
    end
  end
    
  def subpart
    @part = Roompen.category(['parts', params[:part_slug]])
    @subpart = Roompen.category(['parts', params[:part_slug], 'subparts', params[:subpart_slug]])
    @documents = Roompen.categorized(%w(project vedomosti), ['parts', params[:part_slug], 'subparts', params[:subpart_slug]], %w(kinds materials), limit: 50)

    respond_to do |format|
      format.html
      format.json { render json: { documents: @documents, part: @part, subpart: @subpart } }
    end
  end

private
  def render_page(use_case)
    @news  = use_case.news
    @top   = use_case.top
    @lists = use_case.lists

    respond_to do |format|
      format.html   { render params[:action] }
      format.mobile { render :index }
      format.json   { render json: use_case }
    end
  end

  def page
    @page ||= (page = (params[:page] || 1).to_i) > PAGES_LIMIT ? 1 : page
  end

  def limit
    50
  end

  def offset
    @offset ||= limit * (page - 1)
  end

  def hide_more_button?
    @hide_more_button ||= (page > PAGES_LIMIT - 1 || (@documents || []).length < limit)
  end
end
