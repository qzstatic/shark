class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :exception
  append_view_path Rails.root + 'app/views/amp'  
  append_view_path Rails.root + 'app/views/m'
  
  before_action :allow_iframe_requests, :handle_session, :handle_format, :init_seo
  
  rescue_from Roompen::NotFound,              with: :not_found
  rescue_from ActionController::RoutingError, with: :not_found
  rescue_from AquariumClient::Unauthorized,   with: :unauthorized
  
  helper_method :access_token
  
private
  def categories_for_stats(categories)
    @categories_for_stats ||= view_context.categories_to_tags(categories)
  end
  
  def handle_session
    if !cookies[:new_visitor] && !cookies[:access_token]
      set_cookie(:new_visitor, 1)
      ::NewRelic::Agent.increment_metric('Custom/Sessions/new_visitor')
    end
  end
  
  def handle_format
    request.format = :mobile if is_mobile? && request.format != 'json'
    if request.format == 'adv'
      @adv = true
      request.format = :html
    end
  end
  
  def announce_rss_for(type, *args)
    @rss_announcer = case type
    when :index
      RSSAnnouncer::Index.new
    when :rubric
      RSSAnnouncer::Rubric.new(*args)
    when :subrubric
      RSSAnnouncer::Subrubric.new(*args)
    when :newsline
      RSSAnnouncer::Newsline.new
    end
  end
  
  helper_method :rss_announcer
  def rss_announcer
    @rss_announcer ||= RSSAnnouncer::Null.new
  end
  
  def not_found
    render 'errors/not_found', layout: 'error', status: :not_found
  end
  
  def render_document_body(document)
    render partial: 'document/box', collection: document.boxes
  end
  
  def access_token
    @access_token ||= get_access_token
  end
  
  def get_access_token
    access_token = cookies[:access_token] || request.headers['X-Access-Token']
    access_token == 'new_visitor' ? '' : access_token
  end
  
  def unauthorized
    if Rails.env.development?
      raise
    else
      unset_cookie(:access_token)
      redirect_to root_url
    end
  end
  
  def user(access_token)
    return if access_token.nil? || access_token.empty?
    response = client.get(request.ip, "/shark/users/#{access_token}")
    user_data = { ip: request.ip, access_token: access_token }
    
    User.new(response.body.merge(user_data)) if response.success?
  end
  
  def redline_active
    response = client.get(request.ip, "/shark/redlines/active")
    response.body if response.success?
  end
  
  def client
    AquariumClient.new
  end
  
  def regulus_client
    RegulusClient.new
  end
  
  def is_mobile?
    SubdomainConstraints::M.matches?(request)
  end
  
  def is_desktop?
    !is_mobile?
  end
  
  def set_cookie(name, value, expiration_date = nil)
    ::Rack::Utils.set_cookie_header!(
      response.headers,
      name,
      value:   value,
      expires: expiration_date,
      domain:  Settings.cookie_domain,
      path:    '/'
    )
  end
  
  def unset_cookie(name)
    ::Rack::Utils.set_cookie_header!(
      response.headers,
      name,
      expires: Time.at(0),
      domain:  Settings.cookie_domain,
      path:    '/'
    )
  end
  
  def token_cookie_header(access_token)
    {
      access_token: access_token,
      expires:      20.years.from_now.to_s(:rfc822),
      domain:       Settings.cookie_domain,
      path:         '/'
    }.map { |key, value| "#{key}=#{value}" }.join('; ')
  end
  
  def delete_cookie_and_redirect(fallback = root_url)
    unset_cookie(:return_to)
    redirect_to cookies[:return_to] || fallback
  end
  
  def set_return_to_cookie(url = request.referer)
    set_cookie(:return_to, url)
  end
  
  def allow_iframe_requests
    response.headers.delete('X-Frame-Options')
  end

  def init_seo
    @seo = Hashie::Mash.new(
      description: 'Качественный контент о бизнесе и экономике. Аналитика, финансовые новости, электронная версия газеты Ведомости.',
      keywords: 'Бизнес, Экономика, Финансы, Мнения, Политика, Технологии, Недвижимость, Авто, Менеджмент, Стиль жизни, Действующие лица, Расследования, Промышленность, ТЭК, Торговля и услуги, Агропром, Транспорт, Спорт, Макроэкономика, Бюджет, Госинвестиции, Мировая экономика, Налоги, Банки, Страхование, Аналитика, Власть, Демократия, Международные отношения, Безопасность, Социальная политика, Международная жизнь, Телекоммуникации, Интернет и digital, Медиа, ИТ-бизнес, Стройки и инфраструктура, Архитектура и дизайн, Тест-драйвы, Предпринимательство, Культура, Интервью'
    )
  end

  def bubo_state
    @bubo_state ||= request.headers['X-State']
  end
end
