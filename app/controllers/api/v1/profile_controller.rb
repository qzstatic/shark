module Api
  module V1
    class ProfileController < ApplicationController
      def show
        if profile = user
          render json: profile
        else
          head :not_found
        end
      end
      
      def update
        response = client.patch(request.ip, "/shark/users/#{access_token}", params)
        
        if response.success?
          render json: response.body
        else
          render json: response.body, status: :unprocessable_entity
        end
      end
      
      def accept_promo
        use_case = AcceptPromo.new(params[:promo_name], access_token, request.ip)
    
        if use_case.call
          head :ok
        else
          head :unprocessable_entity
        end
      end

    private
      def user
        response = client.get(request.ip, "/shark/users/#{access_token}/profile")
        User.new(response.body) if response.success?
      end
    end
  end
end
