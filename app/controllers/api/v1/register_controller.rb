module Api
  module V1
    class RegisterController < ApplicationController
      skip_before_filter :verify_authenticity_token
      
      def sign_up_with_password
        registration_form = PasswordRegistrationForm.new(
          access_token: access_token,
          ip: request.ip,
          **params.fetch(:user_with_password, {}).symbolize_keys
        )
        
        use_case = SignUpWithPassword.new(registration_form)
        use_case.call
        
        if use_case.success?
          render json: use_case.user_data
        else
          render json: registration_form.errors, status: :unprocessable_entity
        end
      end
      
      def sign_up_with_oauth
        use_case = SignUpWithOauthToken.new(params[:user_with_oauth], request.ip, access_token)
        
        if use_case.call
          render json: use_case.user
        else
          render json: use_case.errors, status: :unprocessable_entity
        end
      end
      
      def check_email
        use_case = SendPasswordResetEmail.new(params[:email], request.ip)
        use_case.call
        
        if use_case.success?
          render json: { status: 'ok' }
        else
          render json: { status: 'error', errors: ['Пользователь с таким e-mail не найден.'] }, status: :unprocessable_entity
        end
      end
    end
  end
end
