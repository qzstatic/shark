module Api
  module V1
    class PodcastsController < ApplicationController
      def index
        @podcasts = PodcastsItems.new(limit: limit, offset: offset).call

        render json: { podcasts: @podcasts, page: page }
      end

      def by_date
        @podcast = PodcastByNewsrelease.new(newsrelease).call
        not_found and return unless @podcast

        render json: @podcast
      end

      private

      def page
        params.fetch(:page, 1).to_i
      end

      def offset
        limit * (page - 1)
      end

      def limit
        10
      end

      def newsrelease
        @newsrelease ||= Roompen.document("vedomosti.ru/newspaper/#{params[:year]}/#{params[:month]}/#{params[:day]}")
      end
    end
  end
end
