module Api
  module V1
    class MainController < ApplicationController
      PER_PAGE = 30

      before_filter :set_view, only: %i(index rubric)

      def index
        use_case = PageShow.new('main', mode: params.fetch(:view, :phone))

        render json: use_case.as_json.merge(targetings: targetings)
      end

      def rubric
        @rubric  = Roompen.category(['rubrics', params[:rubric_slug]])
        use_case = PageShow.new('rubrics', params[:rubric_slug], mode: params.fetch(:view, :phone))

        render json: use_case.as_json.merge(targetings: targetings)
      end

      def subrubric
        @rubric    = Roompen.category(['rubrics', params[:rubric_slug]])
        @subrubric = Roompen.category(['rubrics', params[:rubric_slug], 'subrubrics', params[:subrubric_slug]])
        list       = Roompen.uncached_list("rubrics-#{params[:rubric_slug]}-#{params[:subrubric_slug]}-top", limit: PER_PAGE, offset: offset)

        render json: { list: list, targetings: targetings }
      end

      def fast_news
        list_slug = if params[:category_slug].present? && params[:slug].present?
                      "#{category_slug}-#{slug}-news"
                    else
                      'main-news'
                    end
        fast_news = Roompen.uncached_list(list_slug, limit: PER_PAGE, offset: offset).documents

        render json: { fast_news: fast_news }
      end

      def part
        part      = Roompen.category(['parts', slug])
        documents = Roompen.categorized(%w(project vedomosti), ['parts', slug], %w(kinds materials))

        render json: { documents: DocumentsSerializer.new(documents), header: part.title }
      end

      def mobile_menu
        mobile_menu = Roompen.options('mobile_menu')

        render json: { mobile_menu: mobile_menu }
      end

      def mobile_menu_v2
        mobile_menu = Roompen.options('mobile_menu_v2')

        render json: { mobile_menu: mobile_menu }
      end

      def story
        url = [Settings.domain, 'story', slug].join('/')
        @document = Roompen.document(url)

        story = Roompen.category(['story', slug])
        fast_news = Roompen.uncached_list("story-#{slug}-news", limit: PER_PAGE, offset: offset).documents
        documents = Roompen.uncached_list("story-#{slug}-top", limit: PER_PAGE, offset: offset).documents

        render json: { top: DocumentsSerializer.new(documents), news: fast_news, header: story.title, targetings: targetings }
      end

      def check_appkey
        if Settings.api.app_keys.include?(app_key)
          head :ok
        else
          head :unauthorized
        end
      end

      def popular24
        render json: Roompen.list('popular24')
      end

      def main_top
        render json: Roompen.list('main-top')
      end

      private

      def set_view
        params[:view] = 'phone' if params[:view].nil?
      end

      def offset
        (page - 1) * PER_PAGE
      end

      def page
        page = params[:page].to_i
        page > 0 ? page : 1
      end

      def category_slug
        params.require(:category_slug)
      end

      def slug
        params.require(:slug)
      end
    end
  end
end
