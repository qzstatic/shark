module Api
  module V1
    class SessionController < ApplicationController
      skip_before_filter :authorize_session!, only: :create
      
      def create
        set_session
        head :ok
      end
      
      def access_rights
        response = client.get(
          request.ip,
          "/shark/sessions/#{access_token}"
        )
        
        if response.success?
          render json: response.body
        else
          head :unauthorized
        end
      end
      
      def sign_in_with_password
        response = client.post(
          request.ip,
          '/shark/sign_in_with_password',
          email:        params[:email],
          password:     params[:password],
          access_token: access_token
        )
        
        if response.success?
          render json: response.body
        else
          head :unprocessable_entity
        end
      end
      
      def sign_in_with_oauth
        use_case = SignInWithOauthToken.new(
          request.ip,
          access_token,
          params[:provider],
          params[:oauth_token],
          params[:oauth_token_secret]
        )
        
        if use_case.call
          render json: use_case.user
        else
          render json: use_case.user, status: :accepted
        end
      end
      
      def sign_out
        client.delete(
          request.ip,
          '/shark/sign_out',
          access_token: access_token
        )
        head :ok
      end
      
    private
      def set_session
        data = client.post(
          request.ip,
          '/shark/sessions',
          session: { ip: request.ip },
          mobile: true
        ).body
        
        response.headers['X-Access-Token'] = data[:access_token]
      end
      
      def client
        AquariumClient.new
      end
    end
  end
end
