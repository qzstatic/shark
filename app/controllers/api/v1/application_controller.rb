module Api
  module V1
    class ApplicationController < ActionController::API
      include ApplicationHelper
      include ActionController::MimeResponds
      include ActionController::ImplicitRender
      
      before_filter :authorize!, :authorize_session!, except: :check_appkey
      
      rescue_from Roompen::NotFound, with: :not_found
      rescue_from AquariumClient::Unauthorized, with: :unauthorized
      
    private
      def not_found
        if Rails.env.development?
          raise
        else
          raise ActionController::RoutingError.new('Not Found')
        end
      end
      
      def authorize!
        unless Settings.api.app_keys.include?(app_key)
          head :unauthorized
        end
      end
      
      def authorize_session!
        unless has_session?
          head :unauthorized
        end
      end
      
      def unauthorized
        head :unauthorized
      end
      
      def app_key
        request.headers['X-App-Key']
      end
      
      def client
        AquariumClient.new
      end
      
      def access_token
        request.headers['X-Access-Token']
      end
      
      def has_session?
        request.headers.key?('X-Access-Token') && !request.headers['X-Access-Token'].empty?
      end

      def bubo_reason
        @bubo_reason ||= request.headers['X-Reason']
      end

      def targetings
        {
            rubrika:        rubric_targeting_params,
            pagetype:       pagetype_targeting_params,
            podrubrika:     subrubric_targeting_params,
            paywall_status: request.cookies['paywallStatus'] || 0,
            referrer:       request.referer,
            ip:             request.remote_ip,
            url:            targeting_url([Settings.domain, request.path.sub('/api/v1', '')].join),
            story:          story_targeting_params.present? ? story_targeting_params : 'NO'
        }
      end
    end
  end
end
