module Api
  module V1
    class SubscriptionController < ApplicationController
      def apple_verify
        use_case = Subscription::AppleVerify.new(request.ip, access_token, params[:receipt])

        if use_case.call
          render json: use_case.response, status: :ok
        else
          render json: use_case.response, status: :unprocessable_entity
        end
      end

      def google_verify
        use_case = Subscription::GoogleVerify.new(request.ip, access_token, params[:subscription_id], params[:token])

        if use_case.call
          render json: use_case.response, status: :ok
        else
          render json: use_case.response, status: :unprocessable_entity
        end
      end

      def link
        use_case = Subscription::Link.new(request.ip, access_token)

        if use_case.call
          render json: use_case.response, status: :ok
        else
          render json: use_case.response, status: :unprocessable_entity
        end
      end

      def products_list
        use_case = Subscription::ProductsList.new(request.ip, params.require(:target))
        if use_case.call
          render json: use_case.response, status: :ok
        else
          render json: use_case.response, status: :unprocessable_entity
        end
      end
    end
  end
end
