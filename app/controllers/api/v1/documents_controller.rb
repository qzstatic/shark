module Api
  module V1
    class DocumentsController < ApplicationController
      def show
        if params[:id].present?
          @document = Roompen.document_by_id(params[:id])
        else
          @document = Roompen.document(params[:url])
          @document = Roompen.document(@document.location.sub(/\Ahttp:\/\//,'')) if @document.redirect?
        end
        
        if stale?(last_modified: @document.last_published_at, public: true)
          expires_in 1.hour, public: true, must_revalidate: true
          
          popular24 = Roompen.list('popular24')
          
          render json: {
            document: DocumentSerializer.new(@document, bubo_reason),
            popular24: popular24,
            targetings: targetings
          }
        end
      end
    end
  end
end
