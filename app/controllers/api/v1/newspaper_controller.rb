module Api
  module V1
    class NewspaperController < ApplicationController
      def index
        documents = Roompen.categorized(%w(project vedomosti), %w(kinds newsrelease), %w(domains vedomosti.ru), full: true, limit: 10)
        
        newsreleases = documents.select do |newsrelease|
          newsrelease.has_boxes? && newsrelease.boxes.find{|box| box.type == 'newsrelease_mobile_archive'}
        end
        
        render json: { newsreleases: newsreleases }
      end

      def by_date
        use_case = NewsreleaseShow.new(**params.symbolize_keys)

        use_case.call
        
        render json: {
          newsrelease: use_case.newsrelease,
          pages: use_case.pages
        }
      end
      
      def archive
        response = client.get(request.ip, archive_path)
        
        if response.success?
          redirect_to response.body[:urls][params[:version].to_sym]
        else
          head :not_found
        end
      end
      
    private
      def client
        @client ||= AquariumClient.new
      end
      
      def archive_path
        "/shark/newspaper/#{params[:year]}/#{params[:month]}/#{params[:day]}/zip"
      end
    end
  end
end
