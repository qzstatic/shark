module Api
  module V1
    class SearchController < ApplicationController
      def documents
        # TODO это заглушка
        result = Oj.load(File.read("#{Rails.root}/lib/dummy/search.json"))
        
        result[:found].each_with_index do |item, index|
          item[:source][:id] = 140737488355328 + index
        end
        
        render json: result
      end
    end
  end
end
