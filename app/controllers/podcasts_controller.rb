class PodcastsController < ApplicationController
  def last
    @podcasts = PodcastsItems.new.call
    @podcast = @podcasts.shift

    respond_to do |format|
      format.html { render 'by_date', layout: 'podcast'}
      format.mobile { render 'by_date' }
      format.json { render json: { podcast: @podcast, podcasts: @podcasts } }
    end
  end
  
  def by_date
    @podcast = PodcastByNewsrelease.new(newsrelease).call
    not_found and return unless @podcast
    @podcasts = PodcastsItems.new(exclude_ids: [@podcast.newsrelease_id]).call

    respond_to do |format|
      format.html { render 'by_date', layout: 'podcast'}
      format.mobile { render 'by_date' }
      format.json { render json: { podcast: @podcast, podcasts: @podcasts } }
    end
  end

private

  def newsrelease
    @newsrelease ||= Roompen.document("vedomosti.ru/newspaper/#{params[:year]}/#{params[:month]}/#{params[:day]}")
  end
end
