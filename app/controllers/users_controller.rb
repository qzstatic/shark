class UsersController < ApplicationController
  def show
    if user_data = user(access_token)
      @form = ProfileForm.new(access_token: access_token, **user_data)
      
      respond_to do |format|
        format.html
        format.json { render json: { user: user_data, access_rights: @form.access_rights } }
      end
    else
      redirect_to root_url
    end
  end
  
  def update
    @form = ProfileForm.new(ip: request.ip, access_token: access_token, **params[:profile_form].symbolize_keys)
    
    if @form.save
      redirect_to users_show_path
    else
      render :show
    end
  end
  
  def accept_promo
    use_case = AcceptPromo.new(params[:promo_name], access_token, request.ip)
    
    if use_case.call
      head :ok
    else
      head :unprocessable_entity
    end
  end
  
private
  def user(access_token)
    response = client.get(request.ip, "/shark/users/#{access_token}/profile")
    response.body if response.success?
  end
end
