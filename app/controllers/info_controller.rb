class InfoController < ApplicationController
  layout 'info'
  
  def index
    @documents = Roompen.categorized(%w(kinds info), limit: 30).sort { |a, b| a.title <=> b.title }

    respond_to do |format|
      format.html
      format.json do
        render json: {
          document: @documents
        }
      end
    end
  end

  def feedback
    @feedback_form = FeedbackForm.new
  end
  
  def send_feedback
    @feedback_form = FeedbackForm.new(
      access_token: access_token,
      ip: request.ip,
      user_agent: request.user_agent,
      **params[:feedback_form].symbolize_keys
    )
    
    use_case = FeedbackSend.new(@feedback_form)
    use_case.call
    @success = use_case.success?

    respond_to do |format|
      format.html { render 'info/feedback' }
      format.mobile { render 'm/info/feedback' }
    end
  end

  #GET /contacts
  def contacts
    respond_to do |format|
      format.html
      format.mobile
    end
  end
  
  def show
    url = [Settings.domain, 'info', params[:slug]].join('/')
    @document = Roompen.document(url)

    if @document.redirect?
      set_cookie(:original_referer, request.referrer) if cookies[:original_referer].nil?
      redirect_to "http://www.#{@document.location}", status: :moved_permanently
    else
      expires_in 1.year, public: true, must_revalidate: true
      response.headers['Last-Modified'] = @document.last_published_at.httpdate
      unset_cookie(:original_referer)
      
      respond_to do |format|
        format.html
        format.mobile
        format.json do
          render json: {
            document: @document
          }
        end
      end
    end
  end
end
