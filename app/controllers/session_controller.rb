class SessionController < ApplicationController
  def create
    if !Rails.env.production? || token_absent? || token_invalid?
      set_session
      ::NewRelic::Agent.increment_metric('Custom/Sessions/create')
    end

    redirect_to params.fetch(:url, root_path)
  end

  def sign_in_form
    set_return_to_cookie
    @sign_in_form = SignInForm.new
  end

  def sign_in_with_password
    @sign_in_form = SignInForm.new(
      access_token: access_token,
      ip: request.ip,
      **params.fetch(:sign_in_form, {}).symbolize_keys
    )

    use_case = SignInWithPassword.new(@sign_in_form)
    use_case.call

    if use_case.new_token.present?
      response.headers['Set-Cookie'] = token_cookie_header(use_case.new_token)
    end

    if use_case.success?
      respond_to do |format|
        format.html   { delete_cookie_and_redirect }
        format.mobile { delete_cookie_and_redirect }
        format.json   { render json: use_case.user_data }
      end
    elsif use_case.blocked?
      redirect_to blocked_user_url
    elsif use_case.too_many_sessions?
      redirect_to sessions_limit_url
    else
      respond_to do |format|
        format.html   { render 'session/sign_in_form' }
        format.mobile { render 'session/sign_in_form' }
        format.json   { head :unprocessable_entity }
      end
    end
  end

  def sign_out
    client.delete(
      request.ip,
      '/shark/sign_out',
      access_token: access_token
    )
    redirect_to request.referer || root_url
  end

private
  def set_session
    aquarium_response = client.post(
      request.ip,
      '/shark/sessions',
      session: { ip: request.ip }
    )

    if aquarium_response.success?
      response.headers['Set-Cookie'] = token_cookie_header(aquarium_response.body[:access_token])
    end
  end

  def client
    AquariumClient.new
  end

  def token_absent?
    !cookies.key?(:access_token) || cookies[:access_token].empty?
  end

  def token_invalid?
    return true if access_token.empty?
    client.get(request.ip, "/shark/users/#{access_token}")
    false
  rescue AquariumClient::Unauthorized
    return true
  end
end
