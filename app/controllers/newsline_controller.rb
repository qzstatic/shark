class NewslineController < ApplicationController
  def index
    if params[:url].present?
      find_document
      @paywall_visit_url = "vedomosti.ru#{request.path}"
    end
    
    list_slug = if params[:rubric_slug].present?
                  "rubrics-#{params[:rubric_slug]}-news"
                elsif params[:story_slug].present?
                  @story = params[:story_slug]
                  "story-#{@story}-news"
                elsif params[:top]
                  'main-news'
                elsif @document && @document.has_rubric?
                  "rubrics-#{@document.rubric.slug}-news"
                else
                  'main-news'
                end
    
    @news = Roompen.uncached_list(list_slug, limit: 100).documents
    @sign_in_form = SignInForm.new
    @registration_form = PasswordRegistrationForm.new
    
    announce_rss_for(:newsline)
    
    respond_to do |format|
      format.html
      format.json   { render json: { news: @news, document: @document } }
    end
  end
  
  def owl
    find_document if params[:url].present?
    
    list_slug = if params[:rubric_slug].present?
                  "rubrics-#{params[:rubric_slug]}-news"
                elsif params[:story_slug].present?
                  "story-#{params[:story_slug]}-news"
                elsif params[:top]
                  'main-news'
                elsif @document && @document.has_rubric?
                  "rubrics-#{@document.rubric.slug}-news"
                else
                  'main-news'
                end
    @news = Roompen.uncached_list(list_slug, limit: 100).documents
    @sign_in_form = SignInForm.new
    @registration_form = PasswordRegistrationForm.new

    @owl_status = params[:owl_status].to_s.downcase
    @owl_reason = params[:owl_reason]
    @owl_state  = params[:owl_state]

    respond_to do |format|
      format.html { render "newsline/owl", layout: false }
      format.json { render json: { news: @news, document: @document } }
    end
  end
  
private
  def find_document
    url = [Settings.domain, URI.escape(params[:url])].join('/')
    @document = Roompen.document(url)
    if @document.redirect?
      @document = Roompen.document(@document.location)
    end
  end
end
