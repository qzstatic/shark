class StoriesController < ApplicationController
  def show
    url = [Settings.domain, 'story', URI.escape(params[:slug])].join('/')
    @story = Roompen.document(url)
    @page_type = :story
    @documents = Roompen.uncached_list("story-#{params[:slug]}-top", limit: 50).documents
    @news = (@news_exists = @story.try(:show_news)) ? story_news : []

    respond_to do |format|
      format.html
      format.json { render json: { story: @story, documents: @documents, news: @news } }
    end
  end
  
  def news
    @documents = story_news
    
    respond_to do |format|
      format.html
      format.json { render json: { documents: @documents } }
    end
  end
  
private
  def story_news
    Roompen.uncached_list("story-#{params[:slug]}-news", limit: 50).documents
  end
end
