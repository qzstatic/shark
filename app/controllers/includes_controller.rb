class IncludesController < ApplicationController
  skip_before_action :handle_session
  layout false
  
  def get_session
    if cookies[:new_visitor] && token = client.get_access_token(ip: request.ip)
      set_cookie(:access_token, token, 20.years.from_now)
      response.headers['X-Access-Token'] = token
      ::NewRelic::Agent.increment_metric('Custom/Sessions/create')
    end
    
    head :ok
  end
  
  def user_profile
    begin
      @user = user(params[:access_token]) if params.key?(:access_token) && owl_state != 'guest'
    rescue AquariumClient::Unauthorized
      unset_cookie(:access_token)
    end
    
    respond_to do |format|
      format.html
      format.json { render json: @user }
    end
  end
  
  def popular24
    @popular24 = Roompen.list('popular24')
    
    respond_to do |format|
      format.html   { render partial: 'includes/popular24' }
      format.mobile { render partial: 'includes/popular24' }
      format.json   { render json: { popular: @popular24 } }
    end
  end
  
  def top_right
    @main_top, @popular24 = Roompen.lists('main-top', 'popular24')
    
    respond_to do |format|
      format.html { render partial: 'includes/tabs' }
      format.json { render json: { main_top: @main_top, popular24: @popular24 } }
    end
  end

  def fast_news
    @fast_news = Roompen.list('main-news')

    respond_to do |format|
      format.html { render partial: 'includes/fast_news' }
      format.json { render json: { fast_news: @fast_news } }
    end
  end
  
  def quotes
    response = client.get(request.ip, '/shark/quotes/main_page')
    @quotes = response.body if response.success?
    
    respond_to do |format|
      format.html { render partial: 'includes/course' }
      format.json { render json: { quotes: @quotes } }
    end
  end
  
  def also_in_rubric
    @also_in_rubric = AlsoInRubric.new("rubrics-#{params[:rubric_slug]}-top")
    
    respond_to do |format|
      format.html { render partial: 'includes/also_in_rubric', locals: { list: @also_in_rubric } }
      format.json { render json: { also_in_rubric: @also_in_rubric } }
    end
  end
  
  def also_in_story
    @also_in_story = AlsoInStory.new("story-#{params[:story_slug]}-top")
    
    respond_to do |format|
      format.html { render partial: 'includes/also_in_story', locals: { list: @also_in_story } }
      format.json { render json: { also_in_story: @also_in_story } }
    end
  end

  def also_in_story_media
    @also_in_story = AlsoInStory.new("story-#{params[:story_slug]}-top")

    respond_to do |format|
      format.html { render partial: 'includes/also_in_story_media', locals: { list: @also_in_story } }
      format.json { render json: { also_in_story: @also_in_story } }
    end
  end

  def region
    @list = Roompen.list("regions-#{params[:region_slug]}-top")
    
    respond_to do |format|
      format.html { render 'common/rubric/_medium_no_banner', locals: { list: @list } }
      format.json { render json: { list: @list } }
    end
  end
  
  def regions_menu
    @regions = Roompen.child_categories_of(['regions'])
    
    respond_to do |format|
      format.html { render partial: 'includes/regions_menu', locals: { regions: @regions } }
      format.json { render json: { regions: @regions } }
    end
  end
  
  def big_photo
    @documents = Roompen.categorized(%w(project vedomosti), %w(parts galleries), %w(kinds materials), limit: 4)
    
    respond_to do |format|
      format.html { render partial: 'includes/photo' }
      format.json { render json: { documents: @documents } }
    end
  end
  
  def another_from_part
    @part = Roompen.category(['parts', params[:slug]])
    @documents = Roompen.categorized(%w(project vedomosti), ['parts', params[:slug]], %w(kinds materials), limit: 7)
    
    respond_to do |format|
      format.html { render partial: 'includes/another_from_part' }
      format.json { render json: { documents: @documents, part: @part } }
    end
  end
  
  def top_press_releases
    @press_releases = Roompen.categorized(%w(kinds press_releases), limit: 4, full: true)
    
    respond_to do |format|
      format.html { render partial: 'includes/top_press_releases' }
      format.json { render json: { press_releases: @press_releases } }
    end
  end

  def materials_of_company
    find_company
    
    response = regulus_client.get '/documents/published', q: "\"#{@company.title}\"", categories: 'kinds, materials, parts, rubrics', sort: 'date'
    @documents = if response.success? && response.body.key?(:found)
      response.body[:found].lazy.select do |item|
        item.key?(:source)
      end.map do |item|
        Document.new(item[:source])
      end.sort do |doc1, doc2|
        doc2.published_at <=> doc1.published_at
      end
    else
      []
    end
    
    respond_to do |format|
      format.html { render partial: 'includes/materials_of_company' }
      format.mobile { render partial: 'm/includes/materials_of_company' }
      format.json { render json: { documents: @documents, company: @company } }
    end
  end

  def materials_of_person
    find_person

    response = regulus_client.get '/documents/published', q: "\"#{@person.title}\"", categories: 'kinds, materials, parts, rubrics', sort: 'date'
    @documents = if response.success? && response.body.key?(:found)
                   response.body[:found].lazy.select do |item|
                     item.key?(:source)
                   end.map do |item|
                     Document.new(item[:source])
                   end.sort do |doc1, doc2|
                     doc2.published_at <=> doc1.published_at
                   end
                 else
                   []
                 end

    respond_to do |format|
      format.html { render partial: 'includes/materials_of_person' }
      format.mobile { render partial: 'm/includes/materials_of_person' }
      format.json { render json: { documents: @documents, person: @person } }
    end
  end

  def story
    url = [Settings.domain, 'story', URI.escape(params[:slug])].join('/')
    story = Roompen.document(url)
    
    documents = Roompen.uncached_list("story-#{params[:slug]}-top", limit: 4).documents
    
    respond_to do |format|
      format.html   { render 'includes/_story', locals: { story: story, documents: documents } }
      format.mobile { render 'blocks/_story', locals: { story: story, documents: documents } }
      format.json   { render json: { story: story, documents: documents } }
    end
  end
  
  def redline
    @redline_active = redline_active
    
    respond_to do |format|
      format.html   { render 'includes/_redline' }
      format.mobile { render 'includes/_redline' }
      format.json   { render json: { redline: @redline_active } }
    end
  end

  def metynnis
    response = MetynnisClient.new.get(params[:url], returnUrl: params[:returnUrl])
    if response.success?
      render text: response.body.force_encoding('utf-8')
    else
      head :ok
    end
  end

private
  def find_company
    url = [Settings.domain, 'companies', URI.escape(params[:slug])].join('/')
    @company = Roompen.document(url)
  end

  def owl_state
    params.fetch(:owl_state, 'guest')
  end

  def find_person
    url = [Settings.domain, 'people', URI.escape(params[:slug])].join('/')
    @person = Roompen.document(url)
  end
end
