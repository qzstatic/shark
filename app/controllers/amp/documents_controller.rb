class Amp::DocumentsController < ApplicationController
  include Amp::UrlHelper
  layout "amp/layouts/amp"
  before_action :authorize

  newrelic_ignore_enduser
  
  def show
    @document_url = [Settings.domain, URI.escape(params[:document_url])].join('/')
    @document = Roompen.document @document_url
    if @document.redirect?
      redirect_to document_absolute_amp_url(@document.location), :status => :moved_permanently
    end  
  end

private

  def authorize
    render :nothing => true, status: :forbidden if !access_amp_url?(params[:document_url],params[:secret_key])
  end
end
