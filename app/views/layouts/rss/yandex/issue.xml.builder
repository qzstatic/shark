xml.instruct!
xml.rss 'version' => '2.0', 'xmlns' => 'http://backend.userland.com/rss2', 'xmlns:yandex' => 'http://news.yandex.ru', 'xmlns:media' => "http://search.yahoo.com/mrss/" do
  xml.channel do
    xml.title rss_title
    xml.link rss_site_url
    xml.description 'Главные материалы дня.'
    xml.tag!('yandex:logo', rss_feed_image)
    xml.tag!('yandex:logo', {'type' => 'square'}, rss_yandex_logo_square)
    xml << yield
  end
end
