xml.instruct!
xml.rss 'version' => '2.0', 'xmlns:yandex' => 'http://news.yandex.ru' do
  xml.channel do
    xml.title @title
    xml.link @link
    xml.description @description
    xml.tag!('yandex:logo', rss_feed_image)
    xml.tag!('yandex:logo', {'type' => 'square'}, rss_yandex_logo_square)
    xml << yield
  end
end
