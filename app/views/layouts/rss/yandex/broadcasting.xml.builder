xml.instruct!
xml.rss 'version' => '2.0', 'xmlns' => 'http://backend.userland.com/rss2' do
  xml.channel do
    xml.title @document.title
    xml.link rss_item_link(@document)
    xml.pubDate @document.published_at.in_time_zone.to_s(:rfc822)
    xml << yield
  end
end
