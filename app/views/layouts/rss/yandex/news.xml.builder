xml.instruct!
xml.rss 'version' => '2.0', 'xmlns' => 'http://search.yahoo.com/mrss/', 'xmlns:media' => 'http://search.yahoo.com/mrss/', 'xmlns:yandex' => 'http://news.yandex.ru' do
  xml.channel do
    xml.title rss_title
    xml.link rss_site_url
    xml.description rss_news_description
    xml.tag!('yandex:logo', rss_feed_image)
    xml.tag!('yandex:logo', {'type' => 'square'}, rss_yandex_logo_square)
    xml << yield
  end
end
