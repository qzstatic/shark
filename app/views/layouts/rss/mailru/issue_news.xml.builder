xml.instruct!
xml.rss 'version' => '2.0', 'xmlns:mailru' => 'http://news.mail.ru/' do
  xml.channel do
    xml.title rss_title
    xml.link rss_site_url
    xml.description rss_news_description
    xml.image do
      xml.url rss_feed_image
      xml.title rss_title
      xml.link rss_site_url
    end
    xml << yield
  end
end
