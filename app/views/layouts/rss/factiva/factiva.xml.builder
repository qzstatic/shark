xml.instruct!
xml.rss 'version' => '2.0', 'xmlns:yandex' => 'http://news.yandex.ru' do
  xml.channel do
    xml.title @title
    xml.link rss_site_url
    xml.description @description
    xml.image do
      xml.url rss_feed_image
      xml.title rss_title
      xml.link rss_site_url
    end
    xml << yield
  end
end
# <subcategory>
