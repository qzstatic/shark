xml.instruct!
xml.rss(
  'version' => '2.0',
  'xmlns:media' => 'http://search.yahoo.com/mrss/',
  'xmlns:itunes' => 'http://www.itunes.com/dtds/podcast-1.0.dtd',
  'xmlns:atom' => 'http://www.w3.org/2005/Atom'
) do
  xml.channel do
    xml.title 'Слушай Ведомости'
    xml.link "#{rss_site_url}podcasts/last"
    xml.language 'ru'
    xml.managingEditor 'tech.mail@vedomosti.ru'
    xml.description 'Подкаст Ведомостей.'
    xml.tag!('media:rating', {'scheme' => 'urn:simple'}, 'nonadult')
    xml.tag!('media:keywords', 'ведомости,vedomosti')
    xml.tag!('media:thumbnail', {'url' => cdn_url('/assets/vedomosti_180x180.png')})
    xml.copyright 'Ведомости 2015'
    
    xml.tag!('itunes:category', {'text' => 'News &amp; Politics'})
    xml.tag!('itunes:explicit', 'no')
    xml.tag!('itunes:summary', 'Свежий номер газеты &quot;Ведомости&quot;')
    xml.tag!('itunes:subtitle', 'Выходит ежедневно по будням')
    xml.tag!('itunes:author', 'Ведомости')
    
    xml.tag!('itunes:owner') do
      xml.tag!('itunes:email', 'tech.mail@vedomosti.ru')
      xml.tag!('itunes:name',  'Ведомости')
    end
    
    xml.tag!('itunes:image', {'href' => cdn_url('/assets/vedomosti_180x180.png')})
    
    xml.image do
      xml.link rss_site_url
      xml.url cdn_url('/assets/vedomosti_180x180.png')
      xml.title 'Подкаст Ведомостей'
    end
    
    xml << yield
  end
end
