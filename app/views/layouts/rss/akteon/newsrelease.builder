xml.instruct!
xml.rss 'version' => '2.0', 'xmlns' => 'http://backend.userland.com/rss2', 'xmlns:media' => "http://search.yahoo.com/mrss/" do
  xml.channel do
    xml.title rss_title
    xml.link rss_site_url
    xml.logo rss_feed_image
    xml.description @description
    xml << yield
  end
end
