xml.instruct!
xml.rss 'version' => '2.0', 'xmlns:atom10' => 'http://www.w3.org/2005/Atom' do
  xml.channel do
    xml.title @title
    xml.link rss_site_url
    xml.description @description
    xml.image do
      xml.url   "https://#{Settings.hosts.agami}/assets/vedomosti-logo-250x40.png"
      xml.title '«Ведомости». Ежедневная деловая газета'
      xml.link "#{rss_site_url}?#{rss_google_tracker}"
    end
    xml << yield
  end
end
