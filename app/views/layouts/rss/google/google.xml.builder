xml.instruct!
xml.rss(
  'version'       => '2.0',
  'xmlns:content' => 'http://purl.org/rss/1.0/modules/content/',
  'xmlns:dc'      => 'http://purl.org/dc/elements/1.1/',
  'xmlns:media'   => 'http://search.yahoo.com/mrss/',
  'xmlns:atom'    => 'http://www.w3.org/2005/Atom'
) do

  xml.channel do
    xml.title @title
    xml.link rss_site_url
    xml.description @description
    xml.language 'ru-ru'
    xml.tag!('atom:link', rel: 'hub', href: 'http://pubsubhubbub.appspot.com')
    xml << yield
  end
end
