xml.url do
  xml.loc "http://www.#{sitemap_google_news_item.url}"
  xml.tag!('news:news') do
    xml.tag!('news:publication') do
      xml.tag!('news:name', 'Ведомости')
      xml.tag!('news:language', 'ru')
    end
    # xml.tag!('news:genres', 'PressRelease')
    xml.tag!('news:publication_date', sitemap_google_news_item.published_at.in_time_zone.to_s(:iso8601))
    xml.tag!('news:title', sitemap_google_news_item.title)
  end
end
