xml.item do
  xml.title news_item.title
  xml.link rss_item_link(news_item)
  xml.guid rss_item_link(news_item)
  xml.category news_item.rubric.title if news_item.has_rubric?
  
  if news_item.has_image? && news_item.image.has_version?(:normal)
    # TODO: при необходимости добавить length: news_item.image.normal.size
    xml.enclosure url: cdn_url(news_item.image.normal.url), type: 'image/jpeg'
  end
  
  xml.description strip_tags(@bodies[news_item.id][:description])
  xml.pubDate news_item.published_at.in_time_zone.to_s(:rfc822)
end
