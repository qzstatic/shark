xml.url do
  xml.loc        sitemap_item[:url]
  xml.lastmod    sitemap_item[:lastmod].in_time_zone.to_s(:iso8601)
  xml.changefreq sitemap_item[:changefreq]
  xml.priority   sitemap_item[:priority]
end
