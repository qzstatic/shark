xml.item do
  xml.title item.title
  xml.link rss_item_link(item)
  xml.guid rss_item_link(item)
  xml.pdalink rss_item_pdalink(item)
  xml.author @bodies[item.id][:author]
  xml.category item.rubric.title if item.has_rubric?
  
  if item.has_image? && item.image.has_version?(:normal)
    # TODO: при необходимости добавить length: item.image.normal.size
    xml.enclosure url: cdn_url(item.image.normal.url), type: 'image/jpeg'
  end
  
  xml.description strip_tags(item.subtitle) if item.has_subtitle?
  xml.pubDate item.published_at.in_time_zone.to_s(:rfc822)
end
