xml.instruct!
xml.rss 'version' => '2.0' do
  xml.channel do
    @quotes.map do |quote|
      xml.quote do
        xml.title quote[:title]
        xml.slug quote[:slug]
        xml.price_time quote[:price_time]
        xml.value quote[:value]
        xml.prev_value quote[:prev_value]
      end
    end
  end
  xml.channel do
    xml.title rss_title
    xml.description rss_news_description
    xml.link 'http://www.aaaaa.ru/'
    xml.language 'ru-ru'
    xml.pubDate Time.now.in_time_zone.to_s(:rfc822)
    xml.lastBuildDate Time.now.in_time_zone.to_s(:rfc822)
    xml << (render(partial: 'rss/common/monitor_item', collection: @monitor_items) || '')
  end
end
