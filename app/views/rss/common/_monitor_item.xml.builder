xml.item do
  xml.title monitor_item.title
  xml.pubDate monitor_item.published_at.in_time_zone.to_s(:rfc822)
end
