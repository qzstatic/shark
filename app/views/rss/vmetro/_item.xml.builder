xml.item do
  xml.title item.title
  # TODO: при необходимости использовать CGI.escapeHTML(text)
  xml.description strip_tags(@bodies[item.id][:description])
  if item.has_image? && item.image.has_version?(:normal)
    # TODO: при необходимости добавить length: item.image.normal.size
    xml.enclosure url: cdn_url(item.image.normal.url), type: 'image/jpeg'
  end
  xml.category item.rubric.title if item.has_rubric?
  xml.pubDate item.published_at.in_time_zone.to_s(:rfc822)
  xml.fulltext do
    xml.cdata! [rss_body_html(@bodies[item.id][:body]), rss_also_links(@popular24.documents)].join
  end
  xml.link rss_item_link(item)
  xml.guid rss_item_link(item)
end
