xml.item do
  # TODO: при необходимости добавить guid
  # xml.guid rss_item_link(news_item)
  xml.title news_item.title
  xml.link rss_item_link(news_item)
  xml.pdalink rss_item_pdalink(news_item)
  xml.category news_item.rubric.title if news_item.has_rubric?
  xml.tag!('description') { xml << strip_tags(@bodies[news_item.id][:description].to_s) }
  xml.pubDate news_item.published_at.in_time_zone.to_s(:rfc822)

  xml.tag!('yandex:genre')     { xml << 'message' }
  
  if news_item.has_source_url? && news_item.source_url.present?
    xml.tag!('yandex:related')   { xml.link(url: news_item.source_url) { xml << news_item.source_title } }
  end
  
  # TODO: при необходимости использовать CGI.escapeHTML(text)
  xml.tag!('yandex:full-text') { xml << strip_tags(@bodies[news_item.id][:body]) }
end
