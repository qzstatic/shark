xml.item do
  xml.description do
    xml.cdata! strip_tags_for_cdata(broadcasting.try(:description) || broadcasting.try(:body))
  end
  xml.pubDate broadcasting.try(:created_at).try(:in_time_zone).try(:to_s, :rfc822)
end
