xml.item do
  xml.title widget_item.title
  xml.category widget_item.rubric.title if widget_item.has_rubric?
  xml.link rss_item_link(widget_item)
  xml.guid rss_item_link(widget_item)
  xml.tag!('description', strip_tags(@bodies[widget_item.id][:description]))
  xml.pubDate widget_item.published_at.in_time_zone.to_s(:rfc822)

  if widget_item.has_image? && widget_item.image.has_version?(:normal)
    xml.enclosure url: cdn_url(widget_item.image.normal.url), type: 'image/jpeg'
  end
end
