xml.item do
  xml.title { xml.cdata! item.title }
  # TODO: при необходимости использовать CGI.escapeHTML(text)
  xml.tag!('description') { xml.cdata! strip_tags(@bodies[item.id][:description]) }
  xml.tag!('mailru:full-text') { xml.cdata! strip_tags(@bodies[item.id][:body]) }
  xml.category item.rubric.title if item.has_rubric?
  xml.pubDate item.published_at.in_time_zone.to_s(:rfc822)
  xml.link rss_item_link(item)
  xml.guid rss_item_link(item)
end
