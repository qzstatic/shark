xml.item do
  xml.title { xml.cdata! news_item.title }
  # TODO: при необходимости использовать CGI.escapeHTML(text)
  xml.tag!('description') { xml.cdata! strip_tags(@bodies[news_item.id][:description] || '') }
  xml.tag!('mailru:full-text') { xml.cdata! strip_tags(@bodies[news_item.id][:body]) }
  xml.category news_item.rubric.title if news_item.has_rubric?
  xml.pubDate news_item.published_at.in_time_zone.to_s(:rfc822)
  xml.link rss_item_link(news_item)
  xml.guid rss_item_link(news_item)
end
