xml.instruct!
xml.feed 'xmlns' => 'http://www.w3.org/2005/Atom', 'xmlns:news' => 'http://itunes.apple.com/2011/Newsstand' do
  xml.updated @newsrelease.published_at.strftime('%FT%T%:z')
  xml.entry do
    xml.id @newsrelease.title.match(/[0-9]+/)[0]
    xml.updated @newsrelease.published_at.strftime('%FT%T%:z')
    xml.published @newsrelease.published_at.to_date.strftime('%FT%T+03:00')
    xml.summary rss_newsstand_summary
    xml.tag!('news:cover_art_icons') do
      list = @newsstand
      list.delete(:_preview)
      list.delete(:original)
      list.each do |version, data|
        xml.tag!('news:cover_art_icon', size: version.upcase, src: "https://#{Settings.hosts.agami}#{data[:url]}")
      end
    end
  end
end
