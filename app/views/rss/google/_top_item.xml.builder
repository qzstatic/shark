xml.item do
  xml.title top_item.title
  xml.link "#{rss_item_link(top_item)}?#{rss_google_tracker}"
  xml.description strip_links(@bodies[top_item.id][:description])
  xml.author @bodies[top_item.id][:author]
end
