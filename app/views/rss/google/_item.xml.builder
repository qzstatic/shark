xml.item do
  xml.title item.title
  xml.link rss_item_link(item)
  xml.guid "ved:newsline:#{item.id}"
  xml.pubDate item.published_at.in_time_zone.to_s(:rfc822)
  xml.dc :creator, @bodies[item.id][:author]
  xml.description strip_links(@bodies[item.id][:description])
  xml.content :encoded do
    xml.cdata! rss_body_html(@bodies[item.id][:body])
  end
end
