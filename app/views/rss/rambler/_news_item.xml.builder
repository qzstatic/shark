xml.item do
  xml.title news_item.title
  xml.link rss_item_link(news_item)
  xml.guid rss_item_link(news_item)
  # TODO: при необходимости использовать CGI.escapeHTML(text)
  xml.tag!('description') { xml << strip_tags(@bodies[news_item.id][:description]) }
  xml.pubDate news_item.published_at.in_time_zone.to_s(:rfc822)
  xml.category 'Новости'
  xml.tag!('rambler:full-text') { xml << strip_tags(@bodies[news_item.id][:body]) }
  xml.tag!('rambler:related', url: rss_item_link(news_item), rel: 'discussion', text: 'text/html')
end
