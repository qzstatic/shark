xml.tag!(:enclosure, url: cdn_url(local_assigns[:url]), type: 'image/jpeg') do
  xml.tag!('image-description') { xml << local_assigns[:description] }
  xml.tag!('image-author') { xml << local_assigns[:author] }
  xml.tag!('image-source') { xml << local_assigns[:source] }
end
