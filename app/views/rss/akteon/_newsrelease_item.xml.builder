xml.tag!(newsrelease_item.part.slug.singularize) do
  xml.title strip_tags(newsrelease_item.newspaper_title)
  xml.intro strip_tags(newsrelease_item.newspaper_subtitle)
  if newsrelease_item.has_authors?
    xml.authors do
      newsrelease_item.authors.each do |author|
        xml.tag!(:author) {
          xml.authorname author.bound_document.title || ''
          xml.authorinfo author.bound_document.appointment
        }
      end
    end
  end
  if newsrelease_item.has_image?
    xml.imagebox do
      xml.imgext(src: cdn_url(newsrelease_item.image.normal.url))
      xml.imagetext newsrelease_item.image_alt
    end
  end
  xml.jrubric newsrelease_item.newspaper.title if newsrelease_item.has_newspaper?
  if newsrelease_item.categories.has_themes? && newsrelease_item.categories.themes.has_cycle_list?
    xml.jrubric2 newsrelease_item.categories.themes.cycle_list.title
  end

  xml.tag!('content') do
    newsrelease_item.boxes.each do |box|
      case box.type
      when 'paragraph'
        tag_name = 'h1' == box.kind ? 'h2' : 'body'
        xml.tag!(tag_name) { xml << strip_tags(box.body.gsub(/<em>.*<\/em>/, '')) }
      when 'text', 'inset_text'
        xml.remarkheader box.title
        xml.remarktext strip_tags(box.body)
      when 'inset_image'
        xml.imagebox do
          xml.imgext(src: cdn_url(box.image.mobile_high.url))
          xml.imagetext box.description
        end
      when 'quote'
        xml.nwname box.name
        xml.nwinfo box.job
        xml.nwtext box.body
      else
        Rails.logger.info("Unknown tag #{box.type}")
      end
    end
  end
end
