xml.item do
  xml.title item.title
  xml.link rss_podcast_link(item)
  xml.pubDate item.published_at.in_time_zone.to_s(:rfc822)
  xml.tag!('guid', {'isPermaLink' => 'true'}, rss_podcast_link(item))
  xml.description strip_tags(item.subtitle) if item.has_subtitle?

  xml.enclosure url: cdn_url(item.url), type: 'audio/mpeg' if item.url
  xml.tag!('itunes:explicit', 'no')
end
