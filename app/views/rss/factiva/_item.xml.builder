xml.item do
  # TODO: при необходимости добавить guid
  xml.guid rss_item_link(item)
  xml.title item.newspaper_title
  xml.link rss_item_link(item)
  xml.pdalink rss_item_pdalink(item)
  xml.author @bodies[item.id][:author]
  xml.category item.categories.newspaper.title
  xml.subcategory 'Газета Ведомости'
  
  if item.has_image? && item.image.has_version?(:normal)
    # TODO: при необходимости добавить length: item.image.normal.size
    xml.enclosure url: cdn_url(item.image.normal.url), type: 'image/jpeg'
  end
  
  if @bodies[item.id][:images].present?
    @bodies[item.id][:images].map do |image|
     xml.enclosure url: cdn_url(image), type: 'image/jpeg'
    end
  end
  
  xml.description strip_tags(@bodies[item.id][:description])
  xml.pubDate item.published_at.in_time_zone.to_s(:rfc822)
  
  xml.tag!('yandex:genre')     { xml << 'article' }
  # TODO: при необходимости использовать CGI.escapeHTML(text)
  xml.tag!('yandex:full-text') do
    xml << "#{strip_tags(item.newspaper_subtitle)}\n" if item.has_newspaper_subtitle?
    xml << strip_tags(@bodies[item.id][:body])
  end
end
