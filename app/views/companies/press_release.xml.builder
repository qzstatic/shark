xml.instruct!
xml.root do
  xml.content do
    xml.company @company.title
    xml.title   @press_release.title
    xml.date    @press_release.published_at.strftime('%d.%m.%Y')
    xml.link    "http://www.#{@press_release.url}"
  end
end
