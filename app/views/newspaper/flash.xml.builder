xml.issue number: @newsrelease.title, date: Russian::strftime(@newsrelease.published_at, '%d %B %Y') do
  @pages_xml_items.each { |item| xml.item txt: item }
end

xml.pages do
  @pages.each do |page|
    # вычисление номера полосы, соответсвующего странице, когда известно category_id полосы и страницы
    chap = @pages_data.each_with_index.find { |page_data, index| page_data[:id] == page.category_page[:id] if page.has_category_page? }
    xml.page src: "https://#{Settings.hosts.agami}" + page.jpg[:original][:url], big: "https://#{Settings.hosts.agami}" + page.swf[:original][:url], chap: chap.nil? ? -1 : chap.last
  end
end
