module SubdomainConstraints
  class M
    class << self
      def matches?(request)
        request.subdomains.include?('m') || (request.params[:mobile].present? && !request.params[:mobile].empty?)
      end
    end
  end
end
