class RegulusClient
  extend Forwardable
  
  %i(get post put patch delete).each do |verb|
    define_method(verb) do |path, params = {}|
      connection.send(verb, path, params)
    end
  end
  
  def connection
    Faraday.new(url: Settings.hosts.regulus) do |builder|
      builder.request  :url_encoded
      builder.response :oj, content_type: 'application/json'
      builder.adapter  :net_http_persistent
    end
  end
end

