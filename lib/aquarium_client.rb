class AquariumClient
  extend Forwardable
  
  %i(get post put patch delete).each do |verb|
    define_method(verb) do |ip, path, params = {}|
      begin
        connection.send(verb, path, params) do |req|
          req.headers['X-Forwarded-For'] = ip
        end
      rescue Net::HTTP::Persistent::Error => e
        raise unless e.message =~ /too many connection resets/
        ::NewRelic::Agent.increment_metric('Custom/AquariumClient/ConnectionResets')
        retry
      end
    end
  end
  
  def get_access_token(ip:)
    response = post(
      ip,
      '/shark/sessions',
      session: { ip: ip }
    )
    
    response.body[:access_token] if response.success?
  end
  
  def connection
    Faraday.new(url: Settings.hosts.aquarium) do |builder|
      builder.request  :url_encoded
      builder.response :oj, content_type: 'application/json'
      builder.response :unauthorized_handler
      builder.adapter  :net_http_persistent
    end
  end
  
  class Unauthorized < StandardError; end
  
  class UnauthorizedHandler < Faraday::Response::Middleware
    def on_complete(response)
      raise Unauthorized if response.status == 401
    end
  end
  
  Faraday::Response.register_middleware(unauthorized_handler: UnauthorizedHandler)
end
