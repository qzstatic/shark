class MetynnisClient
  extend Forwardable
  
  %i(get post).each do |verb|
    define_method(verb) do |path, params = {}|
      connection.send(verb, path, params)
    end
  end
  
  def connection
    Faraday.new(url: "http://#{Settings.hosts.metynnis}") do |builder|
      builder.request :url_encoded
      builder.adapter Faraday.default_adapter
    end
  end
end
