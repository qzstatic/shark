module Shark
  module SocialNetworkClient
    class << self
      def user_info_for(provider:, token:, token_secret: nil)
        case provider
        when 'twitter'
          Twitter.new(token, token_secret)
        when 'facebook'
          Facebook.new(token)
        when 'vkontakte'
          Vkontakte.new(token)
        else
          raise ArgumentError, "Unknown provider #{provider.ai}"
        end.user_info.merge(provider: provider)
      end
    end
    
    # Тестовые токены можно получить https://apps.twitter.com/app/7037452/keys
    # Не забываем указывать token_secret.
    #
    class Twitter
      attr_reader :client
      
      def initialize(access_token, access_token_secret)
        Rails.logger.info "access_token = #{access_token}"
        Rails.logger.info "access_token_secret = #{access_token_secret}"
        
        @client = ::Twitter::REST::Client.new do |config|
          config.consumer_key        = Settings.twitter.app_id
          config.consumer_secret     = Settings.twitter.app_secret
          config.access_token        = access_token
          config.access_token_secret = access_token_secret
        end
      end
      
      def user_info
        first_name, last_name = user_hash.name.split(' ', 2)
        {
          external_id: user_hash.id.to_i,
          first_name:  first_name || '',
          last_name:   last_name || '',
          nickname:    user_hash.screen_name
        }
      end
      
    private
      def user_hash
        @user_hash ||= client.user
      rescue ::Twitter::Error => e
        Rails.logger.fatal "#{e.class}: #{e.message.ai} (rate limit = #{e.rate_limit.remaining.ai} / #{e.rate_limit.limit.ai})"
        raise
      end
    end
    
    # Сначала получаем ссылку с code:
    #
    # client = Koala::Facebook::OAuth.new(
    #   Settings.facebook.app_id,
    #   Settings.facebook.app_secret,
    #   'http://shark.dev/'
    # )
    # client.url_for_oauth_code
    #
    # Затем получаем URL для получения токена:
    #
    # client.url_for_access_token('...')
    #
    class Facebook
      attr_reader :client
      
      def initialize(access_token)
        @client = Koala::Facebook::API.new(access_token, Settings.facebook.app_secret)
      end
      
      def user_info
        {
          external_id: user_hash['id'].to_i,
          first_name:  user_hash['first_name'],
          last_name:   user_hash['last_name'],
          email:       user_hash['email']
        }
      end
      
    private
      def user_hash
        @user_hash ||= client.get_object('me')
      end
    end
    
    # Для получения тестового токена надо получить и посетить URL:
    # VkontakteApi.authorization_url(type: :client)
    #
    class Vkontakte
      attr_reader :client
      
      FIELDS = %i(first_name last_name screen_name nickname).freeze
      
      def initialize(access_token)
        @client = VkontakteApi::Client.new(access_token)
      end
      
      def user_info
        {
          external_id: user_hash.uid.to_i,
          first_name:  user_hash.first_name,
          last_name:   user_hash.last_name,
          nickname:    nickname
        }
      end
      
    private
      def user_hash
        @user_hash ||= client.users.get(fields: FIELDS).first
      end
      
      def nickname
        if user_hash.nickname.empty?
          user_hash.screen_name
        else
          user_hash.nickname
        end
      end
    end
  end
end
