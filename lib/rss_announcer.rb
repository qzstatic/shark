module RSSAnnouncer
  class Null
    def feeds
      []
    end
  end
  
  class Index
    def feeds
      [news_feed, articles_feed, issue_feed]
    end
    
  private
    def news_feed
      Feed.new('/news', 'Новости')
    end
    
    def articles_feed
      Feed.new('/articles', 'Главное')
    end
    
    def issue_feed
      Feed.new('/issue', 'Газетный выпуск')
    end
  end
  
  class Rubric
    def initialize(rubric)
      @rubric = rubric
    end
    
    def feeds
      [articles_feed, news_feed]
    end
    
  private
    attr_reader :rubric
    
    def articles_feed
      Feed.new("/rubric/#{rubric.slug}", [rubric.title, 'Статьи'])
    end
    
    def news_feed
      Feed.new("/news/#{rubric.slug}", [rubric.title, 'Новости'])
    end
  end
  
  class Subrubric
    def initialize(rubric, subrubric)
      @rubric    = rubric
      @subrubric = subrubric
    end
    
    def feeds
      [articles_feed, news_feed]
    end
    
  private
    attr_reader :rubric, :subrubric
    
    def articles_feed
      Feed.new("/rubric/#{rubric.slug}/#{subrubric.slug}", [rubric.title, subrubric.title, 'Статьи'])
    end
    
    def news_feed
      Feed.new("/news/#{rubric.slug}", [rubric.title, 'Новости'])
    end
  end
  
  class Newsline
    def feeds
      [news_feed]
    end
    
  private
    def news_feed
      Feed.new('/news', 'Новости')
    end
  end
  
  class Feed
    BASE_URL = "http://#{Settings.hosts.shark}/rss"
    
    attr_reader :url, :title
    
    def initialize(url_part, title_parts)
      @url   = BASE_URL + url_part
      @title = (%w(Ведомости) + Array(title_parts)).join(' — ')
    end
  end
end
