class ArchiveYearsConstraint
  def self.matches?(request)
    (1999..Time.now.year).include?(request.params[:year].to_i)
  end
end
