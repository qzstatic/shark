class ArchiveDateConstraint
  def self.matches?(request)
    begin
      date = Date.new(request.params[:year].to_i, request.params[:month].to_i, request.params[:day].to_i)
      (Date.new(1999, 9, 7)..Date.today).include_with_range? date
    rescue
    end
  end
end
