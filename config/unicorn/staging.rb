app_root = File.expand_path('../../..', __FILE__)

worker_processes   2
preload_app        true
timeout            15
listen             9005, backlog: 4096
user               'app'
working_directory  app_root
stderr_path        'log/unicorn.stderr.log'
stdout_path        'log/unicorn.stdout.log'

if GC.respond_to?(:copy_on_write_friendly=)
  GC.copy_on_write_friendly = true
end
