# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.

# Rails.application.config.assets.precompile += %w( search.js )
# Rails.application.config.assets.precompile += %w( online.js )
# Rails.application.config.assets.precompile += %w( vendors/hc.stream.js )
# Rails.application.config.assets.precompile += %w( vendors/style.css )
# Rails.application.config.assets.precompile += %w( vendors/TeaserBlock.site.js )

Rails.application.config.assets.precompile += [
    'online.js',
    'no_console.js',
    'vendors/hc.stream.js',
    'vendors/hypercomments.js',
    'subscription.css',
    'app.js',
    'podcast.js',
    'vendors/detect-private-browsing.js',
    'vendors/ads.js',
    'vendors/agima.js',
    'vendors/agima2.js',
    'vendors/adfox.asyn.code.ver3.js',
    'ie/vendors.css',
    'ie/vedomosti.css',
    'fonts_css.css',
    'm/fonts.css',
    'vendors/d3.v3.min.js',
    'vendors/queue.v1.min.js',
    'vendors/topojson.js',
    'vendors/sticky-tables.js',
    'special/ampersand/ampersand.css',
    'm/special/ampersand/ampersand.css',
]
