VkontakteApi.configure do |config|
  config.app_id       = Settings.vkontakte.app_id
  config.app_secret   = Settings.vkontakte.app_secret
  config.redirect_uri = 'http://api.vk.com/blank.html'
end
