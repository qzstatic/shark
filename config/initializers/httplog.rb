HttpLog.options[:compact_log]   = true
HttpLog.options[:severity]      = Logger::Severity::DEBUG
HttpLog.options[:logger]        = Logger.new($stdout)
