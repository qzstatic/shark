require File.expand_path('../boot', __FILE__)

require 'action_controller/railtie'
require 'action_mailer/railtie'
require 'action_view/railtie'
require 'sprockets/railtie'
require 'active_model'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

Settings::Utils.environments << :staging << :preproduction

module Shark
  class Application < Rails::Application
     %w(lib app/validators).each do |path|
      config.autoload_paths << config.root.join(path).to_s
    end

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    config.time_zone = 'Moscow'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    config.i18n.default_locale = :ru

    config.middleware.use Rack::Session::Cookie, secret: Settings.rack_session_cookie.secret
    config.middleware.use ActionDispatch::Flash

    config.assets.precompile += %w(hc.css m/boot.js)
  end
end
