
Rails.application.routes.draw do
  root 'main#index'

  get '/main' => 'main#index'

  get '/:region_slug' => 'main#index', region_slug: /spb|siberia|south|volga/

  get '/rubrics/:rubric_slug' => 'main#rubric', rubric_slug: /[a-z0-9_]+/, as: :rubric
  get '/rubrics/:rubric_slug/:subrubric_slug' => 'main#subrubric', rubric_slug: /[a-z0-9_]+/, subrubric_slug: /[a-z0-9_]+/, as: :subrubric

  # parts
  get '/parts/:part_slug' => 'main#part', part_slug: /[a-z0-9_]+/, as: :part
  get '/parts/:part_slug/:subpart_slug' => 'main#subpart', part_slug: /video/, subpart_slug: /[a-z0-9_]+/, as: :subpart

  # newspaper
  get '/newspaper/:year/:month/calendar'   => 'newspaper#calendar', year: /\d{4}/, month: /\d{2}/
  get '/newspaper/:year/:month/:day'       => 'newspaper#by_date',  year: /\d{4}/, month: /\d{2}/, day: /\d{2}/, as: :newspaper
  get '/newspaper/last'                    => 'newspaper#by_date', as: :newspaper_last

  # vedomosti&
  get '/partner'                         => 'partner#index'
  get '/partner/:year/:month/:day'       => 'partner#by_date',  year: /\d{4}/, month: /\d{2}/, day: /\d{2}/
  get '/partner/last'                    => 'partner#by_date'
  get '/partner/*url'                    => 'partner#show'

  # newspaper flash.xml
  get '/newspaper/:year/:month/:day/flash' => 'newspaper#flash', year: /\d{4}/, month: /\d{2}/, day: /\d{2}/, as: :newspaper_flash
  get '/newspaper/last/flash'              => 'newspaper#flash', as: :newspaper_last_flash
  get '/newspaper',                        to: redirect('/newspaper/last')

  # story
  get '/story/:slug' => 'stories#show', slug: /[a-z0-9_]+/, as: :stories
  get '/story/:slug/news' => 'stories#news', slug: /[a-z0-9_]+/

  # newsline
  get '/newsline'              => 'newsline#index', top: true
  get '/newsline(/top)'        => 'newsline#index', top: true
  get '/newsline/:rubric_slug' => 'newsline#index', rubric_slug: /[a-z0-9_]+/, as: :rubric_news

  get '/newsline/news/*oldurl' => 'document#oldnews' # для новостей со старыми урлами
  get '/fast_news'             => redirect('/newsline')

  # search
  get '/search' => 'search#index'

  # sign up
  get  '/sign_up' => 'register#sign_up_form'
  post '/sign_up_with_password' => 'register#sign_up_with_password'

  # password reset
  get  '/password_reset'                       => 'register#email_form',  as: :password_reset_email_form
  post '/password_reset'                       => 'register#check_email', as: :password_reset_email_send

  get  '/password_reset/:password_reset_token' => 'register#password_form',   password_reset_token: /[a-z0-9_]+/, as: :password_form
  post '/password_reset/:password_reset_token' => 'register#password_update', password_reset_token: /[a-z0-9_]+/, as: :password_send

  get  '/auth/:provider/callback' => 'register#oauth_callback', provider: /[a-z0-9_]+/
  post '/sign_up_with_oauth'      => 'register#sign_up_with_oauth'

  # sign in / out
  get  '/sign_in'  => 'session#sign_in_form', as: :sign_in
  get  '/sign_out' => 'session#sign_out',     as: :sign_out

  # sign in with password
  post '/sign_in_with_password' => 'session#sign_in_with_password'

  get '/blocked_user'      => 'session#blocked_user'
  get '/too_many_sessions' => 'session#too_many_sessions', as: :sessions_limit

  # sign in with oauth
  post '/sign_in_with_oauth'    => 'session#sign_in_with_oauth'
  get  '/auth/failure' => 'register#oauth_failure'

  get '/includes/session/yes'               => 'includes#get_session'
  get '/includes/popular24'                 => 'includes#popular24', as: :popular24
  get '/includes/story/:slug'               => 'includes#story', slug: /[a-z0-9_]+/
  get '/includes/redline'                   => 'includes#redline'
  get '/includes/companies/:slug/materials' => 'includes#materials_of_company'
  get '/includes/people/:slug/materials'    => 'includes#materials_of_person'

  # user registration confirm
  get  '/confirm' => 'register#confirm'

  # user profile include
  get '/includes/long_cache/user_profile(/:access_token)'          => 'includes#user_profile', owl_state: 'guest',               access_token: /[a-f0-9]+/
  get '/includes/long_cache/user_profile/:owl_state/:access_token' => 'includes#user_profile', owl_state: /guest|registered|authenticated/, access_token: /[a-f0-9]+/

  get '/includes/metynnis/*url' => 'includes#metynnis'

  get '/owl/body/:owl_status/:owl_reason/:owl_state/*url(.:format)' => 'document#owl_show', format: /json|html|xml/

  get '/includes/owl_smart/:owl_status/:owl_reason/:owl_state/newspaper/:year/:month/:day/issue' => 'newspaper#owl_newspaper_issue', year: /\d{4}/, month: /\d{2}/, day: /\d{2}/

  #online
  get 'online/template' => 'online#index'

  # libraries
  get '/library' => 'libraries#index', as: :libraries
  get '/library/:slug' => 'libraries#show', slug: /[a-z0-9_]+/, as: :library

  # sessions
  get '/session'               => 'session#create'
  get '/user/profile'          => 'users#show', as: :users_show
  post '/user/profile'         => 'users#update', as: :users_update
  post '/profile/accept_promo' => 'users#accept_promo'

  # info
  namespace :info, defaults: { controller: 'info' } do
    get  '/',         action: 'index'
    get  '/feedback', action: 'feedback'
    post '/feedback', action: 'send_feedback'
    get  '/contacts', action: 'contacts'
    get  '/:slug',    action: 'show', slug: /[a-z0-9_-]+/
  end

  # multimedia
  get '/multimedia' => 'main#multimedia'

  # podcasts
  get '/podcast'                    => redirect('/podcasts/last')
  get '/podcasts'                   => redirect('/podcasts/last')
  get '/podcasts/last'              => 'podcasts#last'
  get '/podcasts/:year/:month/:day' => 'podcasts#by_date'

  get '/tests/*url/q:step'             => 'tests#show'
  get '/tests/*url/r:result_id'        => 'tests#result'
  get '/tests/*url/s:index'            => 'tests#share'
  get '/tests/*url'                    => 'tests#show'
  get '/:rubric/test/*url/q:step'      => 'tests#show'
  get '/:rubric/test/*url/r:result_id' => 'tests#result'
  get '/:rubric/test/*url/s:index'     => 'tests#share'
  get '/:rubric/test/*url'             => 'tests#show'


  # broadcastings
  get '/:rubric/online/*url/check_new_posts/(:start_date)' => 'broadcastings#check_new_posts'
  get '/:rubric/online/*url/p:page' =>                        'broadcastings#show', page: /[1-9][0-9]*/
  get '/:rubric/online/*url' =>                               'broadcastings#show', page: 1

  # companies
  namespace :companies, defaults: { controller: 'companies' } do
    get '/'                                      => redirect('companies/index')
    get '/search',                               action: 'search'
    get '/:filter/',                             action: 'main', filter: /index|industries/
    get '/:filter/:slug',                        action: 'by_filter', filter: /index|industries/
    get '/:filter/:slug/groups/:group',          action: 'by_group', filter: /index|industries/
    get '/:slug',                                action: 'show', slug: /[a-z0-9_\-]+/, tab: 'about'
    get '/:slug/press_releases',                 action: 'press_releases', slug: /[a-z0-9_\-]+/
    get '/:slug/press_releases/:year',           action: 'press_releases_by_year', slug: /[a-z0-9_\-]+/, year: /\d{4}/

    get '/:slug/company_messages',               action: 'company_messages', slug: /[a-z0-9_\-]+/
    get '/:slug/company_messages/:year(/:page)', action: 'company_messages', slug: /[a-z0-9_\-]+/, year: /\d{4}/, page: /\d+/

    get '/:slug/all_press_releases',             action: 'all_press_releases', slug: /[a-z0-9_\-]+/
    get '/:slug/:tab',                           action: 'show', slug: /[a-z0-9_\-]+/, tab: /about|kso|policy/
  end
  get '/press_releases/*suburl'                  => 'companies#press_release'
  get '/company_messages/*suburl'                => 'companies#company_message'

  # people
  namespace :people, defaults: { controller: 'people' } do
    get '/'                               => redirect('people/index')
    get '/:filter',                       action: 'main', filter: /index/
    get '/:filter/:slug',                 action: 'by_filter', constraints: ->(req) { CGI::unescape(req.params[:slug]) =~ /[А-ЯЁA-Z]/ }, filter: /index/
    get '/:filter/:slug/groups/:group',   action: 'by_group',  constraints: ->(req) { CGI::unescape(req.params[:slug]) =~ /[А-ЯЁA-Z]/ && CGI::unescape(req.params[:group]) =~ /[А-ЯЁA-Z][а-яёa-z]+\s—\s[А-ЯЁA-Z][а-яёa-z]+/ }, filter: /index/
    get '/:filter/search',                action: 'search', filter: /index/
    get '/*url',                          action: 'show'
  end
  
  # mobile version
  constraints(SubdomainConstraints::M) do
    scope module: :m do
      get '/user/profile'  => 'users#show',   as: :m_users_show
      post '/user/profile' => 'users#update', as: :m_users_update
      get '/newsline/top/*url' => 'document#show', top: true
      get '/newsline/*url'     => 'document#show'
      get '/*url'              => 'document#show'
    end
  end

  namespace :advert, defaults: { controller: 'advert' } do
    get '/main', action: 'main'
    get '/noteads', action: 'noteads'
    get '/teads', action: 'teads'
    get '/mobile_main', action: 'mobile_main'
  end

  # subscription
  namespace :subscription, defaults: { controller: 'subscription' } do
    get '/',      action: 'engineering_works'
    get '/step1', action: 'engineering_works'
    get '/step2', action: 'engineering_works'
    get '/step3', action: 'engineering_works'

    get '/corporate',        action: 'corporate'
    get '/gift',             action: 'gift'
    get '/representational', action: 'representational'

    get '/prices', action: 'prices'

    post '/payment/card',    action: 'payment_card'
    post '/payment/yandex',  action: 'payment_yandex'

    get '/card_order_check/:uuid', action: 'card_order_check', uuid: /[a-f0-9\-]+/

    post '/card_callback',        action: 'card_callback'

    get  '/success',              action: 'success', as: :ok
  end

  # AMP -  https://www.ampproject.org/docs/get_started/about-amp.html
  namespace :amp do
    # documents
    get '/:secret_key/*document_url'            => 'documents#show',as: :document
  end

  # mobile API
  namespace :api do
    namespace :v1, defaults: { format: :json } do
      get '/check_appkey' => 'main#check_appkey'

      # pages
      get '/main'                                 => 'main#index'
      get '/rubrics/:rubric_slug'                 => 'main#rubric', rubric_slug: /[a-z0-9_]+/, as: :rubric
      get '/rubrics/:rubric_slug/:subrubric_slug' => 'main#subrubric', rubric_slug: /[a-z0-9_]+/, subrubric_slug: /[a-z0-9_]+/, as: :subrubric
      get '/fast_news'                            => 'main#fast_news', as: :fast_news
      get '/fast_news/:slug'                      => 'main#fast_news', category_slug: :rubrics, slug: /[a-z0-9_]+/
      get '/fast_news/:category_slug/:slug'       => 'main#fast_news', category_slug: /rubrics|story/, slug: /[a-z0-9_]+/
      get '/mobile_menu'                          => 'main#mobile_menu'
      get '/mobile_menu_v2'                       => 'main#mobile_menu_v2'
      get '/story/:slug'                          => 'main#story'

      # parts
      get '/parts/:slug' => 'main#part', slug: /[a-z0-9_]+/

      get '/popular24' => 'main#popular24'
      get '/main_top' => 'main#main_top'

      # newspaper
      get '/newspaper/:year/:month/:day' => 'newspaper#by_date', year: /\d{4}/, month: /\d{2}/, day: /\d{2}/, as: :newspaper
      get '/newspaper/last' => 'newspaper#by_date', as: :newspaper_last
      get '/newspaper' => 'newspaper#index', as: :newspaper_index

      # newspaper archive
      get '/newspaper/:year/:month/:day/archive/:version' => 'newspaper#archive', version: /[a-z0-9_]+/, year: /\d{4}/, month: /\d{2}/, day: /\d{2}/, as: :newspaper_archive
      get '/newspaper/last/archive/:version' => 'newspaper#archive', version: /[a-z0-9_]+/, as: :newspaper_last_archive

      get '/podcasts/(:page)' => 'podcasts#index', page: /\d+/
      get '/podcasts/:year/:month/:day' => 'podcasts#by_date'

      # documents
      resources :documents, only: [:show]

      # search
      get '/search' => 'search#documents'

      # user
      get   '/session/access_rights' => 'session#access_rights'
      get   '/session' => 'session#create' # СЕРЬЁЗНО?
      get   '/profile' => 'profile#show'
      patch '/profile' => 'profile#update'
      post  '/profile/accept_promo' => 'profile#accept_promo'

      # sign up/in with password
      post '/sign_up_with_password' => 'register#sign_up_with_password'
      post '/sign_in_with_password' => 'session#sign_in_with_password'
      get  '/sign_out'              => 'session#sign_out'

      # sign up/in with oauth
      post '/sign_up_with_oauth' => 'register#sign_up_with_oauth'
      post '/sign_in_with_oauth' => 'session#sign_in_with_oauth'

      # password reset
      post '/password_reset' => 'register#check_email'

      # subscription
      post '/subscription_apple_verify'  => 'subscription#apple_verify'
      post '/subscription_google_verify' => 'subscription#google_verify'

      post '/subscription_link' => 'subscription#link'

      get '/subscription_products/:target'  => 'subscription#products_list', target: /apple|google/

      get '/documents/*url' => 'documents#show'
    end
  end

  # rss feeds
  scope defaults: { format: :xml } do
    # /sitemap.xml
    get '/sitemap' => 'rss/common#sitemap'
    get '/sitemap_google_news' => 'rss/common#sitemap_google_news'

    # old URLs for compatibility

    # yandex
    get '/newspaper/out/issue4ya'    => 'rss/yandex#issue'
    get '/newsline/out/yandex'       => 'rss/yandex#news'
    get '/newspaper/out/comments4ya' => 'rss/yandex#comments'

    # interfax
    get '/newsline/out/interfax'     => 'rss/yandex#news'
    get '/rss/newsline_interfax'     => 'rss/yandex#news'

    # rambler
    get '/rss/news4rambler_v3'         => 'rss/rambler#news'
    get '/newspaper/out/pages/finance' => 'rss/common#rubric', rubric_slug: 'finance'

    # mail
    get '/newsline/out/news_mail_ru' => 'rss/yandex#news'
    get '/newspaper/out/issue4mail'  => 'rss/mailru#issue_news'

    # anews
    get '/newsline/out/rss_big' => 'rss/anews#news'
    get '/rss/themes_big/tech'  => 'rss/common#rubric', rubric_slug: 'technology'

    # medialogia
    get '/out/medialogia/vedomosti' => 'rss/yandex#issue'
    get '/out/medialogia/news' => 'rss/yandex#news'

    # factiva
    get '/out/factiva/vedomosti' => 'rss/yandex#issue'
    get '/out/factiva/news'      => 'rss/yandex#news'

    # public.ru
    get '/out/public.ru/news' => 'rss/yandex#news'

    # common
    get '/newspaper/out/rss'  => 'rss/common#news'
    get '/newsline/out/rss'   => 'rss/common#news'
    get '/rss/themes/topnews' => 'rss/common#news'

    # old -> new
    get '/rss/themes/finance'   => 'rss/common#rubric', rubric_slug: 'finance'
    get '/rss/themes/tech'      => 'rss/common#rubric', rubric_slug: 'technology'
    get '/rss/themes/auto'      => 'rss/common#rubric', rubric_slug: 'auto'
    get '/rss/themes/opinion'   => 'rss/common#rubric', rubric_slug: 'opinion'
    get '/rss/themes/lifestyle' => 'rss/common#rubric', rubric_slug: 'lifestyle'
    get '/rss/themes/companies' => 'rss/common#rubric', rubric_slug: 'business'
    get '/rss/themes/realty'    => 'rss/common#rubric', rubric_slug: 'realty'
    get '/rss/themes/politics'  => 'rss/common#rubric', rubric_slug: 'politics'

    get '/rss/themes/sport'           => 'rss/common#subrubric', rubric_slug: 'business',   subrubric_slug: 'sport'
    get '/rss/themes/career'          => 'rss/common#subrubric', rubric_slug: 'management', subrubric_slug: 'career'
    get '/newspaper/out/pages/career' => 'rss/common#subrubric', rubric_slug: 'management', subrubric_slug: 'career'
    get '/newspaper/out/pages/analytics' => 'rss/common#subrubric', rubric_slug: 'opinion', subrubric_slug: 'analytics'
    get '/rss/screensaver/news'       => 'rss/common#screensaver'

    # new URLs
    namespace :rss do
      # yandex
      get '/yandex/news'            => 'yandex#news'
      get '/yandex/issue'           => 'yandex#issue'
      get '/yandex/broadcasting:id' => 'yandex#broadcasting', id: /\d+/
      get '/yandex/comments'        => 'yandex#comments'

      # yandex widgets
      get '/yandex/widgets/(:region_slug)' => 'yandex#widgets', region_slug: /spb|siberia|south|volga/

      # interfax
      get '/interfax/news'   => 'yandex#news'

      # rambler
      get '/rambler/finance' => 'rambler#finance'
      get '/rambler/news'    => 'rambler#news'

      # mail
      get '/mailru/news'             => 'mailru#news'
      get '/mailru/issue_news'       => 'mailru#issue_news'
      get '/mailru/informer/(:type)' => 'mailru#informer', type: /auto|manual/
      get '/mailru/articles'         => 'mailru#articles'

      # anews
      get '/anews/news'           => 'anews#news'
      get '/anews/all'            => 'anews#all'
      get '/anews/issue_articles' => 'anews#issue_articles'

      # medialogia
      get '/medialogia/issue' => 'yandex#issue', paying: 'all'
      get '/medialogia/news'  => 'medialogia#news'

      # factiva
      get '/factiva/issue'    => 'factiva#issue'
      get '/factiva/news'     => 'factiva#news'
      get '/factiva/articles' => 'factiva#articles'

      # ios kiosk images
      get '/newsstand' => 'newsstand#newsstand'

      # public.ru
      get '/public/news' => 'yandex#news'

      # common
      get '/rubric/:rubric_slug'                 => 'common#rubric', rubric_slug: /[a-z0-9_]+/
      get '/rubric/:rubric_slug/:subrubric_slug' => 'common#subrubric', rubric_slug: /[a-z0-9_]+/, subrubric_slug: /[a-z0-9_]+/
      get '/news/(:rubric_slug)'                 => 'common#news'
      get '/library/(:library_slug)'             => 'common#library'
      get '/articles'                            => 'common#articles'
      get '/issue'                               => 'common#issue'
      get '/monitor'                             => 'common#monitor'

      # akteon
      get '/akteon/newsrelease(/:year)(/:month)(/:day)' => 'akteon#newsrelease', year: /\d{4,4}/, month: /0?[1-9]|1[0-2]/, day: /0?1-9|[1-2][0-9]|3[0-1]/

      # google newsstand
      get '/google/main_newsstand'      => 'google#news'
      get '/google/auto_newsstand'      => 'google#rubric',    rubric_slug: 'auto'
      get '/google/realty_newsstand'    => 'google#rubric',    rubric_slug: 'realty'
      get '/google/finance_newsstand'   => 'google#rubric',    rubric_slug: 'finance'
      get '/google/career_newsstand'    => 'google#rubric',    rubric_slug: 'management'
      get '/google/politics_newsstand'  => 'google#rubric',    rubric_slug: 'politics'
      get '/google/opinion_newsstand'   => 'google#rubric',    rubric_slug: 'opinion'
      get '/google/tech_newsstand'      => 'google#rubric',    rubric_slug: 'technology'
      get '/google/lifestyle_newsstand' => 'google#rubric',    rubric_slug: 'lifestyle'
      get '/google/economics_newsstand' => 'google#rubric',    rubric_slug: 'economics'

      get '/google/business_newsstand'  => 'google#rubric',    rubric_slug: 'business'
      get '/google/sport_newsstand'     => 'google#rubric',    rubric_slug: 'business'
      get '/google/companies_newsstand' => 'google#rubric',    rubric_slug: 'business'

      get '/googletop' => 'google#top'

      # vmetro (vmet.ro)
      get '/vmetro/all' => 'vmetro#all'

      # podcast
      get '/podcast' => 'podcast#podcast'
    end
  end

  get '/rubrics/:rubric_slug/:subrubric_slug/:section_slug' => 'main#section',
    rubric_slug: /[a-z0-9_]+/, subrubric_slug: /[a-z0-9_]+/, section_slug: /[a-z0-9_]+/, as: :section
  get '/themes/:slug'  => 'main#theme', slug: /[a-z0-9_-]+/, as: :theme

  # newsline
  get '/newsline/story/:story_slug'                                                      => 'newsline#index', story_slug: /[a-z0-9_]+/
  get '/newsline/story/:story_slug/*url'                                                 => 'newsline#index', story_slug: /[a-z0-9_]+/

  get '/owl/newsline/:owl_status/:owl_reason/:owl_state/newsline'                        => 'newsline#owl', top: true
  get '/owl/newsline/:owl_status/:owl_reason/:owl_state/newsline/top'                    => 'newsline#owl', top: true
  get '/owl/newsline/:owl_status/:owl_reason/:owl_state/newsline/top/*url'               => 'newsline#owl', top: true

  get '/owl/newsline/:owl_status/:owl_reason/:owl_state/newsline/story/:story_slug/*url' => 'newsline#owl', story_slug: /[a-z0-9_]+/
  get '/owl/newsline/:owl_status/:owl_reason/:owl_state/newsline/story/:story_slug'      => 'newsline#owl', story_slug: /[a-z0-9_]+/

  get '/owl/newsline/:owl_status/:owl_reason/:owl_state/newsline/:rubric_slug'           => 'newsline#owl', rubric_slug: /[a-z0-9_]+/
  get '/owl/newsline/:owl_status/:owl_reason/:owl_state/newsline/*url'                   => 'newsline#owl'
  get '/newsline/top/*url'                                                               => 'newsline#index', top: true
  get '/newsline/*url'                                                                   => 'newsline#index'
  get '/body/*url'                                                                       => 'document#body'

  # includes
  get '/includes/top_right'                             => 'includes#top_right', as: :top_right
  get '/includes/fast_news'                             => 'includes#fast_news'
  get '/includes/big_photo'                             => 'includes#big_photo', as: :big_photo
  get '/includes/another_from_part/:slug'               => 'includes#another_from_part', slug: /[a-z0-9_]+/, as: :another_from_part
  get '/includes/also_in_rubric/:rubric_slug'           => 'includes#also_in_rubric', rubric_slug: /[a-z0-9_]+/, as: :also_in_rubric
  get '/includes/also_in_story/:story_slug'             => 'includes#also_in_story',  story_slug: /[a-z0-9_]+/, as: :also_in_story
  get '/includes/also_in_story_media/:story_slug'       => 'includes#also_in_story_media',  story_slug: /[a-z0-9_]+/, as: :also_in_story_media
  get '/includes/long_cache/quotes'                     => 'includes#quotes'
  get '/includes/regions/:region_slug'                  => 'includes#region', region_slug: /[a-z0-9_]+/, as: :region_block
  get '/includes/long_cache/regions_menu'               => 'includes#regions_menu'
  get '/includes/top_press_releases'                    => 'includes#top_press_releases'

  # companies
  get '/companies/industries'                 => 'companies#industries'
  get '/companies/:slug'                      => 'companies#show', slug: /[a-z0-9_\-]+/, tab: 'about'
  get '/companies'                            => 'companies#index'
  get '/companies/:slug/press_releases'       => 'companies#press_releases', slug: /[a-z0-9_\-]+/
  get '/companies/:slug/press_releases/:year' => 'companies#press_releases_by_year', slug: /[a-z0-9_\-]+/, year: /\d{4}/

  get '/companies/:slug/company_messages'     => 'companies#company_messages', slug: /[a-z0-9_\-]+/
  get '/companies/:slug/company_messages/:year(/:page)' => 'companies#company_messages', slug: /[a-z0-9_\-]+/, year: /\d{4}/, page: /\d+/

  get '/companies/:slug/all_press_releases'   => 'companies#all_press_releases', slug: /[a-z0-9_\-]+/
  get '/companies/:slug/:tab'                 => 'companies#show', slug: /[a-z0-9_\-]+/, tab: /about|kso|policy/

  get '/press_releases/*suburl'               => 'companies#press_release'
  get '/company_messages/*suburl'             => 'companies#company_message'

  # Архив новостей
  get '/archive/:year/:month/:day' => 'archive#day',  constraints: ArchiveDateConstraint , as: :archive_for_day
  get '/archive/:year'             => 'archive#year', constraints: ArchiveYearsConstraint, as: :archive_for_year
  get '/archive'                   => 'archive#years', as: :archive

  # comments
  if Settings.comments.enable
    # comments
    get  '/comments/popular/:widget_id/:xid' => 'comments#show_popular',   widget_id: /\d+/, xid: /\d+/
    get  '/includes/long_cache/popular_comment/:xid' => 'comments#show_popular', xid: /\d+/
    post '/comments/popular' => 'comments#update_popular'

    get '/comments/*url' => 'document#comment'
  end

  get '/preview/:document_id'                   => 'document#preview', document_id: /\d+/

  get '/preview/tests/:document_id(/q:step)'    => 'tests#preview',        document_id: /\d+/, step: /\d+/
  get '/preview/tests/:document_id/r:result_id' => 'tests#preview_result', document_id: /\d+/, result_id: /\d+/
  get '/preview/tests/:document_id/s:index'     => 'tests#preview_share',  document_id: /\d+/

  get '/regions/:region_slug' => 'main#region', region_slug: /[a-z0-9_]+/, as: :region

  get '/documents/:id' => 'document#by_id', id: /[0-9\-]+/

  # moved permanently
  %w(
    /persons/:staff/*query
    /companies/a-z/:staff/*query
    /glossary/*query
    /digest/*query
    /related/*query
    /geo/:staff/*query
  ).each do |path|
    get path, to: redirect('/')
  end

  # ниже этого места ничего не трогать!
  get '/*url(.:format)' => 'document#show', format: /json|html|xml/
end
